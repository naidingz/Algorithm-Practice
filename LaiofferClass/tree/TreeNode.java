package tree;

public class TreeNode {
	int value;
	TreeNode left;
	TreeNode right;
	
	public TreeNode(int k) {
		this.value = k;
	}
}
