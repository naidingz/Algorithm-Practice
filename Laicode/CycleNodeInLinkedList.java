package laicode;

import java.util.HashSet;

public class CycleNodeInLinkedList {

    class ListNode {
        int value;
        ListNode next;

        ListNode(int value) {
            this.value = value;
            next = null;
        }
    }

    /*
     * Check if a given linked list has a cycle.
     * Return the node where the cycle starts.
     * Return null if there is no cycle.
     */
    public ListNode cycleNode(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode slow = head, fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (fast == slow) {
                HashSet<ListNode> set = new HashSet<>();
                set.add(slow);
                slow = slow.next;
                while (slow != fast) {
                    slow = slow.next;
                    set.add(slow);
                }
                while (!set.contains(head)) {
                    head = head.next;
                }
                return head;
            }
        }
        return null;
    }
}
