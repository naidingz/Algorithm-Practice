package laicode;

public class SpiralOrderGenerate {

	public static void main(String[] args) {
		SpiralOrderGenerate test = new SpiralOrderGenerate();
		int[][] arr = test.spiralGenerate2(4, 4);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}

	}
	
	public int[][] spiralGenerate(int n) {
		int[][] result = new int[n][n];
		int maxLevel = n / 2, number = 1;
		for (int level = 0; level < maxLevel; level++) {
			int levelLength = n - 2 * level - 1;
			for (int i = 0; i < levelLength; i++) {
				result[level][level + i] = number++;
			}
			for (int i = 0; i < levelLength; i++) {
				result[level + i][n - 1 - level] = number++;
			}
			for (int i = 0; i < levelLength; i++) {
				result[n - 1 - level][n - 1 - level - i] = number++;
			}
			for (int i = 0; i < levelLength; i++) {
				result[n - 1 - level - i][level] = number++;
			}
		}
		if (maxLevel * 2 < n) {
			result[n / 2][n / 2] = number;
		}
		return result;
	}
	
	public int[][] spiralGenerate2(int m, int n) {
		int[][] result = new int[m][n];
		helper(result, m, n, 0, 1);
		return result;
	}

	private void helper(int[][] result, int m, int n, int level, int number) {
		if (m <= 1 || n <= 1) {
			if (m == 1) {
				for (int i = 0; i < n; i++) {
					result[level][level + i] = number++;
				}
			} else if (n == 1) {
				for (int i = 0; i < m; i++) {
					result[level + i][level] = number++;
				}
			}
			return;
		}

		for (int i = 0; i < n - 1; i++) {
			result[level][level + i] = number++;
		}
		for (int i = 0; i < m - 1; i++) {
			result[level + i][level + n - 1] = number++;
		}
		for (int i = 0; i < n - 1; i++) {
			result[level + m - 1][level + n - 1 - i] = number++;
		}
		for (int i = 0; i < m - 1; i++) {
			result[level + m - 1 - i][level] = number++;
		}
		helper(result, m - 2, n - 2, level + 1, number);
	}

}
