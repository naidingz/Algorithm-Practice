package laicode;

public class SuperUglyNumber {

    public static void main(String[] args) {
        SuperUglyNumber test = new SuperUglyNumber();
        System.out.println(test.nthSuperUglyNumber(12, new int[] {2,7,13,19}));
    }

    public int nthSuperUglyNumber(int n, int[] primes) {
        int[] uglys = new int[n];
        uglys[0] = 1;
        int[] ptr = new int[primes.length];

        for (int i = 1; i < n; i++) {
            int min = uglys[ptr[0]] * primes[0], index = 0;
            for (int j = 1; j < primes.length; j++) {
                int next = uglys[ptr[j]] * primes[j];
                if (next < min) {
                    min = next;
                    index = j;
                }
            }
            ptr[index]++;
            if (min == uglys[i-1]) {
                i--;
            } else {
                uglys[i] = min;
            }
        }
        return uglys[n-1];
    }
}
