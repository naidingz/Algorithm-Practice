package laicode;

import java.util.PriorityQueue;
import java.util.Queue;

public class KthSmallestSumInTwoSortedArray {

    /*
     * Given two sorted arrays A and B, of sizes m and n respectively.
     * Define s = a + b, where a is one element from A and b is one element from B.
     * Find the Kth smallest s out of all possible s'.
     */
    public int kthSum(int[] A, int[] B, int k) {
        if (A == null && B == null) {
            return 0;
        }
        if (A == null || B == null) {
            return A == null ? B[k - 1] : A[k - 1];
        }

        int m = A.length, n = B.length;
        boolean[][] visited = new boolean[m][n];

        Queue<ArrayElement> pq = new PriorityQueue<>();
        pq.offer(new ArrayElement(0, 0, A[0] + B[0]));

        int count = 0;
        while (!pq.isEmpty()) {
            ArrayElement curr = pq.poll();
            if (visited[curr.row][curr.col]) {
                continue;
            }
            count++;
            if (count == k) {
                return curr.value;
            }
            if (curr.row < m - 1) {
                pq.offer(new ArrayElement(curr.row + 1, curr.col, A[curr.row + 1] + B[curr.col]));
            }
            if (curr.col < n - 1) {
                pq.offer(new ArrayElement(curr.row, curr.col + 1, A[curr.row] + B[curr.col + 1]));
            }
            visited[curr.row][curr.col] = true;
        }
        return -1;
    }

    class ArrayElement implements Comparable<ArrayElement> {
        final int row;
        final int col;
        final int value;

        public ArrayElement(int r, int c, int v) {
            row = r;
            col = c;
            value = v;
        }

        public int compareTo(ArrayElement a) {
            if (this.value > a.value) {
                return 1;
            } else if (this.value < a.value) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
