package laicode;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Bipartite {

	public static void main(String[] args) {
		GraphNode g1 = new GraphNode(1);
		GraphNode g2 = new GraphNode(2);
		GraphNode g3 = new GraphNode(3);
		GraphNode g4 = new GraphNode(4);

		g1.neighbors = new ArrayList<GraphNode>() {{
			add(g2);
		}};
		
		g2.neighbors = new ArrayList<GraphNode>() {{
			add(g1);
			add(g3);
			add(g4);
		}};
		
		g3.neighbors = new ArrayList<GraphNode>() {{
			add(g2);
			add(g4);
		}};
		
		g4.neighbors = new ArrayList<GraphNode>() {{
			add(g3);
			add(g2);
		}};
		Bipartite test = new Bipartite();

		System.out.println(test.isBipartite(new ArrayList<GraphNode>() {{
			add(g1);
		}}));
	}
	
	public static class GraphNode {
		public int key;
		public List<GraphNode> neighbors;

		public GraphNode(int key) {
			this.key = key;
			this.neighbors = new ArrayList<GraphNode>();
		}
	}
	 
	
	public boolean isBipartite(List<GraphNode> graph) {
		if (graph == null) {
			return false;
		}
		
		HashSet<GraphNode> set1 = new HashSet<>();
		HashSet<GraphNode> set2 = new HashSet<>();
		HashSet<GraphNode> visited = new HashSet<>();
		for (GraphNode gn : graph) {
			Queue<GraphNode> q = new LinkedList<>();
			if (!visited.contains(gn)) {
				q.offer(gn);
			}
			while (!q.isEmpty()) {
				GraphNode node = q.poll();
				HashSet<GraphNode> inSet = null;
				HashSet<GraphNode> outSet = null;
				if (set2.contains(node)) {
					inSet = set2;
					outSet = set1;
				} else {
					inSet = set1;
					outSet = set2;
					inSet.add(node);
				}
				for (GraphNode nei : node.neighbors) {
					if (!visited.contains(nei)) {
						q.add(nei);
					}
					if (inSet.contains(nei)) {
						return false;
					} else if (!outSet.contains(nei)) {
						outSet.add(nei);
					}
				}
				visited.add(node);
			}
		}
		return true;
	}

}
