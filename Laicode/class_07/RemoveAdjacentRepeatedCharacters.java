package laicode.class_07;

import java.util.Deque;
import java.util.LinkedList;

public class RemoveAdjacentRepeatedCharacters {

    public static void main(String[] args) {
        String s = "aa";
        RemoveAdjacentRepeatedCharacters test = new RemoveAdjacentRepeatedCharacters();
        System.out.println(test.deDup4(s));
    }

    /*
     * Remove adjacent, repeated characters in a given string,
     * leaving only one character for each group of such characters.
     */
    public String deDup(String input) {
        StringBuilder sb = new StringBuilder();
        if (input == null || input.length() == 0) {
            return sb.toString();
        }

        char[] inputChars = input.toCharArray();
        int slow = 0, fast = 1;
        while (fast < inputChars.length) {
            if (inputChars[fast] != inputChars[slow]) {
                sb.append(inputChars[slow++]);
                inputChars[slow] = inputChars[fast];
            }
            fast++;
        }
        sb.append(inputChars[slow]);
        return sb.toString();
    }

    /*
     * Repeatedly remove all adjacent, repeated characters in a given string from left to right.
     * No adjacent characters should be identified in the final string.
     */
    public String deDup4(String input) {
        StringBuilder sb = new StringBuilder();
        if (input == null || input.length() == 0) {
            return sb.toString();
        }

        char[] inputChars = input.toCharArray();
        Deque<Character> stack = new LinkedList<>();
        int ptr = 0;
        while (ptr < inputChars.length) {
            if (stack.isEmpty()) {
                stack.offerFirst(inputChars[ptr++]);
            } else if (inputChars[ptr] != stack.peekFirst()) {
                stack.offerFirst(inputChars[ptr++]);
            } else {
                while (ptr < inputChars.length && inputChars[ptr] == stack.peekFirst()) {
                    stack.offerFirst(inputChars[ptr]);
                    ptr++;
                }
                deDupStack(stack);
            }
        }

        while (!stack.isEmpty()) {
            sb.append(stack.pollLast());
        }

        return sb.toString();
    }

    private void deDupStack(Deque<Character> stack) {
        if (stack.isEmpty() || stack.size() <= 1) {
            return;
        }
        char tmp = stack.pollFirst();
        if (tmp != stack.peekFirst()) {
            stack.offerFirst(tmp);
        } else {
            while (!stack.isEmpty() && tmp == stack.peekFirst()) {
                tmp = stack.pollFirst();
            }
        }
    }
}
