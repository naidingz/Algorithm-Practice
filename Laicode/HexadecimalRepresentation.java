package laicode;

public class HexadecimalRepresentation {
    public static void main(String[] args) {
        int s = 0;
        HexadecimalRepresentation test = new HexadecimalRepresentation();
        System.out.println(test.hex(s));
    }

    public String hex(int number) {
        if (number == 0) {
            return "0x0";
        }

        StringBuilder sb = new StringBuilder();
        sb.append("0x");
        for (int i = 28; i >= 0; i -= 4) {
            int four = ((number >> i) & 15);
            sb.append(convert(four));
        }
        while (sb.length() > 2 && sb.charAt(2) == '0') {
            sb.deleteCharAt(2);
        }
        return sb.toString();
    }

    private char convert(int number) {
        if (number >= 0 && number <= 9) {
            return (char)(number + 48);
        } else if (number <= 15) {
            return (char)(number + 55);
        } else {
            return '-';
        }
    }


}
