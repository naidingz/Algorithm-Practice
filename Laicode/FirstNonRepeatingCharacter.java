package laicode;

import java.util.HashMap;
import java.util.Map;

public class FirstNonRepeatingCharacter {

	private class ListNode {
		char value;
		ListNode prev;
		ListNode next;

		ListNode(char v) {
			value = v;
		}
	}

	private ListNode head;
	private ListNode tail;
	private Map<Character, ListNode> lookup;

	public FirstNonRepeatingCharacter() {
			lookup = new HashMap<>();
		}

	public void read(char ch) {
		ListNode node = null;
		if (lookup.containsKey(ch)) {
			node = lookup.get(ch);
			if (node != null) {
				delete(node);
				node = null;
			}
		} else {
			node = new ListNode(ch);
			insert(node);
		}
		lookup.put(ch, node);
	}

	public Character firstNonRepeating() {
		return head == null ? null : head.value;
	}

	private void delete(ListNode node) {
		if (node.prev != null) {
			node.prev.next = node.next;
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		}
		if (head == node) {
			head = head.next;
		}
		if (tail == node) {
			tail = tail.prev;
		}
		node.next = node.prev = null;
	}

	private void insert(ListNode node) {
		if (head == null) {
			head = tail = node;
		} else {
			tail.next = node;
			node.prev = tail;
			tail = tail.next;
		}
	}
}
