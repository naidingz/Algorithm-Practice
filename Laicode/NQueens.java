package laicode;

import java.util.ArrayList;
import java.util.List;

public class NQueens {

	public static void main(String[] args) {
		NQueens test = new NQueens();
		for (List<Integer> l : test.nqueens(15)) {
			System.out.println(l);
		}
	}

	public List<List<Integer>> nqueens(int n) {
		List<List<Integer>> result = new ArrayList<>();
		helper(0, new ArrayList<Integer>(), n, result);
		return result;
	}
	
	private void helper(int curr, List<Integer> oneSol, int n, List<List<Integer>> result) {
		if (curr == n) {
			result.add(new ArrayList<Integer>(oneSol));
		}
		for (int i = 0; i < n; i++) {
			if (isValid(oneSol, i)) {
				oneSol.add(i);
				helper(curr + 1, oneSol, n, result);
				oneSol.remove(oneSol.size() - 1);
			}
		}
	}
	
	private boolean isValid(List<Integer> sol, int col) {
		int currRow = sol.size();
		for (int row = 0; row < sol.size(); row++) {
			if (sol.get(row) == col) {
				return false;
			} else if (Math.abs(sol.get(row) - col) == Math.abs(row - currRow)) {
				return false;
			}
		}
		return true;
	}
}
