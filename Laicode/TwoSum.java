package laicode;

public class TwoSum {

    public static void main(String[] args) {
        int[] array = {4, 2, 3};
        System.out.println(existSum(array, 4));

    }
    
    public static boolean existSum(int[] array, int target) {
        if (array == null || array.length == 0) {
            return false;
        }
        int minVal = array[0];
        int maxVal = array[0];
        for (int i : array) {
            if (i < minVal) {
                minVal = i;
            }
            if (i > maxVal) {
                maxVal = i;
            }
        }
        
        int shiftVal = Math.min(0, minVal);
        int[] helperArray = new int[maxVal - shiftVal + 1];
        for (int i = 0; i < array.length; i++) {
            helperArray[array[i] - shiftVal] += 1;
        }
        for (int i = 0; i < helperArray.length; i++) {
            System.out.print(helperArray[i] + " ");
        }
        System.out.println();
        
        for (int i = 0; i < array.length; i++) {
            int toFind = target - array[i];
            if (toFind - shiftVal >= 0 && toFind - shiftVal < helperArray.length) {
                if (toFind == array[i] && helperArray[toFind - shiftVal] > 1) {
                    return true;
                }
                if (toFind != array[i] && helperArray[toFind - shiftVal] == 1) {
                    return true;
                }
            }
        }
        return false;
    }

}
