package laicode;

public class CuttingWood {

	public static void main(String[] args) {
		CuttingWood test =new CuttingWood();
		System.out.println(test.minCost(new int[] {2, 4, 7}, 10));
		
	}

	/*
	 * There is a wooden stick with length L >= 1, we need to cut it into pieces, 
	 * where the cutting positions are defined in an int array A. 
	 * The positions are guaranteed to be in ascending order in the range of [1, L - 1]. 
	 * The cost of each cut is the length of the stick segment being cut. 
	 * Determine the minimum total cost to cut the stick into the defined pieces.
	 * 
	 * Example: L = 10, A = {2, 4, 7}, 
	 * the minimum total cost is 10 + 4 + 6 = 20 
	 * (cut at 4 first then cut at 2 and cut at 7)
	 */
	public int minCost(int[] cuts, int length) {
		if (length == 0 || cuts == null || cuts.length == 0) {
			return 0;
		}
		
		int[] fullCuts = new int[cuts.length + 2];
		int N = fullCuts.length;
		for (int i = 1; i <= cuts.length; i++) {
			fullCuts[i] = cuts[i - 1];
		}
		fullCuts[N - 1] = length;
		
		int[][] M = new int[N - 1][N];
		for (int i = N - 2; i >= 0; i--) {
			for (int j = i + 2; j < N; j++) {
				int cost = Integer.MAX_VALUE;
				for (int k = i + 1; k < j; k++) {
					cost = Math.min(cost, M[i][k] + M[k][j] + fullCuts[j] - fullCuts[i]);
				}
				M[i][j] = cost;
			}
		}
		return M[0][N - 1];
	}
}
