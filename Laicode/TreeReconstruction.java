package laicode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreeReconstruction {

	public class TreeNode {
		public int key;
		public TreeNode left;
		public TreeNode right;

		public TreeNode(int key) {
			this.key = key;
		}
	}
	
	/*
	 * Given the preorder and inorder traversal sequence of a binary tree, 
	 * reconstruct the original tree.
	 * The given sequences are not null and they have the same length
	 * There are no duplicate keys in the binary tree
	 */
	
	/*
	public TreeNode reconstruct(int[] inOrder, int[] preOrder) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < inOrder.length; i++) {
			map.put(inOrder[i], i);
		}
		return reconstruct(inOrder, 0, inOrder.length - 1, preOrder, 0, preOrder.length - 1, map);
	}

	private TreeNode reconstruct(int[] in, int inLeft, int inRight, int[] pre, int preLeft, int preRight,
			HashMap<Integer, Integer> idxMap) {
		if (inLeft > inRight) {
			return null;
		}
		TreeNode root = new TreeNode(pre[preLeft]);
		int leftSize = idxMap.get(root.key) - inLeft;

		root.left = reconstruct(in, inLeft, inLeft + leftSize - 1, pre, preLeft + 1, preLeft + leftSize, idxMap);
		root.right = reconstruct(in, inLeft + leftSize + 1, inRight, pre, preLeft + leftSize + 1, preRight, idxMap);
		return root;
	}
	*/
	
	/*
	 * Given the postorder and inorder traversal sequence of a binary tree, 
	 * reconstruct the original tree.
	 * The given sequences are not null and they have the same length
	 * There are no duplicate keys in the binary tree
	 */
	/*
	public TreeNode reconstruct(int[] inOrder, int[] postOrder) {
		HashMap<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < inOrder.length; i++) {
			map.put(inOrder[i], i);
		}
		return reconstruct(inOrder, 0, inOrder.length - 1, postOrder, 0, postOrder.length - 1, map);
	}
	
	private TreeNode reconstruct(int[] in, int inLeft, int inRight,
								 int[] post, int postLeft, int postRight,
								 HashMap<Integer, Integer> idxMap) {
		if (inLeft > inRight) {
			return null;
		}
		TreeNode root = new TreeNode(post[postRight]);
		int leftSize = idxMap.get(root.key) - inLeft;
		
		root.left = reconstruct(in, inLeft, inLeft + leftSize - 1, post, postLeft, postLeft + leftSize - 1, idxMap);
		root.right = reconstruct(in, inLeft + leftSize + 1, inRight, post, postLeft + leftSize, postRight - 1, idxMap);
		return root;
	}
	*/
	
	/*
	 * Given the levelorder and inorder traversal sequence of a binary tree, 
	 * reconstruct the original tree.
	 * The given sequences are not null and they have the same length
	 * There are no duplicate keys in the binary tree
	 */
	public TreeNode reconstruct(int[] inOrder, int[] levelOrder) {
		List<Integer> level = new ArrayList<>();
		Map<Integer, Integer> inMap = new HashMap<>();
		for (int i = 0; i < inOrder.length; i++) {
			level.add(levelOrder[i]);
			inMap.put(inOrder[i], i);
		}
		return reconstruct(level, inMap);
	}
	
	private TreeNode reconstruct(List<Integer> level, Map<Integer, Integer> inMap) {
		if (level.isEmpty()) {
			return null;
		}

		TreeNode root = new TreeNode(level.remove(0));
		List<Integer> left = new ArrayList<>();
		List<Integer> right = new ArrayList<>();

		for (Integer i : level) {
			if (inMap.get(i) < inMap.get(root.key)) {
				left.add(i);
			} else {
				right.add(i);
			}
		}

		root.left = reconstruct(left, inMap);
		root.right = reconstruct(right, inMap);
		return root;
	}
}
