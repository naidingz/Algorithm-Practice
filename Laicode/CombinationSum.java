package laicode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSum {

	public static void main(String[] args) {
		CombinationSum test = new CombinationSum();
		for (List<Integer> l : test.combinationSum2(new int[] {10, 1, 2, 7, 6, 1, 5}, 8)) {
			System.out.println(l);
		}
	}
	
	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<>();
		helper(target, new ArrayList<Integer>(), 0, candidates, result);
		return result;
	}

	private void helper(int remain, List<Integer> sol, int start, int[] candidates, List<List<Integer>> result) {
		if (remain == 0) {
			result.add(new ArrayList<Integer>(sol));
			return;
		} else if (start == candidates.length) {
			return;
		}
		
		helper(remain, sol, start + 1, candidates, result);
		int count = 1;
		while (remain - candidates[start] * count >= 0) {
			sol.add(candidates[start]);
			helper(remain - candidates[start] * count, sol, start + 1, candidates, result);
			count++;
		}
		while (count > 1) {
			sol.remove(sol.size() - 1);
			count--;
		}
	}

	public List<List<Integer>> combinationSum2(int[] num, int target) {
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(num);
		helper2(target, 0, num, new ArrayList<Integer>(), result);
		return result;
	}

	private void helper2(int remain, int index, int[] num, List<Integer> sol, List<List<Integer>> result) {
		if (remain == 0) {
			result.add(new ArrayList<Integer>(sol));
			return;
		} else if (index >= num.length) {
			return;
		}

		sol.add(num[index]);
		helper2(remain - num[index], index + 1, num, sol, result);
		while (index + 1 < num.length && num[index + 1] == num[index]) {
			index++;
		}
		sol.remove(sol.size() - 1);
		helper2(remain, index + 1, num, sol, result);
	}
	
}
