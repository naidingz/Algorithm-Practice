package laicode.class_08;

public class DecompressString {
    public static void main(String[] args) {
        String s = "a1b2c3";
        DecompressString test = new DecompressString();
        System.out.println(test.decompress(s));
    }


    public String decompress(String input) {
        StringBuilder sb = new StringBuilder();
        if (input == null) {
            return sb.toString();
        }

        for (int i = 0; i < input.length(); i+=2) {
            for (int j = 0; j < input.charAt(i+1) - 48; j++) {
                sb.append(input.charAt(i));
            }
        }
        return sb.toString();
    }
}
