import java.util.ArrayList;
import java.util.Map;
import java.util.Queue;
import java.util.LinkedList;
import java.util.HashMap;

public class TopologicalSort {

    public static void main(String[] args) {
        DirectedGraphNode node0 = new DirectedGraphNode(0);
        DirectedGraphNode node1 = new DirectedGraphNode(1);
        DirectedGraphNode node2 = new DirectedGraphNode(2);
        DirectedGraphNode node3 = new DirectedGraphNode(3);
        DirectedGraphNode node4 = new DirectedGraphNode(4);
        DirectedGraphNode node5 = new DirectedGraphNode(5);

        node0.neighbors.add(node1);
        node0.neighbors.add(node2);
        node0.neighbors.add(node3);
        node1.neighbors.add(node4);
        node2.neighbors.add(node4);
        node2.neighbors.add(node5);
        node3.neighbors.add(node4);
        node3.neighbors.add(node5);

        ArrayList<DirectedGraphNode> graph = new ArrayList<>();
        graph.add(node0);

        ArrayList<DirectedGraphNode> result = new TopologicalSort().topSort(graph);
        for (DirectedGraphNode node : result) {
            System.out.println(node.label);
        }
    }

    public static class DirectedGraphNode {
        int label;
        ArrayList<DirectedGraphNode> neighbors;

        DirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<DirectedGraphNode>();
        }
    }

    public ArrayList<DirectedGraphNode> topSort(ArrayList<DirectedGraphNode> graph) {
        ArrayList<DirectedGraphNode> result = new ArrayList<>();
        Map<DirectedGraphNode, Integer> inDegree = new HashMap<>();
        getInDegree(graph, inDegree);

        Queue<DirectedGraphNode> queue = new LinkedList<>();
        for (Map.Entry<DirectedGraphNode, Integer> entry : inDegree.entrySet()) {
            if (entry.getValue() == 0) {
                queue.offer(entry.getKey());
            }
        }

        while (!queue.isEmpty()) {
            DirectedGraphNode node = queue.poll();
            result.add(node);
            for (DirectedGraphNode nei : node.neighbors) {
                Integer in = inDegree.get(nei);
                inDegree.put(nei, in - 1);
                if (in - 1 == 0) {
                    queue.offer(nei);
                }
            }
        }

        return result;
    }


    private void getInDegree(ArrayList<DirectedGraphNode> graph, Map<DirectedGraphNode, Integer> map) {
        Queue<DirectedGraphNode> queue = new LinkedList<>();
        for (DirectedGraphNode root : graph) {
            if (!map.containsKey(root)) {
                queue.offer(root);
                map.put(root, 0);
            }
            while (!queue.isEmpty()) {
                DirectedGraphNode node = queue.poll();
                for (DirectedGraphNode nei : node.neighbors) {
                    Integer in = map.get(nei);
                    if (in == null) {
                        queue.offer(nei);
                        map.put(nei, 1);
                    } else {
                        map.put(nei, in + 1);
                    }
                }
            } 
        } 
    }
}