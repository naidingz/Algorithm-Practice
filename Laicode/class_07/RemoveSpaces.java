package laicode.class_07;

public class RemoveSpaces {

    public String removeSpaces(String input) {
        StringBuilder sb = new StringBuilder();
        if (input == null || input.length() == 0) {
            return sb.toString();
        }

        int slow = 0, fast = 0;
        char[] inputChars = input.toCharArray();
        while (fast < inputChars.length) {
            if (inputChars[fast] == ' ') {
                if (slow == 0) {
                    fast++;
                } else if (inputChars[slow-1] == ' '){
                    fast++;
                } else {
                    inputChars[slow++] = inputChars[fast++];
                }
            } else {
                inputChars[slow++] = inputChars[fast++];
            }
        }
        if (slow > 0 && inputChars[slow - 1] == ' ') {
            slow--;
        }
        for (int i = 0; i < slow; i++) {
            sb.append(inputChars[i]);
        }
        return sb.toString();
    }
}
