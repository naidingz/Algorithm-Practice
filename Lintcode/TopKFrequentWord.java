import java.awt.List;
import java.util.HashMap;
import java.util.Map;

public class TopKFrequentWord {

    public String[] topKFrequentWords(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        for (String word : words) {
            map.put(word, map.getOrDefault(word, 0) + 1);
        }

        List<String>[] buckets = new List[words.length + 1];
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            if (buckets[entry.getValue()] == null) {
                buckets[entry.getValue()] = new ArrayList<String>();
            }
            buckets[entry.getValue()].add(entry.getKey());
        }

        List<String> result = new ArrayList<>();
        for (int i = buckets.length - 1; i >= 1; i--) {
            if (buckets[i] != null) {
                if (result.size() + buckets[i].size() <= k) {
                    java.util.Collections.sort(buckets[i]);
                    result.addAll(buckets[i]);
                } else {
                    java.util.Collections.sort(buckets[i]);
                    for (int j = 0; j < buckets[i].size() && result.size() < k; j++) {
                        result.add(buckets[i].get(j));
                    }
                    break;
                }
            }
        }
        String[] resultArr = new String[result.size()];
        resultArr = result.toArray(resultArr);
        return resultArr;
    }
}