package laicode.class_08;

import java.util.ArrayList;
import java.util.List;

public class CompressString {

    public static void main(String[] args) {
        String s = "aaabcccdeeefg";
        CompressString test = new CompressString();
        System.out.println(test.compress(s));
    }

    public String compress(String input) {
        if (input == null) {
            return null;
        }
        char[] inputChars = input.toCharArray();
        List<Integer> index = helpCompress(inputChars);
        char[] result = new char[index.get(index.size() - 1) + index.size() - 1];
        int resultPtr = result.length - 1, inputPtr = index.get(index.size() - 1) - 1, indexPtr = index.size() - 2;
        while (inputPtr >= 0) {
            if (indexPtr >= 0 && inputPtr == index.get(indexPtr)) {
                result[resultPtr--] = 1 + 48;
                result[resultPtr--] = inputChars[inputPtr--];
                indexPtr--;
            } else {
                result[resultPtr--] = inputChars[inputPtr--];
                result[resultPtr--] = inputChars[inputPtr--];
            }
        }
        return new String(result);
//        return new String(inputChars, 0, index.get(index.size() - 1));
    }

    private List<Integer> helpCompress(char[] input) {
        // the last element of result is the length
        List<Integer> result = new ArrayList<>();
        int slow = 0, fast = 0;
        while (fast < input.length) {
            int count = 1;
            while (fast + 1 < input.length && input[fast] == input[fast + 1]) {
                fast++;
                count++;
            }
            if (count == 1) {
                result.add(slow);
                input[slow++] = input[fast++];
            } else {
                input[slow++] = input[fast++];
                input[slow++] = (char) (count + 48);
            }
        }
        result.add(slow);
        return result;
    }
}
