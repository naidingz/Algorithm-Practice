package laicode.class_05;

public class SearchInUnknownSizeSortedArray {
    
    public static int search(Dictionary dict, int target) {
        if (dict == null || dict.get(0) == null) {
            return -1;
        }
        
        if (dict.get(0) == target) {
            return 0;
        }
        
        int move = 1;
        while (dict.get(move) != null) {
            if (dict.get(move) == target) {
                return move;
            } else if (dict.get(move) < target) {
                move *= 2;
            } else {
                break;
            }
        }
        
        int left = move / 2, right = move;
        while (right >= left) {
            int mid = left + (right - left) / 2;
            if (dict.get(mid) == null) {
                right = mid - 1;
            } else if (dict.get(mid) == target) {
                return mid;
            } else if (dict.get(mid) < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        
        return -1;
    }

}

interface Dictionary {
    public Integer get(int index);
}