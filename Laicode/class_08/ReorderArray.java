package laicode.class_08;

public class ReorderArray {
    public int[] reorder(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }

        int[] result = new int[array.length];
        int i = 0, j = result.length / 2, k = 0;
        while ( i < result.length / 2) {
            result[k++] = array[i++];
            result[k++] = array[j++];
        }
        if (j < array.length) {
            result[k] = array[j];
        }
        return result;
    }
}
