package laicode.class_08;

public class RightShiftString {

    public static void main(String[] args) {
        String s = "zhounaiding";
        RightShiftString test = new RightShiftString();
        System.out.println(test.rightShift(s, 18));
    }

    public String rightShift(String input, int n) {
        if (input == null || n == 0 || input.length() == 0) {
            return input;
        }

        char[] inputChars = input.toCharArray();
        n = n % inputChars.length;
        reverseBetween(inputChars, 0, inputChars.length - 1);
        reverseBetween(inputChars, 0, n - 1);
        reverseBetween(inputChars, n, inputChars.length - 1);
        return new String(inputChars);
    }

    private void reverseBetween(char[] inputChars, int left, int right) {
        while (right > left) {
            char tmp = inputChars[left];
            inputChars[left] = inputChars[right];
            inputChars[right] = tmp;
            left++;
            right--;
        }
    }
}
