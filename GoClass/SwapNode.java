import ds.*;

public class SwapNode {
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;

        node1.printList();

        SwapNode test = new SwapNode();
        test.swapNodes(node1, 3, 4).printList();;
    }

    public ListNode swapNodes(ListNode head, int v1, int v2) {
        // write your code here
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        
        ListNode prev1 = null, prev2 = null;
        ListNode curr = dummy;
        while (curr.next != null) {
            if (curr.next.val == v1) {
                prev1 = curr;
            }
            if (curr.next.val == v2) {
                prev2 = curr;
            }
            curr = curr.next;
        }
        
        if (prev1 == null || prev2 == null) {
            return head;
        }
        
        ListNode n1 = prev1.next, n2 = prev2.next;
        ListNode next1 = n1.next, next2 = n2.next;
        
        if (prev2 == prev1.next) {
            prev1.next = n2;
            n2.next = n1;
            n1.next = next2;
        } else if (prev1 == prev2.next) {
            prev2.next = n1;
            n1.next = n2;
            n2.next = next1;
        } else {
            prev1.next = n2;
            n2.next = n1.next;

            prev2.next = n1;
            n1.next = next2;
        }

        return dummy.next;
    }
}