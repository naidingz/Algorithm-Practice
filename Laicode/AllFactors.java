package laicode;

import java.util.*;

public class AllFactors {
    public static void main(String[] args) {
        AllFactors test = new AllFactors();
        for (List<Integer> l : test.factors(12)) {
            System.out.println(l);
        }
    }

    public List<List<Integer>> factors(int n) {
    	List<List<Integer>> result = new ArrayList<>();
        List<Integer> oneSol = new ArrayList<>();
        helper(n, oneSol, result);
        return result;
    }

	private void helper(int remain, List<Integer> oneSol, List<List<Integer>> result) {
		if (remain == 1) {
			result.add(new ArrayList<Integer>(oneSol));
			return;
		}

		int size = oneSol.size();
		for (int i = size == 0 ? 2 : oneSol.get(size - 1); i <= remain; i++) {
			if (remain % i == 0) {
				oneSol.add(i);
				helper(remain / i, oneSol, result);
				oneSol.remove(oneSol.size() - 1);
			}
		}
	}
}
