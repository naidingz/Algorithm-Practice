package dp;
import java.util.HashMap;

public class Fibonacci {

    public static void main(String[] args) {
        Fibonacci test = new Fibonacci();

        for (int i = 0; i < 10; i++) {
            System.out.println(test.fib(i));
        }
    }

	public int fib(int n) {
		HashMap<Integer, Integer> map = new HashMap<>();
		fib(n, map);
		return map.get(n);
	}

	private int fib(int n, HashMap<Integer, Integer> map) {
		if (n == 0 || n == 1) {
			map.put(n, n);
			return n;
		}
		int a = fib(n - 1, map);
		int b = fib(n - 2, map);
		map.put(n, a + b);
		return a + b;
	}
}