public class Inversions {

    public static void main(String[] args) {
        Integer[] array = {1,7,5,4,6,2};
        System.out.println(Inversions.count(array));
    }
    
    private static Comparable[] aux;
    private static Integer num;
    public static int count(Comparable[] array) {
        
        int N = array.length;
        aux = new Comparable[N];
        num = 0;
        for (int sz = 1; sz < N; sz = sz+sz) {
            for (int lo = 0; lo < N-sz; lo += sz+sz) {
                merge(array, lo, lo+sz-1, Math.min(lo+sz+sz-1, N-1));
            }
        }
        
        return num;
    }
    
    private static void merge(Comparable[] array, int left, int mid, int right) {
        for (int i = left; i <= right; i++) {
            aux[i] = array[i];
        }
        
        int i = left, j = mid+1;
        for (int k = left; k <= right; k++) {
            if (i > mid) {
                array[k] = aux[j++];
            } else if (j > right) {
                array[k] = aux[i++];
            } else if (less(aux[j], aux[i])) {
                array[k] = aux[j++];
                num += mid-i+1;
            } else {
                array[k] = aux[i++];
            }
        }
    }
    
    private static boolean less(Comparable v, Comparable w) {
        return v.compareTo(w) < 0;
    }
}
