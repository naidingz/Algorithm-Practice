package laicode;

import java.util.ArrayList;
import java.util.List;

public class AllCombinationsOfIfBlock {

	public static void main(String[] args) {
		AllCombinationsOfIfBlock test = new AllCombinationsOfIfBlock();
		test.printBlocks(3);
	}
	
	public void printBlocks(int n) {
		helper(0, 0, 0, n, new ArrayList<String>());
	}
	
	private void helper(int left, int right, int indent, int n, List<String> sol) {
		if (left + right == n + n) {
			if (left == right) {
				for (String s : sol) {
					System.out.println(s);
				}
				System.out.println();
			}
			return;
		}
		
		if (left < n) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indent; i++) {
				sb.append(" ");
			}
			sol.add(sb.toString() + "if {");
			helper(left + 1, right, indent + 2, n, sol);
			sol.remove(sol.size() - 1);
		}
		
		if (right < left) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < indent - 2; i++) {
				sb.append(" ");
			}
			sol.add(sb.toString() + "}");
			helper(left, right + 1, indent - 2, n, sol);
			sol.remove(sol.size() - 1);
		}
	}

}
