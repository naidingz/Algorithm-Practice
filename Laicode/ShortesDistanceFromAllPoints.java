package laicode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ShortesDistanceFromAllPoints {

	public static void main(String[] args) {
		ShortesDistanceFromAllPoints test = new ShortesDistanceFromAllPoints();
		System.out.println(test.shortestDistance(new int[][] {{1,2,0}}));
		
	}

	private class Node {
		private int i;
		private int j;
		public Node(int i, int j) {
			this.i = i;
			this.j = j;
		}
	}
	
	public int shortestDistance(int[][] grid) {
		int M = grid.length;
		int N = grid[0].length;
		int[][] cost = new int[M][N];
		
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				if (grid[i][j] == 1) {
					if (!addCost(grid, cost, i, j)) {
						return -1;
					}
				}
			}
		}
		
		int globalMin = Integer.MAX_VALUE;
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				if (grid[i][j] == 0 && cost[i][j] != 0) {
					globalMin = Math.min(globalMin, cost[i][j]);
				}
			}
		}
		
		return globalMin == Integer.MAX_VALUE ? -1 : globalMin;
 	}
	
	private boolean addCost(int[][] grid, int[][] cost, int i, int j) {
		Queue<Node> q = new LinkedList<>();
		boolean[][] visited = new boolean[grid.length][grid[0].length];
		int pathCost = 1;
		q.offer(new Node(i, j));
		visited[i][j] = true;
		
		while (!q.isEmpty()) {
			int size = q.size();
			for (int l = 0; l < size; l++) {
				Node node = q.poll();
				for (Node nei : getNeis(node, grid)) {
					if (!visited[nei.i][nei.j]) {
						cost[nei.i][nei.j] += pathCost;
						visited[nei.i][nei.j] = true;
						q.offer(nei);
					}
				}
			}
			pathCost++;
		}
		
		for (int m = 0; m < grid.length; m++) {
			for (int n = 0; n < grid[0].length; n++) {
				if (!visited[m][n] && grid[m][n] == 1) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	private List<Node> getNeis(Node curr, int[][] grid) {
		int x = curr.i;
		int y = curr.j;
		int M = grid.length;
		int N = grid[0].length;
		List<Node> neis = new ArrayList<>();
		if (x + 1 < M && 2 != grid[x + 1][y]) {
			neis.add(new Node(x + 1, y));
		}
		if (y + 1 < N && 2 != grid[x][y + 1]) {
			neis.add(new Node(x, y + 1));
		}
		if (x - 1 >= 0 && 2 != grid[x - 1][y]) {
			neis.add(new Node(x - 1, y));
		}
		if (y - 1 >= 0 && 2 != grid[x][y - 1]) {
			neis.add(new Node(x, y - 1));
		}
		return neis;
	}
}
