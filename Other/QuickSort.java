import java.lang.Math;

public class QuickSort {
    public static void main(String[] args) {
        QuickSort test = new QuickSort();
        int[] array = {1,6,3,5,2,7};
        test.sort(array);
        for (Integer i : array) {
            System.out.print(" " + i);
        }
    }

    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int[] array, int left, int right) {
        if (left >= right) {
            return ;
        }

        int mid = partition(array, left, right);
        sort(array, left, mid - 1);
        sort(array, mid + 1, right);
    }

    private int partition(int[] array, int left, int right) {
        int pivot = left + (int) Math.random() * (right - left + 1);
        swap(array, right, pivot);

        int i = left, j = right - 1;
        while (i <= j) {
            if (array[i] < array[right]) {
                i++;
            } else {
                swap(array, i, j--);
            }
        }
        swap(array, i, right);
        return i;
    }

    private void swap(int[] array, int left, int right) {
        int temp = array[left];
        array[left] = array[right];
        array[right] = temp;
    }
}