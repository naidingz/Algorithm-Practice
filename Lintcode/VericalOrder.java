import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class VerticalOrder {
    public static void main(String[] args) {
        
    }

    class TreeNode {
        public int val;
        public TreeNode left, right;
        public TreeNode(int val) {
            this.val = val;
            this.left = this.right = null;
        }
    }

    /*
     * DFS Solution
     */
    // public List<List<Integer>> verticalOrder(TreeNode root) {
    //     List<List<Integer>> result = new ArrayList<>();
    //     Map<Integer, List<Integer>> map = new HashMap<>();
    //     int[] range = new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE};
    //     helper(root, 0, map, range);
    //     for (int i = range[0]; i <= range[1]; i++) {
    //         result.add(map.get(i));
    //     }
    //     return result;
    // }

    // private void helper(TreeNode root, int column, Map<Integer, List<Integer>> map, int[] range) {
    //     if (root == null) {
    //         return ;
    //     }
    //     List<Integer> list = map.get(column);
    //     if (list == null) {
    //         list = new ArrayList<Integer>();
    //     }
    //     list.add(root.val);
    //     map.put(column, list);
    //     range[0] = Math.min(range[0], column);
    //     range[1] = Math.max(range[1], column);
    //     helper(root.left, column - 1, map, range);
    //     helper(root.right, column + 1, map, range);
    // }

    /*
     * BFS Solution
     */
    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        Map<Integer, List<Integer>> map = new HashMap<>();
        int[] range = new int[]{Integer.MAX_VALUE, Integer.MIN_VALUE};

        Queue<TreeNode> nodeQ = new LinkedList<>();
        Queue<Integer> colQ = new LinkedList<>();
        nodeQ.offer(root);
        colQ.offer(0);
        while (!nodeQ.isEmpty()) {
            TreeNode node = nodeQ.poll();
            Integer col = colQ.poll();

            List<Integer> list = map.get(col);
            if (list == null) {
                list = new ArrayList<Integer>();
            }
            list.add(node.val);
            map.put(col, list);
            range[0] = Math.min(range[0], col);
            range[1] = Math.max(range[1], col);

            if (node.left != null) {
                nodeQ.offer(node.left);
                colQ.offer(col - 1);
            }
            if (node.right != null) {
                nodeQ.offer(node.right);
                colQ.offer(col + 1);
            }
        }
        for (int i = range[0]; i <= range[1]; i++) {
            result.add(map.get(i));
        }
        return result;
    }

}