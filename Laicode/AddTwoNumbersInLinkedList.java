package laicode;

public class AddTwoNumbersInLinkedList {

    public static void main(String[] args) {
        ListNode n1 = new ListNode(9);
        ListNode n2 = new ListNode(9);
        ListNode n3 = new ListNode(9);
        ListNode n4 = new ListNode(9);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;

        ListNode b1 = new ListNode(1);
        ListNode b2 = new ListNode(1);
        ListNode b3 = new ListNode(1);
        ListNode b4 = new ListNode(1);
        b1.next = b2;
        b2.next = b3;
        b3.next = b4;

        AddTwoNumbersInLinkedList test = new AddTwoNumbersInLinkedList();

        test.printLL(test.addTwoNumbers(n1, b1));
    }

    private void printLL(ListNode head) {
        while (head != null) {
            System.out.print(head.value + " ");
            head = head.next;
        }
    }

    private static class ListNode {
        int value;
        ListNode next;

        ListNode(int value) {
            this.value = value;
            next = null;
        }
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null || l2 == null) {
            return l1 == null ? l2 : l1;
        }
        int carry = 0;
        ListNode result = l1;
        while (true) {
            int tmp = l1.value + l2.value + carry;
            carry = tmp >= 10 ? tmp / 10 : 0;
            l1.value = tmp % 10;
            if (l1.next == null || l2.next == null) {
                break;
            } else {
                l1 = l1.next;
                l2 = l2.next;
            }
        }
        if (l1.next == null) {
            l1.next = l2.next;
        }
        if (carry == 0) {
            return result;
        }
        l1.next = addTwoNumbers(l1.next, new ListNode(carry));

        return result;

    }
}
