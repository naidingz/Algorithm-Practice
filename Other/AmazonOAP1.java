import java.util.HashSet;
import java.util.Set;

public class AmazonOAP1 {

	public static void main(String[] args) {
		System.out.println(AmazonOAP1.countKDistinctSubstrings("abafg", 2));
	}
	
	public static int countKDistinctSubstrings(String inputString, int num) {
		int result = 0;
		for (int i = 0; i < inputString.length(); i++) {
			Set<Character> set = new HashSet<>();
			for (int j = i; j < inputString.length(); j++) {
				set.add(inputString.charAt(j));
				if (set.size() == num) {
					result++;
				} else if (set.size() > num) {
					j = inputString.length();
				}
			}
		}
		
		return result;
	}
}
