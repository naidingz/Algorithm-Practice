package laicode;

public class Intersection {

    public static void main(String[] args) {
        int[] array1 = {2, 2};
        int[] array2 = {1,2,2,1};
        
        print(intersect(array1, array2));

    }
    
    private static void print(int[] a) {
        if (a == null) {
            return;
        }
        for (Integer i : a) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
    
    public static int[] intersect(int[] nums1, int[] nums2) {
        if (nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0) {
            return new int[0];
        }
        
        mergeSort(nums1);
        mergeSort(nums2);
        
        int[] aux = new int[Math.min(nums1.length, nums2.length)];
        for (int i = 0; i < aux.length; i++) {
            aux[i] = Integer.MIN_VALUE;
        }
        int i = 0, j = 0, k = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] == nums2[j]) {
                aux[k++] = nums1[i];
                i++; j++;
//                while (i < nums1.length && aux[k-1] == nums1[i]) {
//                    i++;
//                }
//                while (j < nums2.length && aux[k-1] == nums2[j]) {
//                    j++;
//                }
            } else if (nums1[i] < nums2[j]) {
                i++;
            } else {
                j++;
            }
        }
        
        int num = 0;
        for (int ind = 0; ind < aux.length; ind++) {
            if (aux[ind] == Integer.MIN_VALUE) {
                break;
            } else {
                num++;
            }
        }
        
        int[] result = new int[num];
        for (int ind = 0; ind < num; ind++) {
            result[ind] = aux[ind];
        }
          
        return result;
    }
    
    private static void mergeSort(int[] array) {
        int[] auxArray = new int[array.length];
        mergeSort(array, auxArray, 0, array.length - 1);
    }
    
    private static void mergeSort(int[] array, int[] aux, int left, int right) {
        
        if (left == right) {
            return;
        }
        
        int mid = left + (right - left) / 2;
        mergeSort(array, aux, left, mid);
        mergeSort(array, aux, mid+1, right);
        merge(array, aux, left, mid, right);
    }
    
    private static void merge(int[] array, int[] aux, int left, int mid, int right) {
        for (int i = left; i <= right; i++) {
            aux[i] = array[i];
        }
        
        int i = left, j = mid+1;
        for (int k = left; k <= right; k++) {
            if (i > mid) {
                array[k] = aux[j++];
            } else if (j > right) {
                array[k] = aux[i++];
            } else if (aux[j] < aux[i]) {
                array[k] = aux[j++];
            } else {
                array[k] = aux[i++];
            }
        }
    }
}
