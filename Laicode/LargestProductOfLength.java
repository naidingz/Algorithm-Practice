package laicode;

import java.util.Arrays;
import java.util.PriorityQueue;

public class LargestProductOfLength {

	public static void main(String[] args) {
		LargestProductOfLength test = new LargestProductOfLength();
		String[] dictionary = new String[] {"ix","hj","x", "abcdefhi"};
		System.out.println(test.largestProduct(dictionary));
	}
	
	/*
	 * Given a dictionary containing many words, 
	 * find the largest product of two words’ lengths, 
	 * such that the two words do not share any common characters.
	 * 
	 * Assumptions:
	 * The words only contains characters of 'a' to 'z'
	 * The dictionary is not null and does not contains null string, and has at least two strings
	 * If there is no such pair of words, just return 0
	 * 
	 * Examples
	 * dictionary = [“abcde”, “abcd”, “ade”, “xy”], 
	 * the largest product is 5 * 2 = 10 (by choosing “abcde” and “xy”)
	 */
	
	class Node implements Comparable<Node> {
		String s1;
		String s2;
		Integer i;
		Integer j;
		Integer product;
		Node(String s1, String s2, Integer i, Integer j) {
			this.s1 = s1;
			this.s2 = s2;
			this.i = i;
			this.j = j;
			this.product = s1.length() * s2.length();
		}
		
		public int compareTo(Node that) {
			return that.product.compareTo(this.product);
		}
	}
	
	class Pair implements Comparable<Pair> {
		String s;
		int hash;
		Pair(String s, int hash) {
			this.s = s;
			this.hash = hash;
		}
		
		public int compareTo(Pair that) {
			return Integer.compare(that.s.length(), this.s.length());
		}
	}
	
	public int largestProduct(String[] dict) {
		int N = dict.length;
		Pair[] pairDict = getPairDict(dict);
		
		PriorityQueue<Node> pq = new PriorityQueue<>();
		boolean[][] visited = new boolean[N][N];

		pq.offer(new Node(pairDict[0].s, pairDict[1].s, 0, 1));
		visited[0][1] = true;
		visited[1][0] = true;

		while (!pq.isEmpty()) {
			Node pair = pq.poll();
			if ((pairDict[pair.i].hash & pairDict[pair.j].hash) == 0) {
				return pair.product;
			} else {
				if (pair.j + 1 < N && !visited[pair.i][pair.j + 1]) {
					pq.offer(new Node(pairDict[pair.i].s, pairDict[pair.j + 1].s, pair.i, pair.j + 1));
					visited[pair.i][pair.j + 1] = true;
					visited[pair.j + 1][pair.i] = true;
				}
				if (pair.i + 1 < N && !visited[pair.i + 1][pair.j]) {
					pq.offer(new Node(pairDict[pair.i + 1].s, pairDict[pair.j].s, pair.i + 1, pair.j));
					visited[pair.i + 1][pair.j] = true;
					visited[pair.j][pair.i + 1] = true;
				}
			}
		}
		return 0;
	}

	private Pair[] getPairDict(String[] dict) {
		Pair[] result = new Pair[dict.length];
		for (int i = 0; i < dict.length; i++) {
			String s = dict[i];
			int hash = 0;
			for (int j = 0; j < s.length(); j++) {
				hash |= 1 << (int) (s.charAt(j) - 'a');
			}
			result[i] = new Pair(s, hash);
		}
		Arrays.sort(result);
		return result;
	}

}
