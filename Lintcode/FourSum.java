import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FourSum {

	public static void main(String[] args) {
		FourSum test = new FourSum();
		System.out.println(test.fourSum(new int[] {1,0,-1,0,-2,2}, -2));
	}
	class Pair {
		int left;
		int right;
		int leftVal;
		int rightVal;
		public Pair(int left, int right, int leftVal, int rightVal) {
			this.left = left;
			this.right = right;
			this.leftVal = leftVal;
			this.rightVal = rightVal;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + leftVal;
			result = prime * result + rightVal;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pair other = (Pair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (leftVal != other.leftVal)
				return false;
			if (rightVal != other.rightVal)
				return false;
			return true;
		}
		private FourSum getOuterType() {
			return FourSum.this;
		}
		@Override
		public String toString() {
			return "Pair [left=" + left + ", right=" + right + ", leftVal=" + leftVal + ", rightVal=" + rightVal + "]";
		}
	}
	public List<List<Integer>> fourSum(int[] numbers, int target) {
        List<List<Integer>> result = new ArrayList<>();
        if (numbers.length < 4) {
            return result;
        }
        Arrays.sort(numbers);
        Map<Integer, Set<Pair>> map = new HashMap<>();
        for (int j = 1; j < numbers.length; j++) {
            for (int i = 0; i < j; i++) {
                int pairSum = numbers[i] + numbers[j];
                Pair thePair = new Pair(i, j, numbers[i], numbers[j]);
                Set<Pair> thePairs = map.get(pairSum);
                if (!(thePairs == null || !thePairs.contains(thePair))) {
                		continue;
                }
                
                Set<Pair> pairs = map.get(target - pairSum);
                if (pairs != null) {
                    for (Pair pair : pairs) {
                        if (pair.right < i) {
                            List<Integer> sol = new ArrayList<>();
                            sol.add(pair.leftVal);
                            sol.add(pair.rightVal);
                            sol.add(numbers[i]);
                            sol.add(numbers[j]);
                            result.add(sol);
                        }
                    }
                }
                
                if (thePairs == null) {
                    thePairs = new HashSet<Pair>();
                }
                if (!thePairs.contains(thePair)) {
                    thePairs.add(thePair);
                    map.put(pairSum, thePairs);
                }
               
            }
        }
        return result;
    }
	
}
