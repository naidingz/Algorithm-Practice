package laicode;

public class BSTreeReconstruction {
	
	public class TreeNode {
		public int key;
		public TreeNode left;
		public TreeNode right;

		public TreeNode(int key) {
			this.key = key;
		}
	}
	
	/*
	 * Given the preorder traversal sequence of a binary search tree
	 * reconstruct the original tree.
	 * Assumption: The given sequence is not null
	 * There are no duplicate keys in the binary search tree
	 */
	
	
	public TreeNode reconstruct(int[] pre) {
		return reconstruct(pre, 0, pre.length - 1);
	}

	private TreeNode reconstruct(int[] pre, int left, int right) {
		if (left > right) {
			return null;
		}
		if (left == right) {
			return new TreeNode(pre[left]);
		}

		TreeNode root = new TreeNode(pre[left]);
		int leftSize = 0, rightSize = 0, i;
		for (i = left + 1; i <= right && pre[i] < pre[left]; i++)
			;
		leftSize = i - (left + 1);
		rightSize = right - i + 1;

		if (leftSize > 0) {
			root.left = reconstruct(pre, left + 1, left + leftSize);
		}
		if (rightSize > 0) {
			root.right = reconstruct(pre, left + leftSize + 1, right);
		}
		return root;
	}
	
	
	/*
	 * Given the postorder traversal sequence of a binary search tree
	 * reconstruct the original tree.
	 * Assumption: The given sequence is not null
	 * There are no duplicate keys in the binary search tree
	 */
	/*
	public TreeNode reconstruct(int[] post) {
		return reconstruct(post, 0, post.length - 1);
	}

	private TreeNode reconstruct(int[] post, int left, int right) {
		if (left > right) {
			return null;
		}
		if (left == right) {
			return new TreeNode(post[right]);
		}

		TreeNode root = new TreeNode(post[right]);
		int i;
		for (i = left; i < right && post[i] < post[right]; i++)
			;
		int leftSize = i - left, rightSize = right - i;
		if (leftSize > 0) {
			root.left = reconstruct(post, left, left + leftSize - 1);
		}
		if (rightSize > 0) {
			root.right = reconstruct(post, left + leftSize, right - 1);
		}
		return root;
	}
	*/
	
	/*
	 * Given the levelorder traversal sequence of a binary search tree
	 * reconstruct the original tree.
	 * Assumption: The given sequence is not null
	 * There are no duplicate keys in the binary search tree
	 */
	/*
	public TreeNode reconstruct(int[] level) {
		List<Integer> list = new ArrayList<>();
		for (Integer i : level) {
			list.add(i);
		}
		return reconstruct(list);
	}

	private TreeNode reconstruct(List<Integer> level) {
		if (level.size() == 0) {
			return null;
		}
		if (level.size() == 1) {
			return new TreeNode(level.get(0));
		}

		TreeNode root = new TreeNode(level.get(0));
		List<Integer> leftLevel = new ArrayList<>();
		List<Integer> rightLevel = new ArrayList<>();

		for (int i = 1; i < level.size(); i++) {
			if (level.get(i) < level.get(0)) {
				leftLevel.add(level.get(i));
			} else {
				rightLevel.add(level.get(i));
			}
		}
		if (leftLevel.size() > 0) {
			root.left = reconstruct(leftLevel);
		}
		if (rightLevel.size() > 0) {
			root.right = reconstruct(rightLevel);
		}
		return root;
	}
	 */
}
