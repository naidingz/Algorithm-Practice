package laicode;

public class KSmallestInUnsortedArray {

	public static void main(String[] args) {
		KSmallestInUnsortedArray test = new KSmallestInUnsortedArray();
		int[] result = test.kSmallest(new int[] {1,4,2,5,6}, 2);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + " ");
		}
	}

	public int[] kSmallest(int[] array, int k) {
		int[] result = new int[k];
		if (k == 0) {
			return result;
		}
		int left = 0, right = array.length - 1;
		while (true) {
			int mid = partition(array, left, right);
			if (mid == k - 1) {
				break;
			} else if (mid < k - 1) {
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}
		for (int i = 0; i < k; i++) {
			result[i] = array[i];
		}
		return result;
	}

	private int partition(int[] array, int left, int right) {
		int i = left, j = right + 1;
		while (true) {
			while (array[++i] < array[left]) {
				if (i == right) {
					break;
				}
			}
			while (array[--j] > array[left]) {
				if (j == left) {
					break;
				}
			}

			if (i >= j) {
				break;
			} else {
				swap(array, i, j);
			}
		}

		swap(array, left, j);
		return j;
	}

	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

}
