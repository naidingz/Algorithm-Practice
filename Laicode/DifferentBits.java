package laicode;

public class DifferentBits {
    public int diffBits(int a, int b) {
        int result = a ^ b;
        int count = 0;
        for (int i = 0; i < 32; i++) {
            if (((result >> i) & 1) == 1) {
                count++;
            }
        }
        return count;
    }
}
