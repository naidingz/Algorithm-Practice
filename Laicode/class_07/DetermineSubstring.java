package laicode.class_07;

public class DetermineSubstring {

    public static void main(String[] args) {
        String l = "abcab";
        String s = "bc";
        DetermineSubstring test = new DetermineSubstring();
        System.out.println(test.strstr(l, s));
    }

    public int strstr(String large, String small) {
        if (large == null || small == null) {
            return 0;
        }
        if (large.length() == 0 && small.length() == 0) {
            return 0;
        }
        if (large.length() == 0) {
            return -1;
        }
        if (small.length() == 0) {
            return 0;
        }

        for (int i = 0; i <= large.length() - small.length(); i++) {
            for (int j = 0; j <= small.length(); j++) {
                if (j == small.length()) {
                    return i;
                }
                if (large.charAt(i + j) != small.charAt(j)) {
                    break;
                }
            }
        }
        return -1;
    }
}
