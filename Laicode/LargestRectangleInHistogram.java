package laicode;

import java.util.Deque;
import java.util.LinkedList;

public class LargestRectangleInHistogram {

	public static void main(String[] args) {
		LargestRectangleInHistogram test = new LargestRectangleInHistogram();
		System.out.println(test.largest(new int[] {1,2,1}));
	}
	
	/*
	 * Given a non-negative integer array representing 
	 * the heights of a list of adjacent bars. 
	 * Suppose each bar has a width of 1. 
	 * Find the largest rectangular area that can be formed in the histogram.
	 * 
	 * Assumptions: The given array is not null or empty
	 * Examples: { 2, 1, 3, 3, 4 }, 
	 * the largest rectangle area is 3 * 3 = 9
	 * (starting from index 2 and ending at index 4)
	 */
	public int largest(int[] array) {
		int result = 0;
		Deque<Integer> stack = new LinkedList<>();
		for (int i = 0; i <= array.length; i++) {
			int curr = i == array.length ? 0 : array[i];
			while (!stack.isEmpty() && array[stack.peekFirst()] >= curr) {
				int height = array[stack.pollFirst()];
				int left = stack.isEmpty() ? 0 : stack.peekFirst() + 1;
				result = Math.max(result, height * (i - left));
			}
			stack.offerFirst(i);
		}
		return result;
	}
	
	
	/* O(n^2) method
	public int largest(int[] array) {
		int left = 0, right = array.length - 1;
		int globalMax = 0;
		while (left <= right) {
			int minimum = findMinimum(array, left, right);
			globalMax = Math.max(globalMax, minimum * (right - left + 1));
			if (array[left] < array[right]) {
				left++;
			} else {
				right--;
			}
		}
		return globalMax;
	}

	private int findMinimum(int[] array, int left, int right) {
		int result = Integer.MAX_VALUE;
		for (int i = left; i <= right; i++) {
			result = Math.min(result, array[i]);
		}
		return result;
	}
	*/
}
