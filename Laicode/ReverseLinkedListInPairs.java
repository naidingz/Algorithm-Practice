package laicode;

public class ReverseLinkedListInPairs {

    private class ListNode {
        int value;
        ListNode next;

        ListNode(int value) {
            this.value = value;
            next = null;
        }
    }

    /*
     * Reverse pairs of elements in a singly-linked list.
     * L = null, after reverse is null
     * L = 1 -> null, after reverse is 1 -> null
     * L = 1 -> 2 -> null, after reverse is 2 -> 1 -> null
     * L = 1 -> 2 -> 3 -> null, after reverse is 2 -> 1 -> 3 -> null
     */
    public ListNode reverseInPairs(ListNode head) {
        // corner case
        if (head == null || head.next == null) {
            return head;
        }

        // base case
        if (head.next.next == null) {
            ListNode newHead = head.next;
            newHead.next = head;
            head.next = null;
            return newHead;
        }

        ListNode midHead = reverseInPairs(head.next.next);
        ListNode newHead = head.next;
        newHead.next = head;
        head.next = midHead;

        return newHead;
    }

}
