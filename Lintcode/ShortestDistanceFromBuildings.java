import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ShortestDistanceFromBuildings {

	public static void main(String[] args) {
		ShortestDistanceFromBuildings test = new ShortestDistanceFromBuildings();
		System.out.println(test.shortestDistance(new int[][] {{1,1,1,1,1,0},
																{0,0,0,0,0,1},
																{0,1,1,0,0,1},
																{1,0,0,1,0,1},
																{1,0,1,0,0,1},
																{1,0,0,0,0,1},
																{0,1,1,1,1,0}}));
	}

	public int shortestDistance(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[][] cost = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    addCost(grid, cost, i, j);
                }
            }
        }
        
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0) {
                    result = Math.min(result, cost[i][j]);
                }
            }
        }
        return result;
    }
    
    class Node {
        int i;
        int j;
        int cost;
        public Node(int i, int j, int cost) {
            this.i = i;
            this.j = j;
            this.cost = cost;
        }
    }
    
    private void addCost(int[][] grid, int[][] cost, int i, int j) {
        boolean[][] visited = new boolean[grid.length][grid[0].length];
        Queue<Node> q = new LinkedList<>();
        q.offer(new Node(i, j, 0));
        visited[i][j] = true;
        while (!q.isEmpty()) {
            Node node = q.poll();
            for (Node nei : getNeis(grid, node.i, node.j)) {
                if (!visited[nei.i][nei.j]) {
                    nei.cost = node.cost + 1;
                    cost[nei.i][nei.j] += nei.cost;
                    q.offer(nei);
                    visited[nei.i][nei.j] = true;
                }
            }
        }
        	for (int m = 0; m < visited.length; m++) {
        		for (int n = 0; n < visited[0].length; n++) {
        			if (!visited[m][n]) {
        				cost[m][n] = Integer.MAX_VALUE / 2;
        			}
        		}
        	}
    }
    
    private List<Node> getNeis(int[][] grid, int i, int j) {
        List<Node> neis = new ArrayList<>();
        if (i - 1 >= 0 && grid[i - 1][j] == 0) {
            neis.add(new Node(i - 1, j, 0));
        }
        if (i + 1 < grid.length && grid[i + 1][j] == 0) {
            neis.add(new Node(i + 1, j, 0));
        }
        if (j - 1 >= 0 && grid[i][j - 1] == 0) {
            neis.add(new Node(i, j - 1, 0));
        }
        if (j + 1 < grid[0].length && grid[i][j + 1] == 0) {
            neis.add(new Node(i, j + 1, 0));
        }
        return neis;
    }
}
