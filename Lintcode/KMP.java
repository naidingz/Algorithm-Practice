public class KMP {

	public static void main(String[] args) {
		KMP test = new KMP();
		System.out.println(test.strStr("abeccdcdaecdbeabdgg", "abacdababc"));
	}

	public int strStr(String source, String target) {
	    char[] s = source.toCharArray();
	    char[] t = target.toCharArray();
	    int i = 0, j = 0;
	    int[] next = getNext(target);
	    while (i < s.length && j < t.length) {
	       if (j == -1 || s[i] == t[j]) { // 当j为-1时，要移动的是i，当然j也要归0
	           i++;
	           j++;
	       } else {
	           // i不需要回溯了
	           // i = i - j + 1;
	           j = next[j]; // j回到指定位置
	       }
	    }
	    if (j == t.length) {
	       return i - j;
	    } else {
	       return -1;
	    }
	}
	
	public int[] getNext(String target) {
	    char[] strs = target.toCharArray();
	    int[] next = new int[strs.length];
	    next[0] = -1;
	    int j = 0, k = -1;
	    while (j < strs.length - 1) {
	       if (k == -1 || strs[j] == strs[k]) {
	           next[++j] = ++k;
	       } else {
	           k = next[k];
	       }
	    }
	    return next;
	}

}
