package laicode;

public class MaxDifferenceOfSubtreeNodes {
	
	class TreeNode {
		int key;
		TreeNode left;
		TreeNode right;
	}
	
	public int maxDiff(TreeNode root) {
		int[] globalMax = new int[]{0};
		helper(root, globalMax);
		return globalMax[0];
	}
	
	private int helper(TreeNode root, int[] globalMax) {
		if (root == null) {
			return 0;
		}
		
		int leftNum = maxDiff(root.left);
		int rightNum = maxDiff(root.right);
		int diff = Math.abs(leftNum - rightNum);
		globalMax[0] = diff > globalMax[0] ? diff : globalMax[0];
		return leftNum + rightNum + 1;
	}
}
