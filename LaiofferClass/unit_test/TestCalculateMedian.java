package unit_test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestCalculateMedian {

	@BeforeClass
	public static void setUpClass() {
		System.out.println("Run \"before class\"");
	}
	
	@AfterClass
	public static void tearDownClass() {
		System.out.println("Run \"after class\"");
	}
	
	@Before
	public void setUp() {
		System.out.println("Run \"before\"");
	}
	
	@After
	public void tearDown() {
		System.out.println("Run \"after\"");
	}
	
	@Test
	public void test1() {
		CalculateMedian cm = new CalculateMedian();
		double res = cm.getMedian(null);
		assertEquals(0, res, 0.00001);
	}
	
	@Test
	public void test2() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {};
		double res = cm.getMedian(arr);
		assertEquals(0, res, 0.00001);
	}
	
	@Test
	public void test3() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {1, 2, 3};
		double res = cm.getMedian(arr);
		assertEquals(2, res, 0.00001);
	}
	
	@Test
	public void test4() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {2, 1, 3};
		double res = cm.getMedian(arr);
		assertEquals(1, res, 0.00001);
	}
	
	@Test
	public void test5() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {-1, -3, -2};
		double res = cm.getMedian(arr);
		assertEquals(-3, res, 0.00001);
	}
	
	@Test
	public void test6() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {4, 4, 3, 2};
		double res = cm.getMedian(arr);
		assertEquals(3.5, res, 0.00001);
	}
	
	@Test
	public void test7() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE};
		double res = cm.getMedian(arr);
		assertEquals(Integer.MAX_VALUE, res, 0.00001);
	}
	
	@Test
	public void test8() {
		CalculateMedian cm = new CalculateMedian();
		int[] arr = {4, 4, 4, 4};
		double res = cm.getMedian(arr);
		assertEquals(4, res, 0.00001);
	}
	
	
}
