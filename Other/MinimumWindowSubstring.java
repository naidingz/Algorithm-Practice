import java.util.HashMap;
import java.util.Map;

public class MinimumWindowSubstring {
    public static void main(String[] args) {
        MinimumWindowSubstring test = new MinimumWindowSubstring();
        System.out.println(test.minWindow("ADOBECODEBANC","ABC"));
    }

    public String minWindow(String S, String T) {
        if (S == null || T == null || T.length() == 0 || S.length() < T.length()) {
            return "";
        }

        String result = S;
        HashMap<Character, Integer> pattern = new HashMap<>();
        for (int i = 0; i < T.length(); i++) {
            char c = T.charAt(i);
            pattern.put(c, pattern.getOrDefault(c, 0) + 1);
        }

        HashMap<Character, Integer> map = new HashMap<>();
        int slow = 0, fast = 0;
        while (fast < S.length() || isFinished(map, pattern)) {
            if (!isFinished(map, pattern)) {
                char c = S.charAt(fast);
                if (pattern.containsKey(c)) {
                    map.put(c, map.getOrDefault(c, 0) + 1);
                }
                fast++;
            } else {
                if (fast - slow < result.length()) {
                    result = S.substring(slow, fast);
                }
                char c = S.charAt(slow);
                if (pattern.containsKey(c)) {
                    int num = map.get(c);
                    if (num == 1) {
                        map.remove(c);
                    } else {
                        map.put(c, num - 1);
                    }
                }
                slow++;
            }
        }
        return result;
    }

    private boolean isFinished(HashMap<Character, Integer> map, HashMap<Character, Integer> pattern) {
        if (map.size() != pattern.size()) {
            return false;
        }
        for (Map.Entry<Character, Integer> e : pattern.entrySet()) {
            Integer i = map.get(e.getKey());
            if (i == null || i < e.getValue()) {
                return false;
            }
        }
        return true;
    }
}
