package laicode;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class KSmallestNumberWithFactors {
    class Node implements Comparable<Node> {
        final int x;
        final int y;
        final int z;
        final long value;
        Node(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
            this.value = (long) Math.pow(3, x) * (long) Math.pow(5, y) * (long) Math.pow(7, z);
        }

        public int compareTo(Node that) {
            if (that.value == this.value) {
                return 0;
            }
            return this.value > that.value ? -1 : 1;
        }
    }

    public long kth(int k) {
        if (k <= 0) {
            return 0;
        }
        PriorityQueue<Node> pq = new PriorityQueue<>();
        Queue<Node> q = new LinkedList<>();
        Node node = new Node(1, 1, 1);
        q.offer(node);
        HashSet<Long> set = new HashSet<>();
        int round = 0, maxRound = 1 + (int) Math.pow(k, 1/3);
        while (!q.isEmpty() && round <= maxRound) {
            int size = q.size();
            round++;
            for (int i = 0; i < size; i++) {
                Node n = q.poll();
                if (!set.contains(n.value)) {
                    set.add(n.value);
                    if (pq.size() < k) {
                        pq.offer(n);
                    } else if (n.value < pq.peek().value) {
                        pq.poll();
                        pq.offer(n);
                    }

                    q.add(new Node(n.x + 1, n.y, n.z));
                    q.add(new Node(n.x, n.y + 1, n.z));
                    q.add(new Node(n.x, n.y, n.z + 1));
                }
            }
        }
        return pq.peek().value;
    }

    public static void main(String[] args) {
        KSmallestNumberWithFactors test = new KSmallestNumberWithFactors();
        System.out.println(test.kth(7));
    }
}
