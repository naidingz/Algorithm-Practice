package laicode.class_07;

import java.util.*;

public class CommonNumberOfTwoArrays {

    public static void main(String[] args) {
        CommonNumberOfTwoArrays test = new CommonNumberOfTwoArrays();
        for (Integer i : test.common3(Arrays.asList(1, 1, 1, 1, 1),
                                        Arrays.asList(1, 1, 1, 1))) {
            System.out.println(i);
        }
    }

    /*
     * arrays are not sorted
     * there are no duplicated keys
     */
    public List<Integer> common(List<Integer> a, List<Integer> b) {
        List<Integer> result = new ArrayList<>();
        if (a == null || b == null) {
            return result;
        }
        if (a.size() == 0 || b.size() == 0) {
            return result;
        }

        HashSet<Integer> setA = new HashSet<>();
        setA.addAll(a);
        for (Integer i : b) {
            if (setA.contains(i)) {
                result.add(i);
            }
        }
        Collections.sort(result);
        return result;
    }

    /*
     * arrays are not sorted
     * there are duplicated keys
     */
    public List<Integer> common2(List<Integer> A, List<Integer> B) {
        List<Integer> result = new ArrayList<>();
        if (A == null || B == null) {
            return result;
        }
        if (A.size() == 0 || B.size() == 0) {
            return result;
        }

        HashMap<Integer, Integer> mapA = new HashMap<>();
        HashMap<Integer, Integer> mapB = new HashMap<>();

        for (Integer i : A) {
            mapA.put(i, mapA.getOrDefault(i, 0) + 1);
        }
        for (Integer j : B) {
            mapB.put(j, mapB.getOrDefault(j, 0) + 1);
        }
        for (Integer aKey : mapA.keySet()) {
            if (mapB.containsKey(aKey)) {
                for (int i = 0; i < Math.min(mapA.get(aKey), mapB.get(aKey)); i++) {
                    result.add(aKey);
                }
            }
        }
        Collections.sort(result);
        return result;
    }

    /*
     * arrays are sorted
     * there are duplicated keys
     */
    public List<Integer> common3(List<Integer> A, List<Integer> B) {
        List<Integer> result = new ArrayList<>();
        if (A == null || B == null) {
            return result;
        }
        if (A.size() == 0 || B.size() == 0) {
            return result;
        }

        int ptrA = 0, ptrB = 0;
        while (ptrA < A.size() && ptrB < B.size()) {
            if (A.get(ptrA) < B.get(ptrB)) {
                ptrA++;
            } else if (A.get(ptrA) > B.get(ptrB)) {
                ptrB++;
            } else {
                result.add(A.get(ptrA));
                ptrA++;
                ptrB++;
            }
        }

        return result;

    }

}
