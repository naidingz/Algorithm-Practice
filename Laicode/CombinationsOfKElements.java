package laicode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationsOfKElements {

	public static void main(String[] args) {
		int[] array = new int[]{1,2,2,3,4};
		CombinationsOfKElements test = new CombinationsOfKElements();
		for (List<Integer> l : test.combinationsII(array, 2)) {
			System.out.println(l);
		}
	}
	
	/*
	 * Select k elements from array
	 * Return all such combinations
	 * Assumptions: array is not null and no duplicate element
	 */
	public List<List<Integer>> combinationsI(int[] array, int k) {
		List<List<Integer>> result = new ArrayList<>();
		combinationsI(0, k, array, new ArrayList<Integer>(), result);
		return result;
	}
	
	private void combinationsI(int n, int k, int[] array, List<Integer> oneSol, List<List<Integer>> result) {
		if (oneSol.size() == k) {
			result.add(new ArrayList<>(oneSol));
			return;
		} else if (n == array.length) {
			return;
		}
		
		oneSol.add(array[n]);
		combinationsI(n + 1, k, array, oneSol, result);
		oneSol.remove(oneSol.size() - 1);
		combinationsI(n + 1, k, array, oneSol, result);
	}
	
	/*
	 * Select k elements from array
	 * Return all such combinations
	 * Assumptions: array is not null and has duplicate elements
	 */
	public List<List<Integer>> combinationsII(int[] array, int k) {
		Arrays.sort(array);
		List<List<Integer>> result = new ArrayList<>();
		combinationsII(0, k, array, new ArrayList<Integer>(), result);
		return result;
	}
	
	private void combinationsII(int n, int k, int[] array, List<Integer> oneSol, List<List<Integer>> result) {
		if (oneSol.size() == k) {
			result.add(new ArrayList<>(oneSol));
			return;
		} else if (n == array.length) {
			return;
		}
		
		oneSol.add(array[n]);
		combinationsII(n + 1, k, array, oneSol, result);
		oneSol.remove(oneSol.size() - 1);
		
		while (n + 1 < array.length && array[n + 1] == array[n]) {
			n++;
		}
		combinationsII(n + 1, k, array, oneSol, result);
	}
}
