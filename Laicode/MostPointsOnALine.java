package laicode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MostPointsOnALine {
	public static void main(String[] args) {
		MostPointsOnALine test = new MostPointsOnALine();
		Point[] ps = new Point[4];
		ps[0] = new Point(0, 0);
		ps[1] = new Point(1, 1);
		ps[2] = new Point(2, 3);
		ps[3] = new Point(3, 3);
		System.out.println(test.most(ps));
		
	}

	static class Point {
		public int x;
		public int y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	class Line {
		public double k;
		public double b;
		
		public Line(double k, double b) {
			this.k = k;
			this.b = b;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			long temp;
			temp = Double.doubleToLongBits(b);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(k);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Line other = (Line) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (Double.doubleToLongBits(b) != Double.doubleToLongBits(other.b))
				return false;
			if (Double.doubleToLongBits(k) != Double.doubleToLongBits(other.k))
				return false;
			return true;
		}

		private MostPointsOnALine getOuterType() {
			return MostPointsOnALine.this;
		}
	}
	
	public int most(Point[] points) {
		Map<Line, Set<Point>> m1 = new HashMap<>();
		Map<Integer, Set<Point>> m2 = new HashMap<>();

		for (int i = 0; i < points.length - 1; i++) {
			for (int j = i + 1; j < points.length; j++) {
				if (points[i].x == points[j].x) {
					Set<Point> set = m2.get(points[i].x);
					if (set == null) {
						set = new HashSet<>();
					}
					set.add(points[i]);
					set.add(points[j]);
					m2.put(points[i].x, set);
				} else {
					double k = (double)(points[i].y - points[j].y) / (points[i].x - points[j].x);
					double b = points[i].y - k * points[i].x;
					Line l = new Line(k, b);
					Set<Point> set = m1.get(l);
					if (set == null) {
						set = new HashSet<>();
					}
					set.add(points[i]);
					set.add(points[j]);
					m1.put(l, set);
				}
			}
		}
		
		int result = 0;
		for (Map.Entry<Line, Set<Point>> entry : m1.entrySet()) {
			result = Math.max(result, entry.getValue().size());
		}
		for (Map.Entry<Integer, Set<Point>> entry : m2.entrySet()) {
			result = Math.max(result, entry.getValue().size());
		}
		return result;
	}
}
