package laicode;

import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

public class SmallestRange {

    /*
    public int[] smallestRange(int[][] nums) {
        int minx = 0, miny = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
        int[] next = new int[nums.length];
        boolean flag = true;
        PriorityQueue< Integer > min_queue = new PriorityQueue <>((i, j) -> nums[i][next[i]] - nums[j][next[j]]);
        for (int i = 0; i < nums.length; i++) {
            min_queue.offer(i);
            max = Math.max(max, nums[i][0]);
        }
        for (int i = 0; i < nums.length && flag; i++) {
            for (int j = 0; j < nums[i].length && flag; j++) {
                int min_i = min_queue.poll();
                if (miny - minx > max - nums[min_i][next[min_i]]) {
                    minx = nums[min_i][next[min_i]];
                    miny = max;
                }
                next[min_i]++;
                if (next[min_i] == nums[min_i].length) {
                    flag = false;
                    break;
                }
                min_queue.offer(min_i);
                max = Math.max(max, nums[min_i][next[min_i]]);
            }
        }
        return new int[] { minx, miny};
    }
    */

    class Pair implements Comparable<Pair> {
        int value;
        int row;
        Pair(int value, int row) {
            this.value = value;
            this.row = row;
        }
        public int compareTo(Pair that) {
            return Integer.compare(this.value, that.value);
        }
    }

    public int[] smallestRange(List<List<Integer>> nums) {
        int[] next = new int[nums.size()];
        PriorityQueue<Pair> pq = new PriorityQueue<>();
        int max = Integer.MIN_VALUE, left = 0, right = Integer.MAX_VALUE;
        for (int i = 0; i < nums.size(); i++) {
            if (nums.get(i).size() == 0) {return new int[0];}
            pq.offer(new Pair(nums.get(i).get(0), i));
            max = Math.max(max, nums.get(i).get(0));
        }

        while (true) {
            Pair p = pq.poll();
            if (max - p.value < right - left) {
                left = p.value;
                right = max;
            }
            next[p.row]++;
            if (next[p.row] == nums.get(p.row).size()) {break;}
            pq.offer(new Pair(nums.get(p.row).get(next[p.row]), p.row));
            max = Math.max(max, nums.get(p.row).get(next[p.row]));
        }
        return new int[] {left, right};
    }
}
