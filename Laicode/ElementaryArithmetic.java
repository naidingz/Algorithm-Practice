package laicode;

public class ElementaryArithmetic {
    public static void main(String[] args) {
        ElementaryArithmetic test = new ElementaryArithmetic();
        System.out.println(test.add(-1,2));
        System.out.println(test.subtract(-1,2));
        System.out.println(test.multiply(3,-2));
        System.out.println(test.divide(6,-3));
        System.out.println(test.addBinary("1", "100"));
    }

    /*
     * addition by bitwise operation
     */
    public int add(int a, int b) {
        if (b == 0) {
            return a;
        }
        int result = a;
        while (b != 0) {
            result = a ^ b;
            b = (a & b) << 1;
            a = result;
        }
        return result;
    }

    public int add2(int a, int b) {
        if (b == 0) {
            return a;
        }
        int sum = a ^ b;
        int carry = (a & b) << 1;
        return add(sum, carry);
    }

    /*
     * subtract by bitwise operation
     */
    public int subtract(int a, int b) {
        return add(a, negative(b));
    }

    /*
     * get the negative by bitwise operation
     */
    public int negative(int x) {
        return add(~x, 1);
    }

    /*
     * is zero?
     */
    public boolean isZero(int x) {
        return (x & 0xFFFF) == 0;
    }

    /*
     * multiply by bitwise operation
     */
    public int multiply(int a, int b) {
        int result = 0;
        boolean sign = (a > 0) ^ (b > 0);
        a = a > 0 ? a : negative(a);
        b = b > 0 ? b : negative(b);
        while (b != 0) {
            if ((b & 1) == 1) {
                result = add(result, a);
            }
            b = b >> 1;
            a = a << 1;
        }
        return sign ? negative(result) : result;
    }


    /*
     * divide by bitwise operation
     */
    public int divide(int a, int b) {
        if (b == 0) {
            return Integer.MAX_VALUE;
        }
        if (a == 0) {
            return 0;
        }

        boolean sign = (a > 0) ^ (b > 0);
        a = a > 0 ? a : negative(a);
        b = b > 0 ? b : negative(b);
        int result = helpDivide(a, b);
        return sign ? negative(result) : result;
    }

    public int helpDivide(int a, int b) {
        int result = 0;
        for (int i = 31; i >= 0; i--) {
            if ((a >> i) >= b) {
                result += (1 << i);
                a -= (b << i);
            }
        }
        return result;
    }

    /*
     * Given two binary strings, return their sum (also a binary string).
     * Input: a = “11”
     * b = “1”
     * Output: “100”
     */
    public String addBinary(String a, String b) {
        if (a == null || b == null) {
            return null;
        }
        if (a.length() == 0) {
            return b;
        }
        if (b.length() == 0) {
            return a;
        }

        int x = stringToInt(a);
        int y = stringToInt(b);
        int result = x;
        while (y != 0) {
            result = x ^ y;
            y = ((x & y) << 1);
            x = result;
        }
        return intToString(result);
    }

    private int stringToInt(String x) {
        int result = 0;
        if (x == null || x.length() == 0) {
            return 0;
        }
        int N = x.length();
        for (int i = 0; i < N; i++) {
            if (x.charAt(i) == '1') {
                result |= (1 << (N - i - 1));
            }
        }
        return result;
    }

    private String intToString(int x) {
        StringBuilder sb = new StringBuilder();
        boolean begin = false;
        for (int i = 31; i >= 0; i--) {
            if (!begin && ((x >> i) & 1) != 0) {
                begin = true;
                sb.append('1');
            } else if (begin){
                if (((x >> i) & 1) == 0) {
                    sb.append('0');
                } else {
                    sb.append('1');
                }
            }
        }
        return sb.toString();
    }
}
