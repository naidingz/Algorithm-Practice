package graph;

import java.util.List;

public class GraphNode {
    public int value;
    public List<GraphNode> nei;
    public boolean visited = false;
    
    public GraphNode(int v) {
        value = v;
    }
}
