package laicode;

import java.util.HashMap;

public class FourSum {

	public static void main(String[] args) {
		FourSum test = new FourSum();
		int[] array = new int[] {2,1,1,1,0};
		System.out.println(test.exist(array, 3));
	}
	
	class Pair {
		final int left;
		final int right;
		public Pair(int left, int right) {
			this.left = left;
			this.right = right;
		}
	}
	
	public boolean exist(int[] array, int target) {
		if (array == null || array.length < 4) {
			return false;
		}
		
		HashMap<Integer, Pair> map = new HashMap<>();
		
		// We must iterate from back to front
		for (int n = 1; n < array.length; n++) {
			for (int m = 0; m < n; m++) {		
//		for (int m = 0; m < array.length - 1; m++) {
//			for (int n = m + 1; n < array.length; n++) {
				int twoSum = array[m] + array[n];
				if (map.containsKey(target - twoSum) && map.get(target - twoSum).right < m) {
					return true;
				}
				if (!map.containsKey(twoSum)) {
					map.put(twoSum, new Pair(m, n));
				}
			}
		}
		return false;
	}
}
