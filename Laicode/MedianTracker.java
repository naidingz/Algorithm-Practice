package laicode;

import java.util.Collections;
import java.util.PriorityQueue;

public class MedianTracker {
    private final int INITIAL_CAPACITY = 11;
    private final PriorityQueue<Integer> maxHeap;
    private final PriorityQueue<Integer> minHeap;

    public MedianTracker() {
        maxHeap = new PriorityQueue<>(INITIAL_CAPACITY, Collections.reverseOrder());
        minHeap = new PriorityQueue<>(INITIAL_CAPACITY);
    }

    public void read(int value) {

        if (maxHeap.size() + minHeap.size() == 0) {
            maxHeap.offer(value);
            return;
        }

        if (maxHeap.peek() >= value) {
            maxHeap.offer(value);
        } else {
            minHeap.offer(value);
        }

        if (Math.abs(maxHeap.size() - minHeap.size()) > 1) {
            if (maxHeap.size() > minHeap.size()) {
                minHeap.offer(maxHeap.poll());
            } else {
                maxHeap.offer(minHeap.poll());
            }
        }
    }

    public Double median() {
        if (maxHeap.size() + minHeap.size() <= 0) {
            return null;
        }

        if (maxHeap.size() == minHeap.size()) {
            return (maxHeap.peek() + minHeap.peek()) / 2.0;
        }

        if (maxHeap.size() > minHeap.size()) {
            return (double)maxHeap.peek();
        } else {
            return (double)minHeap.peek();
        }
    }

    public static void main(String[] args) {
        MedianTracker tracker = new MedianTracker();
        tracker.read(1);
        tracker.read(2);
        tracker.read(3);
        System.out.println(tracker.median());
    }
}
