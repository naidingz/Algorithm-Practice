package queue;

import java.util.*;

public class MyQueue {

	public static void main(String[] args) {
		MyQueue q = new MyQueue();
		q.offer(5);
		q.offer(4);
		q.offer(9);
		q.offer(1);
		System.out.println(q.peek());
		System.out.println(q.poll());
		System.out.println(q.peek());

	}
	
	Deque<Integer> in;
	Deque<Integer> out;
	
	public MyQueue() {
		in = new LinkedList<>();
		out = new LinkedList<>();
	}
	
	public void offer(int element) {
		in.push(element);
	}
	
	private void shuffleIfNecessary() {
		if (out.isEmpty()) {
			while (!in.isEmpty()) {
				out.push(in.pop());
			}
		}
	}
	
	public Integer poll() {
		shuffleIfNecessary();
		if (out.isEmpty()) {
			return null;
		}
		
		return out.pop();
	}
	
	public Integer peek() {
		shuffleIfNecessary();
		if (out.isEmpty()) {
			return null;
		}
		return out.peek();
	}
	
	public int size() {
		return in.size() + out.size();
	}
	
	public boolean isEmpty() {
		return in.isEmpty() && out.isEmpty();
	}
}
