package laicode;

import java.util.LinkedList;
import java.util.Queue;

public class CousinInBinaryTree {
	
	class TreeNode {
		public int key;
		public TreeNode left;
		public TreeNode right;

		public TreeNode(int key) {
			this.key = key;
		}
	}
	
	
	public boolean isCousin(TreeNode root, int a, int b) {
		if (root == null) {
			return false;
		}

		Queue<TreeNode> q = new LinkedList<>();
		q.offer(root);
		while (!q.isEmpty()) {
			int size = q.size();
			TreeNode tagA = null, tagB = null;
			for (int i = 0; i < size; i++) {
				TreeNode t = q.poll();
				if (t.left != null) {
					q.offer(t.left);
					if (t.left.key == a) {
						tagA = t;
					} else if (t.left.key == b) {
						tagB = t;
					}
				}
				if (t.right != null) {
					q.offer(t.right);
					if (t.right.key == a) {
						tagA = t;
					} else if (t.right.key == b) {
						tagB = t;
					}
				}
			}
			if (tagA != null && tagB != null && tagA != tagB) {
				return true;
			}
		}
		return false;
	}
}
