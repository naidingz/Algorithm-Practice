package laicode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class MaximumOfSlidingWindows {

	public static void main(String[] args) {
		MaximumOfSlidingWindows test = new MaximumOfSlidingWindows();
		System.out.println(test.maxWindows(new int[] {5,2,1,4,3,6,2,8,3,1,4}, 4));
	}

	/*
	class Node implements Comparable<Node> {
		int value;
		int index;

		public Node(int v, int i) {
			this.value = v;
			this.index = i;
		}

		@Override
		public int compareTo(Node that) {
			return Integer.compare(that.value, this.value);
		}
	}

	public List<Integer> maxWindows(int[] nums, int k) {
		List<Integer> result = new ArrayList<>();
		PriorityQueue<Node> pq = new PriorityQueue<>();
		for (int i = 0; i < k - 1; i++) {
			pq.offer(new Node(nums[i], i));
		}
		for (int i = k - 1; i < nums.length; i++) {
			pq.offer(new Node(nums[i], i));

			Node node = pq.peek();
			while (i - k + 1 > node.index) {
				node = pq.poll();
			}
			result.add(node.value);
			if (node.index == i - k + 1) {
				pq.poll();
			}
		}
		return result;
	}
	*/
	
	public List<Integer> maxWindows(int[] nums, int k) {
		List<Integer> result = new ArrayList<>();
		Deque<Integer> deque = new LinkedList<>();
		for (int i = 0; i < nums.length; i++) {
			while (!deque.isEmpty() && nums[i] > deque.peekFirst()) {
				deque.pollFirst();
			}
			deque.offerFirst(nums[i]);
			if (i < k - 1) {
				continue;
			}
			result.add(deque.peekLast());
			if (deque.peekLast() == nums[i - k + 1]) {
				deque.pollLast();
			}
		}
		return result;
	}
}
