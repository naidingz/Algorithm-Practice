package sort;

import java.util.Arrays;

public class HeapSort {

    public static void main(String[] args) {
        System.out.println(-1 / 2);
        int[] array = {2, 1, 0};
        HeapSort test = new HeapSort();
        System.out.println(Arrays.toString(test.heapsort(array)));
    }

    public int[] heapsort(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }
        heapify(array);
        int index = array.length - 1;
        while (index > 0) {
            swap(array, 0, index);
            percolateDown(array, index--, 0);
        }
        return array;
    }

    private void percolateDown(int[] array, int size, int i) {
        while (true) {
            int exchange = i;
            if (2*i+1 < size) {
                exchange = array[2*i+1] > array[exchange] ? 2*i+1 : exchange;
            }
            if (2*i+2 < size) {
                exchange = array[2*i+2] > array[exchange] ? 2*i+2 : exchange;
            }
            if (exchange == i) {
                break;
            } else {
                swap(array, i, exchange);
                i = exchange;
            }
        }
    }

    // max heap
    private void heapify(int[] array) {
        for (int i = (array.length - 1 - 1) / 2; i >= 0; i--) {
            percolateDown(array, array.length, i);
        }
    }

    private void swap(int[] array, int i, int j) {
        int tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}
