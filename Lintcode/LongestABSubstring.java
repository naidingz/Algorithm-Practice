import java.util.HashMap;
import java.util.Map;

public class LongestABSubstring {

	public static void main(String[] args) {
		LongestABSubstring test = new LongestABSubstring();
		System.out.println(test.getAns("ABABAB"));
	}

	public int getAns(String S) {
		int[] M = new int[S.length()];
        M[0] = S.charAt(0) == 'A' ? -1 : 1;
        for (int i = 1; i < S.length(); i++) {
            M[i] = M[i - 1] + (S.charAt(i) == 'A' ? -1 : 1); 
        }
        
        Map<Integer, Integer> map = new HashMap<>();
        int max = 0;
        for (int i = 0; i < S.length(); i++) {
            Integer begin = map.get(M[i]);
            if (begin == null) {
                map.put(M[i], M[i] == 0 ? -1 : i);
            } else {
                max = Math.max(i - begin, max);
            }
        }
        return max;
	}

}
