package laicode;

import java.util.Arrays;

public class ThreeSumClosest {

    public static void main(String[] args) {
        ThreeSumClosest test = new ThreeSumClosest();
        int[] nums = {-1, 2, 1, -4};
        System.out.println(test.threeSumClosest(nums, 1));
    }

    public int threeSumClosest(int[] nums, int target) {
        if (nums == null || nums.length < 3) {
            return Integer.MIN_VALUE;
        }

        Arrays.sort(nums);
        int bestDiff = Integer.MAX_VALUE;
        int bestSum = Integer.MAX_VALUE;
        for (int mid = 1; mid < nums.length - 1; mid++) {
            int left = mid - 1, right = mid + 1;
            while (left >= 0 && right <= nums.length - 1) {
                int currSum = nums[left] + nums[mid] + nums[right];
                int currDiff = Math.abs(currSum - target);
                if (currSum == target) {
                    return target;
                } else if (currSum < target) {
                    right++;
                } else {
                    left--;
                }
                System.out.println(currDiff + " " + currSum);
                bestSum = currDiff < bestDiff ? currSum : bestSum;
                bestDiff = currDiff < bestDiff ? currDiff : bestDiff;
            }
        }
        return bestSum;
    }
}
