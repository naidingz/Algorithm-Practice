package laicode;

public class SearchBitonic {

    public static void main(String[] args) {
        int[] array = {0,1,6,9,5,3,2,-2,-4,-5};
        System.out.println(search(array, -2));
    }
    
    public static int search(int[] array, int target) {
        if (array == null || array.length == 0) {
            return -1;
        }
        int bitonic = findBitonicPoint(array, 0, array.length - 1);
        System.out.println(bitonic);
        int leftResult = binarySearch(array, target, 0, bitonic, true);
        if (leftResult != -1) {
            return leftResult;
        }
        int rightResult = binarySearch(array, target, bitonic, array.length - 1, false);
        if (rightResult != -1) {
            return rightResult;
        } 
        return -1;
    }
    
    // TODO : How to solve situations when existing repeated elements
    // such as 1,2,3,3,3,3,4,5,6,6,4,0
    private static int findBitonicPoint(int[] array, int left, int right) {
//        int mid = left + (right - left) / 2;
//        if (array[mid] > array[mid-1] && array[mid] > array[mid+1]) {
//          return mid;
//        } else if (array[mid] > array[mid-1] && array[mid] < array[mid+1]) {
//          return findBitonicPoint(array, mid, right);
//        } else if (array[mid] < array[mid-1] && array[mid] > array[mid+1]) {
//          return findBitonicPoint(array, left, mid);
//        } else {
//          return -1;
//        }
        
        int maxIndex = 0;
        for (int i = left; i <= right; i++) {
            if (array[i] > array[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    }
    
    private static int binarySearch(int[] array, int target, int left, int right, boolean ascending) {
        while (left <= right) {
            int mid = left + (right - left) / 2;
            if (array[mid] == target) {
                return mid;
            } else if (ascending ? array[mid] > target : array[mid] < target) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return -1;
    }
}
