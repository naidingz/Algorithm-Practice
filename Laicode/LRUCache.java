package laicode;

import java.util.HashMap;
import java.util.Map;

public class LRUCache<K, V> {

	public static void main(String[] args) {
		LRUCache<String, Integer> cache = new LRUCache<>(5);
		/*
		cache.set("a", 1);
		cache.set("b", 2);
		cache.set("c", 3);
		cache.set("d", 4);
		
		cache.get("a");
		cache.printList();
		cache.get("d");
		cache.printList();
		cache.get("c");
		cache.printList();
		cache.get("c");
		cache.printList();
		
		cache.set("e", 5);
		cache.set("f", 6);
		cache.set("g", 7);
		cache.printList();	
		*/
		cache.set("a", 1);
 		cache.printList();
		cache.get("a");
 		cache.printList();
 		
		cache.set("b", 2);
		cache.printList();
		cache.get("b");
 		cache.printList();
 		
 		cache.set("c", 3);
 		cache.printList();
		cache.get("c");
 		cache.printList();
 		
		cache.get("a");
 		cache.printList();
		cache.get("c");
 		cache.printList();
	}
	
	public class ListNode<K, V> {
		K key;
		V value;
		ListNode<K, V> prev;
		ListNode<K, V> next;
		public ListNode(K k, V v) {
			this.key = k;
			this.value = v;
		}
		
		public void update(K k, V v) {
			this.key = k;
			this.value = v;
		}
	}
	
	private Map<K, ListNode<K, V>> lookup;
	private ListNode<K, V> head;
	private ListNode<K, V> tail;
	private final int limit;
	
	public LRUCache(int limit) {
		this.limit = limit;
		this.lookup = new HashMap<>();
	}

	public void set(K key, V value) {
		ListNode<K, V> node = null;
		if (lookup.containsKey(key)) {
			node = lookup.get(key);
			node.value = value;
			remove(node); 
		} else if (lookup.size() < limit) {
			node = new ListNode<K, V>(key, value);
		} else {
			node = tail;
			remove(node);
			node.update(key, value);
		}
		insert(node);
	}

	public V get(K key) {
		ListNode<K, V> node = lookup.get(key);
		if (node == null) {
			return null;
		}
		remove(node);
		insert(node);
		return node.value;
	}
	
	private ListNode<K, V> insert(ListNode<K, V> node) {
		lookup.put(node.key, node);
		if (head == null) {
			head = tail = node;
		} else {
			head.prev = node;
			node.next = head;
			head = node;
		}
		return node;
	}
	
	private ListNode<K, V> remove(ListNode<K, V> node) {
		lookup.remove(node.key);
		if (node.prev != null) {
			node.prev.next = node.next;
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		}
		if (node == head) {
			head = head.next;
		}
		if (node == tail) {
			tail = tail.prev;
		}
		node.next = node.prev = null;
		return node;
	}
	
	private void printList() {
		ListNode<K, V> curr = head;
		while (curr != null) {
			System.out.print(curr.key + "->");
			curr = curr.next;
		}
		System.out.println();
	}
}
