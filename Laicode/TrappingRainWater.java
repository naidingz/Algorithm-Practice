package laicode;

import java.util.ArrayList;
import java.util.List;

public class TrappingRainWater {

    public static void main(String[] args) {
        TrappingRainWater test = new TrappingRainWater();
        int[] height = {5,5,4,7,8,2,6,9,4,5};
        System.out.println(test.maxTrapped(height));
    }

    public int maxTrapped(int[] array) {
        if (array == null || array.length < 3) {
            return 0;
        }
        int result = 0, ptr = 0;
        List<Integer> index = new ArrayList<>();
        while (true) {
            if (ptr + 1 < array.length) {
                if (array[ptr + 1] <= array[ptr]) {
                    index.add(ptr);
                }
                ptr++;
            } else {
                if (array[ptr - 1] <= array[ptr]) {
                    index.add(ptr);
                }
                break;
            }
        }
        if (index.size() < 2) {
            return result;
        }
        ptr = 0;
        int left = index.get(ptr);
        while (true) {
            int bestRight = index.get(++ptr);
            for (int i = ptr; i < index.size(); i++) {
                int right = index.get(i);
                if (array[right] > array[left]) {
                    bestRight = right;
                    ptr = i;
                    break;
                } else {
                    bestRight = array[right] >= array[bestRight] ? right : bestRight;
                    ptr = array[right] >= array[bestRight] ? i : ptr;
                }
            }
            for (int i = left + 1; i <= bestRight - 1; i++) {
                result += Math.max((Math.min(array[left], array[bestRight]) - array[i]), 0);
            }
            if (ptr == index.size() - 1) {
                break;
            } else {
                left = index.get(ptr);
            }
        }
        return result;
    }
}
