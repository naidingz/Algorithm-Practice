package laicode;

public class ArrayHopper {

	public static void main(String[] args) {
		ArrayHopper test = new ArrayHopper();
		System.out.println(test.minJumpOut(new int[] {1, 3, 2, 0, 2}));
	}

	/*
	 * Given an array A of non-negative integers, 
	 * you are initially positioned at index 0 of the array. 
	 * A[i] means the maximum jump distance from that position 
	 * (you can only jump towards the end of the array). 
	 * Determine if you are able to reach the last index.
	 * 
	 * Assumption: The given array is not null and has length of at least 1.
	 * {1, 3, 2, 0, 3}, we are able to reach the end of array
	 * (jump to index 1 then reach the end of the array)
	 */
	public boolean canJump(int[] array) {
		boolean[] M = new boolean[array.length];
		M[array.length - 1] = true;
		for (int i = array.length - 2; i >= 0; i--) {
			for (int j = 1; j <= array[i] && i + j < array.length; j++) {
				if (M[i + j]) {
					M[i] = true;
					break;
				}
			}
		}
		return M[0];
	}
	
	/*
	 * Given an array A of non-negative integers, 
	 * you are initially positioned at index 0 of the array. 
	 * A[i] means the maximum jump distance from index i 
	 * (you can only jump towards the end of the array). 
	 * Determine the minimum number of jumps you need to reach the end of array. 
	 * If you can not reach the end of the array, return -1.
	 * 
	 * Assumption: The given array is not null and has length of at least 1.
	 * {3, 3, 1, 0, 4}, the minimum jumps needed is 2 
	 * (jump to index 1 then to the end of array)
	 */
	public int minJump(int[] array) {
		int[] M = new int[array.length];
		for (int i = array.length - 2; i >= 0; i--) {
			int best = -1;
			for (int j = 1; j <= array[i] && i + j < array.length; j++) {
				if (M[i + j] >= 0) {
					if (best < 0) {
						best = M[i + j] + 1;
					} else {
						best = M[i + j] + 1 < best ? M[i + j] + 1 : best;
					}
				}
			}
			M[i] = best;
		}
		return M[0];
	}
	
	/*
	 * Given an array of non-negative integers, 
	 * you are initially positioned at index 0 of the array. 
	 * A[i] means the maximum jump distance from that position 
	 * (you can only jump towards the end of the array). 
	 * Determine the minimum number of jumps you need to jump out of the array.
	 * By jump out, it means you can not stay at the end of the array. 
	 * Return -1 if you can not do so.
	 * 
	 * Assumption: The given array is not null and has length of at least 1.
	 */
	public int minJumpOut(int[] array) {
		int[] M = new int[array.length];
		for (int i = array.length - 1; i >= 0; i--) {
			int best = -1;
			for (int j = 1; j <= array[i]; j++) {
				if (i + j < array.length) {
					if (M[i + j] >= 0) {
						if (best < 0) {
							best = M[i + j] + 1;
						} else {
							best = M[i + j] + 1 < best ? M[i + j] + 1 : best;
						}
					}
				} else {
					best = 1;
					break;
				}
			}
			M[i] = best;
		}
		return M[0];
	}
}
