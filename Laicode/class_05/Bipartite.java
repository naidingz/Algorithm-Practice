package laicode.class_05;

import java.util.ArrayList;
import java.util.List;

public class Bipartite {

    // TODO : finish this
    public boolean isBipartite(List<GraphNode> graph) {
        if (graph == null || graph.size() <= 1) {
            return true;
        }



        return true;
    }
}


class GraphNode {
    public int key;
    public List<GraphNode> neighbors;

    public GraphNode(int key) {
        this.key = key;
        this.neighbors = new ArrayList<GraphNode>();
    }
}