package laicode;

import java.util.HashMap;

public class MeetingRoom {

    public static void main(String[] args) {
        int[][] intervals = {{0,30},{40,50},{60,70},{100,130}};
        MeetingRoom test = new MeetingRoom();
        System.out.println(test.canAttendMeetings(intervals));
    }

    /*
     * Given an array of meeting time intervals
     * consisting of start and end times [[s1,e1],[s2,e2],...] (si < ei),
     * determine if a person could attend all meetings.
     */
    public boolean canAttendMeetings(int[][] intervals) {
        if (intervals == null) {
            return false;
        }
        if (intervals.length <= 1) {
            return true;
        }

        sort(intervals);
        print(intervals);
        for (int i = 1; i < intervals.length; i++) {
            if (intervals[i][0] < intervals[i-1][1]) {
                return false;
            }
        }
        return true;
    }

    private void print(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void sort(int[][] array) {
        if (array == null || array.length == 0) {
            return;
        }

        int N = array.length;
        int[][] aux = new int[N][array[0].length];
        for (int sz = 1; sz < N; sz = sz+sz) {
            for (int i = 0; i < N - sz; i += sz+sz) {
                merge(array, aux, i, i+sz-1, Math.min(i+sz+sz-1, N-1));
            }
        }
    }

    private void merge(int[][] array, int[][] aux, int left, int mid, int right) {
        for (int i = left; i <= right; i++) {
            aux[i] = array[i];
        }

        int i = left, j = mid+1;
        for (int k = left; k <= right; k++) {
            if (i > mid) {
                array[k] = aux[j++];
            } else if (j > right) {
                array[k] = aux[i++];
            } else if (array[j][0] < aux[i][0]) {
                array[k] = aux[j++];
            } else {
                array[k] = aux[i++];
            }
        }
    }

}
