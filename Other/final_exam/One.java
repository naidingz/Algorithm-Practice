package final_exam;

import java.util.ArrayList;
import java.util.List;

public class One {
	public static void main(String[] args) {
		One test = new One();
		for (String string : test.permutation("abc")) {
			System.out.println(string);
		}
	}

	public List<String> permutation(String input) {
		List<String> result = new ArrayList<>();
		helper(0, input, new StringBuilder(), result);
		return result;
	}

	private void helper(int k, String input, StringBuilder sb, List<String> result) {
		if (k == input.length() - 1) {
			sb.append(input.charAt(input.length() - 1));
			result.add(sb.toString());
			sb.deleteCharAt(sb.length() - 1);
			return;
		}
		sb.append(input.charAt(k));
		sb.append('_');
		helper(k + 1, input, sb, result);
		sb.deleteCharAt(sb.length() - 1);
		helper(k + 1, input, sb, result);
		sb.deleteCharAt(sb.length() - 1);
	}
}
