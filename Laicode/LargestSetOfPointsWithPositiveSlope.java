package laicode;

import java.util.Arrays;
import java.util.Comparator;

import laicode.MostPointsOnALine.Point;

public class LargestSetOfPointsWithPositiveSlope {

	public static void main(String[] args) {
		LargestSetOfPointsWithPositiveSlope test = new LargestSetOfPointsWithPositiveSlope();
		Point[] ps = new Point[2];
		ps[0] = new Point(1, 2);
		ps[1] = new Point(1, 2);
		System.out.println(test.largest(ps));
	}
	
	static class Point {
		public int x;
		public int y;

		public Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public int largest(Point[] points) {
		
		Arrays.sort(points, new Comparator<Point>() {
			@Override
			public int compare(Point p1, Point p2) {
				return Integer.compare(p1.y, p2.y);
			}
		});
		
		return longest(points);
	}
	
	private int longest(Point[] points) {
		if (points.length == 0) {
			return 0;
		}
		int[] M = new int[points.length];
		M[0] = 1;
		int global = 1;
		for (int i = 1; i < points.length; i++) {
			int max = 0;
			for (int j = 0; j < i; j++) {
				if (points[j].x < points[i].x && 
					points[j].y != points[i].y && 
					M[j] > max) {
					max = M[j];
				}
			}
			M[i] = max + 1;
			global = Math.max(global, M[i]);
		}
		return global == 1 ? 0 : global;
	}
}
