package final_exam;

import java.util.ArrayList;
import java.util.List;

public class Three {

	public static void main(String[] args) {
		Three test = new Three();
		System.out.println(test.decompose(4));
		
	}
	
	public int decompose(int number) {
		if (number <= 0) {
			return 0;
		}
		int[] best = new int[] {Integer.MAX_VALUE};
		helper(number, 0, best);
		return best[0];
	}
	
	private void helper(int remain, int level, int[] best) {
		if (remain == 0) {
			best[0] = Math.min(best[0], level);
			return;
		}
		
		for (int i = 1; i * i <= remain; i++) {
			helper(remain - i * i, level + 1, best);
		}
	}
}
