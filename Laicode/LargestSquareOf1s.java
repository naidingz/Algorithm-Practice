package laicode;

public class LargestSquareOf1s {

	public static void main(String[] args) {
		LargestSquareOf1s test = new LargestSquareOf1s();
		
		int[][] matrix = new int[][] {{1, 1, 1, 1},
									  {1, 1, 1, 0},
									  {1, 1, 1, 1},
								      {1, 1, 0, 1}};
		System.out.println(test.largest(matrix));
	}
	
	/*
	 * Determine the largest square of 1s in a binary matrix 
	 * (a binary matrix only contains 0 and 1), 
	 * return the length of the largest square.
	 * 
	 * Assumption: The given matrix is not null and guaranteed to be of size N * N, N >= 0
	 * Examples
	 * {{0, 0, 0, 0},
	 *  {1, 1, 1, 1},
	 *  {0, 1, 1, 1},
	 *  {1, 0, 1, 1}}
	 * the largest square of 1s has length of 2
	 */
	public int largest(int[][] matrix) {
		int N = matrix.length;
		if (N == 0) {
			return 0;
		}

		int[][] M = new int[N][N];
		int globalMax = 0;
		for (int i = 0; i < N; i++) {
			M[i][0] = matrix[i][0];
			globalMax = Math.max(M[i][0], globalMax);
		}
		for (int j = 0; j < N; j++) {
			M[0][j] = matrix[0][j];
			globalMax = Math.max(M[0][j], globalMax);
		}
		for (int i = 1; i < N; i++) {
			for (int j = 1; j < N; j++) {
				if (matrix[i][j] == 1) {
					M[i][j] = 1 + Math.min(M[i - 1][j - 1], Math.min(M[i - 1][j], M[i][j - 1]));
					globalMax = Math.max(M[i][j], globalMax);
				}
			}
		}
		return globalMax;
	}
}
