package laicode.class_08;

public class ReverseString {

    public static void main(String[] args) {
        String s = "zhounaiding";
        ReverseString test = new ReverseString();
        System.out.println(test.reverse(s));
    }

    public String reverse(String input) {
        if (input == null) {
            return null;
        }
        char[] inputChars = input.toCharArray();
        int size = inputChars.length;
        for (int i = 0; i < size / 2; i++) {
            char tmp = inputChars[i];
            inputChars[i] = inputChars[size - i - 1];
            inputChars[size - i - 1] = tmp;
        }
        return new String(inputChars);
    }
}
