package laicode.class_07;

import java.util.HashSet;

public class RemoveCertainCharacters {
    public String remove(String input, String t) {
        if (input == null || t == null || input.length() == 0) {
            return new String();
        }
        if (t.length() == 0) {
            return input;
        }

        HashSet<Character> set = new HashSet<>();
        for (Character c : t.toCharArray()) {
            set.add(c);
        }

        char[] inputChars = input.toCharArray();
        int slow = 0, fast = 0;
        while (fast < inputChars.length) {
            if (!set.contains(inputChars[fast])) {
                inputChars[slow++] = inputChars[fast++];
            } else {
                fast++;
            }
        }

        return new String(inputChars, 0, slow);
    }
}
