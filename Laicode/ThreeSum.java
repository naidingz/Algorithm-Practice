package laicode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class ThreeSum {

    public static void main(String[] args) {
        int[] array = {1,2,3,4,5,6,7,8,9,10};
        ThreeSum test = new ThreeSum();
        for (List<Integer> l : test.allTriples(array, 8)) {
            System.out.println(l);
        }
    }
    
    public List<List<Integer>> allTriples(int[] array, int target) {
        if (array == null || array.length < 3) {
            return null;
        }
        List<List<Integer>> result = new ArrayList<>();
        
    	HashMap<Integer, Integer> map = new HashMap<>();
    	for (int i = 0; i < array.length; i++) {
    		map.put(array[i], map.getOrDefault(array[i], 0) + 1);
    	}
    	
        Arrays.sort(array);
        for (int i = 0; i <= array.length - 3; i++) {
        	Integer count = map.get(array[i]);
        	if (count == 1) {
        		map.remove(array[i]);
        	} else {
        		map.put(array[i], count - 1);
        	}
        	if (i == 0 || (i > 0 && array[i] != array[i-1])) {
            	allDoubles(array, map, i+1, array[i], target - array[i], result);
        	}
        }
        return result;
    }
    
    private void allDoubles(int[] array, HashMap<Integer, Integer> map, int start, 
    						int one, int another, List<List<Integer>> result) {
    	HashSet<Integer> visited = new HashSet<>();
    	for (int i = start; i < array.length; i++) {
    		Integer count = map.get(another - array[i]);
    		if (count == null || visited.contains(array[i])) {
    			continue;
    		}
			if (another - array[i] != array[i] || (another - array[i] == array[i] && count > 1)) {
				List<Integer> tmp = new ArrayList<Integer>();
				tmp.add(one);
				tmp.add(array[i]);
				tmp.add(another - array[i]);
				result.add(tmp);
				visited.add(array[i]);
				visited.add(another - array[i]);
			}
    	}
    }
}
