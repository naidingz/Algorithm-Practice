package laicode.class_08;

import java.util.HashSet;

public class LongestSubstring {

    public static void main(String[] args) {
        String s = "12abcdefbt";
        LongestSubstring test = new LongestSubstring();
        System.out.println(test.longest(s));
    }

    public int longest(String input) {
        int result = 0;
        if (input == null || input.length() == 0) {
            return result;
        }

        int left = 0, right = 0;
        HashSet<Character> occurSet = new HashSet<>();
        while (right < input.length()) {
            Character c = input.charAt(right);
            if (!occurSet.contains(c)) {
                occurSet.add(c);
                right++;
                result = right - left > result ? right - left : result;
            } else {
                while (occurSet.contains(c)) {
                    occurSet.remove(input.charAt(left++));
                }
                occurSet.add(c);
                right++;
            }
        }
        return result;
    }
}
