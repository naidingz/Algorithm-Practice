import java.util.*;

public class DisjointWhiteObjects {
    public static void main(String[] args) {
        DisjointWhiteObjects test = new DisjointWhiteObjects();
        int[][] arr = {{0,1,0,1},{1,0,0,1},{0,1,1,0},{0,1,0,0}};
        System.out.println(test.whiteObjects(arr));
        HashMap<Character, Integer> map = new HashMap<>();
        HashMap<Character, Integer> m = new HashMap<>();
        
    }

    class Node {
        int row;
        int col;
        int value;
        int ordinal = 0;
        boolean visited = false;
        Node(int row, int col, int value) {
            this.row = row;
            this.col = col;
            this.value = value;
        }
    }

    public int whiteObjects(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return 0;
        }

        int m = matrix.length;
        int n = matrix[0].length;
        Node[][] nodeMatrix = new Node[m][n];
        for (int i = 0 ; i < m; i++) {
            for (int j = 0; j < n; j++) {
                nodeMatrix[i][j] = new Node(i, j, matrix[i][j]);
            }
        }

        int count = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                Node node = nodeMatrix[i][j];
                if (!node.visited && node.value == 1) {
                    node.visited = true;
                } else if (!node.visited && node.value == 0) {
                    count = count + 1;
                    Queue<Node> q = new LinkedList<>();
                    q.offer(node);
                    while (!q.isEmpty()) {
                        Node oneNode = q.poll();
                        oneNode.ordinal = count;
                        oneNode.visited = true;
                        List<Node> neis = new ArrayList<>();
                        if (oneNode.col - 1 >= 0) {
                            neis.add(nodeMatrix[oneNode.row][oneNode.col - 1]);
                        }
                        if (oneNode.row - 1 >= 0) {
                            neis.add(nodeMatrix[oneNode.row - 1][oneNode.col]);
                        }
                        if (oneNode.col + 1 < n) {
                            neis.add(nodeMatrix[oneNode.row][oneNode.col + 1]);
                        }
                        if (oneNode.row + 1 < m) {
                            neis.add(nodeMatrix[oneNode.row + 1][oneNode.col]);
                        }
                        for (Node nei : neis) {
                            if (!nei.visited && nei.value == 0) {
                                q.offer(nei);
                            }
                        }
                    }
                }

            }
        }
        return count;
    }
}
