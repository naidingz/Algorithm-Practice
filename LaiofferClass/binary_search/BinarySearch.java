package binary_search;

public class BinarySearch {

	public static void main(String[] args) {
		int[] array = {1,2,2,3,3,3,4,7,10};
		System.out.println(binarySearch(array, 10));
		System.out.println(firstOccur(array, 3));
		System.out.println(lastOccur(array, 8));
		System.out.println(closestOccur(array, 9));

	}
	
	
	/* The number to be found is in [left, right] */
	
	public static int binarySearch(int[] array, int target) {
		if (array == null || array.length == 0) {
			return -1;
		}
		
		int left = 0;
		int right = array.length - 1;
		while (left <= right) {
			int mid = left + (right - left) / 2;
			if (array[mid] == target) {
				return mid;
			} else if (array[mid] > target) {
				right = mid - 1;
			} else {
				left = mid + 1;
			}
		}
		return -1;
	}
	
	public static int firstOccur(int[] array, int target) {
		if (array == null || array.length == 0) {
			return -1;
		}
		
		int left = 0;
		int right = array.length - 1;
		while (right - left > 1) {
			int mid = left + (right - left) / 2;
			if (target <= array[mid]) right = mid;
			else left = mid;
		}
		
		if (array[left] == target) return left;
		if (array[right] == target) return right;
		return -1;
	}
	
	public static int lastOccur(int[] array, int target) {
		if (array == null || array.length == 0) {
			return -1;
		}
		
		int left = 0;
		int right = array.length - 1;
		
		while (right - left > 1) {
			int mid = left + (right - left) / 2;
			if (target >= array[mid]) left = mid;
			else right = mid;
		}
		
		if (array[right] == target) return right;
		if (array[left] == target) return left;
		return -1;
	}
	
	public static int closestOccur(int[] array, int target) {
		if (array == null || array.length == 0) {
			return -1;
		}
		
		int left = 0;
		int right = array.length - 1;
		
		while (right - left > 1) {
			int mid = left + (right - left) / 2;
			if (target == array[mid]) return mid;
			else if (target > array[mid]) left = mid;
			else right = mid;
		}

		if (Math.abs(array[left] - target) <= Math.abs(array[right] - target))
			return left;
		else 
			return right;
	}
}
