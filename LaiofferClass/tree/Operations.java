package tree;

import java.util.*;

public class Operations {

	public static void main(String[] args) {

        //     10
        //    /  \
        //   5   15
        //  / \    \
        // 2   7    20
        TreeNode node1 = new TreeNode(10);
        TreeNode node2 = new TreeNode(5);
        TreeNode node3 = new TreeNode(15);
        TreeNode node4 = new TreeNode(2);
        TreeNode node5 = new TreeNode(7);
        TreeNode node6 = new TreeNode(20);

        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.right = node6;
		
//		System.out.println("The height of this tree is : " + getHeight(node1));
//		System.out.println("The total nodes of this tree is : " + countNodes(node1));
//		System.out.println("The balance status of this tree is : " + checkBalanced(node1));
//		System.out.println("The two trees are identical : " + isIdentical(node1, node2));
		System.out.println(checkCompleted(node1));
//		TreeTraverse.levelOrder(node1);
	}
	
	/*----------------------------------------------------------*/
	public static int getHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int leftHeight = getHeight(root.left);
		int rightHeight = getHeight(root.right);
		return Math.max(leftHeight, rightHeight) + 1;
	}
	
	/*----------------------------------------------------------*/
	public static int countNodes(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int leftNodes = countNodes(root.left);
		int rightNodes = countNodes(root.right);
		return leftNodes + rightNodes + 1;
	}
	
	/*----------------------------------------------------------*/
	public static int checkBalanced(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int leftStatus = checkBalanced(root.left);
		int rightStatus = checkBalanced(root.right);
		if (leftStatus == -1 || rightStatus == -1) {
			return -1;
		} else {
			if (Math.abs(leftStatus - rightStatus) > 1) {
				return -1;
			}
		}
		return Math.max(leftStatus, rightStatus) + 1;
	}
	
	/*----------------------------------------------------------*/
	public static boolean isIdentical(TreeNode one, TreeNode two) {
		if (one == two) return true;
		if (one == null && two != null) return false;
		if (one != null && two == null) return false;
		
		boolean leftStatus = isIdentical(one.left, two.left);
		if (!leftStatus) return false;
		
		boolean rightStatus = isIdentical(one.right, two.right);
		if (!rightStatus) return false;
		
		if(one.value != two.value) return false;
		
		return true;
	}

	/*----------------------------------------------------------*/
	public static boolean isSymetric(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		List<Integer> leftList = TreeTraverse.inOrder2(root.left);
		List<Integer> rightList = TreeTraverse.inOrder2(root.right);
		
		int numOfLeftNodes = leftList.size();
		int numOfRightNodes = rightList.size();
		
		if (numOfLeftNodes != numOfRightNodes) return false;
		
		for (int i = 0; i < numOfLeftNodes; i++) {
			if (leftList.get(i).intValue() != rightList.get(numOfLeftNodes-i-1).intValue()) {
				return false;
			}
		}
		
		return true;
	}
	
	/*----------------------------------------------------------*/
	public static boolean isSymmetric2(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		return helpJustifySymmertric(root.left, root.right);
	}
	
	private static boolean helpJustifySymmertric(TreeNode root1, TreeNode root2) {
		// base case
		if (root1 == null && root2 == null) {
			return true;
		}
		
		// recursion rule
		if (root1 == null || root2 == null) {
			return false;
		}
		
		if(root1.value != root2.value) { 
			return false;
		}
		
		return helpJustifySymmertric(root1.left, root2.right) && helpJustifySymmertric(root1.right, root2.left);
	}
	
	/*----------------------------------------------------------*/	
	public static boolean isTweakedIdentical(TreeNode root1, TreeNode root2) {
		if (root1 == null && root2 == null) {
			return true;
		}
		
		if (root1 == null || root2 == null) {
			return false;
		}
		
		if (root1.value != root2.value) {
			return false;
		}
		
		return (isTweakedIdentical(root1.left, root2.left) && isTweakedIdentical(root1.right, root2.right)) ||
			   (isTweakedIdentical(root1.left, root2.right) && isTweakedIdentical(root1.right, root2.left));
	}
	
	/*----------------------------------------------------------*/	
	public static boolean checkCompleted(TreeNode root) {
        if (root == null) {
            return true;
        }

        Queue<TreeNode> pq = new LinkedList<>();
        pq.offer(root);
        int count = 0;
        while (!pq.isEmpty()) {
            TreeNode curr = pq.poll();
            if (curr.left != null && curr.right != null) {
                if (count >= 1) {
                    return false;
                }
                pq.offer(curr.left);
                pq.offer(curr.right);
            } else {
                count++;
                if (curr.left == null && curr.right != null) {
                    return false;
                } else if (curr.left != null && curr.right == null) {
                    if (count >= 2) {
                        return false;
                    } else {
                        pq.offer(curr.left);
                    }
                }
            }
        }
		return true;
	}
}
