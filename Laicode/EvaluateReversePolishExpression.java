package laicode;

import java.util.Deque;
import java.util.LinkedList;

public class EvaluateReversePolishExpression {

    public static void main(String[] args) {
        String[] exp = {"2","1","+","3","*"};
        EvaluateReversePolishExpression test = new EvaluateReversePolishExpression();
        System.out.println(test.evalRPN(exp));
    }

    public int evalRPN(String[] tokens) {
        

        if (tokens == null) {
            return 0;
        }

        Deque<Integer> numStack = new LinkedList<>();
        for (String s : tokens) {
            switch (s) {
                case "+" :
                    numStack.offerFirst(numStack.pollFirst() + numStack.pollFirst());
                    break;

                case "-" :
                    numStack.offerFirst(-numStack.pollFirst() + numStack.pollFirst());
                    break;

                case "*" :
                    numStack.offerFirst(numStack.pollFirst() * numStack.pollFirst());
                    break;

                case "/" :
                    int o1 = numStack.pollFirst(), o2 = numStack.pollFirst();
                    numStack.offerFirst(o2 / o1);
                    break;

                default:
                    numStack.offerFirst(Integer.parseInt(s));
            }
        }

        return numStack.pollFirst();
    }
}
