package laicode;

public class LargestXOf1s {

	public static void main(String[] args) {
		LargestXOf1s test = new LargestXOf1s();
		
		int[][] matrix = new int[][] {{1, 1, 1, 1},
									  {1, 1, 1, 0},
									  {1, 1, 1, 1},
								      {1, 1, 0, 1}};
		System.out.println(test.largest(matrix));
	}
	/*
	 * Given a matrix that contains only 1s and 0s, 
	 * find the largest X shape which contains only 1s, 
	 * with the same arm lengths and the four arms joining at the central point.
	 * Return the arm length of the largest X shape.
	 * 
	 * Assumptions: The given matrix is not null, has size of N * M, N >= 0 and M >= 0.
	 * 
	 * Examples
	 * { {0, 0, 0, 0},
	 *   {1, 1, 1, 1},
	 *   {0, 1, 1, 1},
	 *   {1, 0, 1, 1} }
	 *   the largest X of 1s has arm length 2.
	 */
	public int largest(int[][] matrix) {
		if (matrix.length == 0 || matrix[0].length == 0) {
			return 0;
		}
		
		int M = matrix.length;
		int N = matrix[0].length;
		
		int[][] tl2dr = helperMatrix(matrix, M, N, "tl2dr"); // top left to down right
		int[][] dr2tl = helperMatrix(matrix, M, N, "dr2tl");
		int[][] dl2tr = helperMatrix(matrix, M, N, "dl2tr");
		int[][] tr2dl = helperMatrix(matrix, M, N, "tr2dl");
		
		int globalMax = 0;
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				int len1 = Math.min(tl2dr[i][j], dr2tl[i][j]);
				int len2 = Math.min(dl2tr[i][j], tr2dl[i][j]);
				int len = Math.min(len1, len2);
				globalMax = Math.max(globalMax, len);
			}
		}
		return globalMax;
	}
	
	private int[][] helperMatrix(int[][] matrix, int M, int N, String type) {
		int[][] aux = new int[M][N];
		switch (type) {
		case "tl2dr":
			for (int i = 0; i < M; i++) {
				for (int j = 0; j < N; j++) {
					if (i == 0) {
						aux[0][j] = matrix[0][j];
					} else if (j == 0) {
						aux[i][0] = matrix[i][0];
					} else {
						aux[i][j] = matrix[i][j] == 1 ? aux[i-1][j-1] + 1 : 0; 
					}
				}
			}
			break;

		case "dr2tl":
			for (int i = M - 1; i >= 0; i--) {
				for (int j = N - 1; j >= 0; j--) {
					if (i == M - 1) {
						aux[M - 1][j] = matrix[M - 1][j];
					} else if (j == N - 1) {
						aux[i][N - 1] = matrix[i][N - 1];
					} else {
						aux[i][j] = matrix[i][j] == 1 ? aux[i+1][j+1] + 1 : 0; 
					}
				}
			}
			break;

		case "dl2tr":
			for (int i = M - 1; i >= 0; i--) {
				for (int j = 0; j < N; j++) {
					if (i == M - 1) {
						aux[M - 1][j] = matrix[M - 1][j];
					} else if (j == 0) {
						aux[i][0] = matrix[i][0];
					} else {
						aux[i][j] = matrix[i][j] == 1 ? aux[i+1][j-1] + 1 : 0; 
					}
				}
			}
			break;

		case "tr2dl":
			for (int i = 0; i < M; i++) {
				for (int j = N - 1; j >= 0; j--) {
					if (i == 0) {
						aux[0][j] = matrix[0][j];
					} else if (j == N - 1) {
						aux[i][N - 1] = matrix[i][N - 1];
					} else {
						aux[i][j] = matrix[i][j] == 1 ? aux[i-1][j+1] + 1 : 0; 
					}
				}
			}
			break;

		default:
			throw new IllegalArgumentException("Illegal argument for type");
		}
		
		return aux;
	}

}
