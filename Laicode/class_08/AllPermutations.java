package laicode.class_08;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class AllPermutations {

    public static void main(String[] args) {
        String str = "abca";
        AllPermutations test = new AllPermutations();
        for (String i : test.permutations2(str)) {
            System.out.println(i);
        }
    }

    /*
     * No duplicated keys
     */
    public List<String> permutations(String set) {
        List<String> result = new ArrayList<>();
        if (set == null) {
            return result;
        }

        permutations(set.toCharArray(), 0, result);
        return result;
    }

    private void permutations(char[] input, int index, List<String> result) {
        if (input.length == index) {
            result.add(new String(input));
        }

        for (int i = index; i < input.length; i++) {
            swap(input, index, i);
            permutations(input, index + 1, result);
            swap(input, index, i);
        }
    }

    private void swap(char[] array, int i, int j) {
        char tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }

    /*
     * With duplicated keys
     */

    public List<String> permutations2(String set) {
        if (set == null) {
            return new ArrayList<>();
        }
        List<String> result = new ArrayList<>();
        char[] setChars = set.toCharArray();
        permutations2(setChars, 0, result);
        return result;
    }

    private void permutations2(char[] input, int index, List<String> result) {
        if (index == input.length) {
            result.add(new String(input));
            return ;
        }

        HashSet<Character> used = new HashSet<>();
        for (int i = index; i < input.length; i++) {
            if (!used.contains(input[i])) {
                used.add(input[i]);
                swap(input, index, i);
                permutations2(input, index + 1, result);
                swap(input, index, i);
            }
        }
    }
}
