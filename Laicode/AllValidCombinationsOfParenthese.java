package laicode;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class AllValidCombinationsOfParenthese {

	public static void main(String[] args) {
		AllValidCombinationsOfParenthese test = new AllValidCombinationsOfParenthese();
		for (String s : test.validParentheses(1, 1, 1)) {
			System.out.println(s);
		}
	}
	
	/*
	 * Get all valid permutations of l pairs of (), m pairs of <> and n pairs of {}.
	 * l, m, n >= 0
	 */
	public List<String> validParentheses(int l, int m, int n) {
		List<String> result = new ArrayList<>();
		char[] symbols = new char[] {'(', ')', '<', '>', '{', '}'};
		validParentheses(new int[6], symbols, new int[] {l, m, n}, 
				new StringBuilder(), new LinkedList<Character>(), result);
		return result;
	}
	
	private void validParentheses(int[] current, char[] symbols, int[] total, 
			StringBuilder sol, Deque<Character> stack, List<String> result) {
		if (arraySum(current) == 2 * arraySum(total)) {
			if (current[0] == total[0] && current[1] == total[0]
			 && current[2] == total[1] && current[3] == total[1]
			 && current[4] == total[2] && current[5] == total[2]) {
				result.add(sol.toString());
			}
			return ;
		}
		
		for (int i = 0; i < total.length; i++) {
			if (current[2 * i] < total[i]) {
				sol.append(symbols[2 * i]);
				stack.offerFirst(symbols[2 * i]);
				current[2 * i]++;
				validParentheses(current, symbols, total, sol, stack, result);
				current[2 * i]--;
				sol.deleteCharAt(sol.length() - 1);
				stack.pollFirst();
			}
		}
		for (int i = 0; i < total.length; i++) {
			if (current[2 * i + 1] < current[2 * i] && stack.peekFirst() == symbols[2 * i]) {
				sol.append(symbols[2 * i + 1]);
				stack.pollFirst();
				current[2 * i + 1]++;
				validParentheses(current, symbols, total, sol, stack, result);
				current[2 * i + 1]--;
				stack.offerFirst(symbols[2 * i]);
				sol.deleteCharAt(sol.length() - 1);
			}
		}
	}
	
	private int arraySum(int[] array) {
		int result = 0;
		for (Integer i : array) {
			result += i;
		}
		return result;
	}
}
