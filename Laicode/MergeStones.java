package laicode;

public class MergeStones {

	public static void main(String[] args) {
		MergeStones test = new MergeStones();
		System.out.println(test.minCost(new int[] {4,2,2,6}));
	
	}

	// TODO : finish correct solution
	public int minCost(int[] stones) {
		int N = stones.length;
		int[][] M = new int[N][N];
		
		for (int i = N - 1; i >= 0; i--) {
			for (int j = i; j < N; j++) {
				System.out.print(i + " " + j + " : ");
				if (j == i) {
					M[i][j] = stones[i];
				} else if (j == i + 1) {
					M[i][j] = stones[i] + stones[j];
				} else {
					int min = Integer.MAX_VALUE;
					for (int k = i; k < j; k++) {
						if (k == i) {
							min = Math.min(min, M[i][k] + M[k + 1][j] + M[k + 1][j]);
						} else if (k == j - 1) {
							min = Math.min(min, M[i][k] + M[i][k] + M[k + 1][j]);
						} else {
							min = Math.min(min, M[i][k] + M[i][k] + M[k + 1][j] + M[k + 1][j]);
						}
					}
					M[i][j] = min;
				}
				System.out.println(M[i][j]);
			}
		}
		return M[0][N - 1];
	}
	
}
