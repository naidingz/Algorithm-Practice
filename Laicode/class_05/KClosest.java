package laicode.class_05;

import java.util.Arrays;

public class KClosest {

    public static void main(String[] args) {
        int[] array = {1,2,3,4,4,5,6,9};
        KClosest test = new KClosest();
        System.out.println(Arrays.toString(test.kClosest(array, 4, 3)));
        
    }
    
    public int[] kClosest(int[] array, int target, int k) {
        if (array == null || array.length == 0) {
            return null;
        }
        int N = array.length;
        int begin = findTarget(array, target);
        int result[] = new int[k];
        int leftPtr = begin, rightPtr = begin + 1;
        for (int i = 0; i < k; ) {
            if (leftPtr >= 0 && rightPtr < N) {
                if (Math.abs(array[leftPtr] - target) < Math.abs(array[rightPtr] - target)) {
                    result[i++] = array[leftPtr--];
                } else if (Math.abs(array[leftPtr] - target) > Math.abs(array[rightPtr] - target)) {
                    result[i++] = array[rightPtr++];
                } else {
                    result[i++] = array[leftPtr--];
                    if (i < k) {
                        result[i++] = array[rightPtr++];
                    }
                }
            } else if (leftPtr >= 0) {
                result[i++] = array[leftPtr--];
            } else {
                result[i++] = array[rightPtr++];
            }
        }
        
        return result;
    }
    
    private int findTarget(int[] array, int target) {
        int left = 0, right = array.length - 1;
        while (left < right - 1) {
            int mid = left + (right - left) / 2;
            if (array[mid] == target) {
                right = mid;
            } else if (array[mid] > target) {
                right = mid;
            } else {
                left = mid;
            }
        }
        if (Math.abs(array[left] - target) <= Math.abs(array[right] - target)) {
            return left;
        } else {
            return right;
        }
    }

}
