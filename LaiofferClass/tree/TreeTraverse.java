package tree;
import java.util.*;

public class TreeTraverse {

	public static void main(String[] args) {
	    
	    //     10
	    //    /  \
	    //   5   15
	    //  / \    \
	    // 2   7    20
		TreeNode node1 = new TreeNode(10);
		TreeNode node2 = new TreeNode(5);
		TreeNode node3 = new TreeNode(15);
		TreeNode node4 = new TreeNode(2);
		TreeNode node5 = new TreeNode(7);
		TreeNode node6 = new TreeNode(20);
		
		node1.left = node2;
		node1.right = node3;
		node2.left = node4;
		node2.right = node5;
		node3.right = node6;
		
//		preOrder(node1);
//		System.out.println();
//		inOrder3(node1);
//		System.out.println();
		postOrder4(node1);
		System.out.println();
		
//		System.out.println(preOrder2(node1));
//		System.out.println(inOrder2(node1));
//		System.out.println(postOrder2(node1));
		
//		levelOrder(node1);
//		System.out.println();
	}
	
    /*----------------------------------------------------------*/
	public static void preOrder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		System.out.print(root.value + " ");
		preOrder(root.left);
		preOrder(root.right);
	}
	
	public static void inOrder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		inOrder(root.left);
		System.out.print(root.value + " ");
		inOrder(root.right);
	}
	
	public static void postOrder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		postOrder(root.left);
		postOrder(root.right);
		System.out.print(root.value + " ");
	}
	
    /*----------------------------------------------------------*/
	public static List<Integer> preOrder2(TreeNode root) {
	    List<Integer> list = new ArrayList<Integer>();
	    if (root == null) {
			return list;
		}
			
		List<Integer> leftList = preOrder2(root.left);
		List<Integer> rightList = preOrder2(root.right);
		list.add(root.value);
		list.addAll(leftList);
		list.addAll(rightList);
	    
	    return list;
	}
	
	public static List<Integer> inOrder2(TreeNode root) {
	    List<Integer> list = new ArrayList<Integer>();
	    if (root == null) {
			return list;
		}
			
		List<Integer> leftList = inOrder2(root.left);
		List<Integer> rightList = inOrder2(root.right);
		list.addAll(leftList);
		list.add(root.value);
		list.addAll(rightList);
	    
	    return list;
	}
	
	public static List<Integer> postOrder2(TreeNode root) {
	    List<Integer> list = new ArrayList<Integer>();
	    if (root == null) {
			return list;
		}
			
		List<Integer> leftList = postOrder2(root.left);
		List<Integer> rightList = postOrder2(root.right);
		list.addAll(leftList);
		list.addAll(rightList);
		list.add(root.value);
	    
	    return list;
	}
	
    /*----------------------------------------------------------*/
	public static void preOrder3(TreeNode root) {
	    if (root == null) {
	        return;
	    }
	    
	    Deque<TreeNode> stack = new LinkedList<>();
	    stack.offerFirst(root);
	    while (!stack.isEmpty()) {
	        TreeNode curr = stack.pollFirst();
	        System.out.print(curr.value + " ");
	        if (curr.right != null) {
	            stack.offerFirst(curr.right);
	        }
	        if (curr.left != null) {
	            stack.offerFirst(curr.left);
	        }
	    }
	}
	
	public static void inOrder3(TreeNode root) {
	    if (root == null) {
	        return;
	    }
	    Deque<TreeNode> stack = new LinkedList<>();
	    TreeNode helper = root;

	    while (helper != null || !stack.isEmpty()) {
	        if (helper != null) {
	            stack.offerFirst(helper);
	            helper = helper.left;
	        } else {
	            TreeNode tmp = stack.pollFirst();
	            System.out.print(tmp.value + " ");
	            helper = tmp.right;
	        }
	    }
	}
	
	public static void postOrder3(TreeNode root) {
	    if (root == null) {
	        return;
	    }
	    Deque<TreeNode> stack = new LinkedList<>();
	    Deque<TreeNode> temp = new LinkedList<>();
	    
	    stack.offerFirst(root);
	    while (!stack.isEmpty()) {
	        TreeNode parent = stack.pollFirst();
	        temp.offerFirst(parent);
	        if (parent.left != null) {
	            stack.offerFirst(parent.left);
	        }
           if (parent.right != null) {
                stack.offerFirst(parent.right);
            }
	    }
	    while (!temp.isEmpty()) {
	        System.out.print(temp.pollFirst().value + " ");
	    }
	}
	
	public static void postOrder4(TreeNode root) {
	    if (root == null) {
	        return;
	    }
	    
	    Deque<TreeNode> stack = new LinkedList<>();
	    TreeNode prev = null;
	    stack.offerFirst(root);
	    
	    while (!stack.isEmpty()) {
	        TreeNode curr = stack.peekFirst();
	        if (prev == null || curr == prev.left || curr == prev.right) {
	            if (curr.left != null) {
	                stack.offerFirst(curr.left);
	            } else if (curr.right != null) {
	                stack.offerFirst(curr.right);
	            } else {
	                System.out.print(curr.value + " ");
	                stack.pollFirst();
	            }
	        } else if (prev == curr.left) {
	            if (curr.right != null) {
	                stack.offerFirst(curr.right);
	            } else {
	                System.out.print(curr.value + " ");
	                stack.pollFirst();
	            }
	        } else {
	            System.out.print(curr.value + " ");
	            stack.pollFirst();
	        }
	        prev = curr;
	    }
	}
	
    /*----------------------------------------------------------*/
	public static void levelOrder(TreeNode root) {
		Queue<TreeNode> q = new LinkedList<>();
		q.offer(root);
		while (!q.isEmpty()) {
		    int size = q.size();
		    for (int i = 0; i < size; i++) {
		        TreeNode curr = q.poll();
		        System.out.print(curr.value + " ");
		        if (curr.left != null) {
		            q.offer(curr.left);
		        }
		        if (curr.right != null) {
		            q.offer(curr.right);
		        }
		    }
		    System.out.println();
		}
	}

    /*----------------------------------------------------------*/
    public List<List<Integer>> layerByLayer(TreeNode root) {
        if (root == null) {
            return new ArrayList<List<Integer>>();
        }

        List<List<Integer>> result = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int num = queue.size();
            List<Integer> thisLayer = new ArrayList<>();
            for (int i = 0; i < num; i++) {
                TreeNode curr = queue.poll();
                thisLayer.add(curr.value);
                if (curr.left != null) {
                    queue.offer(curr.left);
                }
                if (curr.right != null) {
                    queue.offer(curr.right);
                }
            }
            result.add(thisLayer);
        }

        return result;
    }
}
