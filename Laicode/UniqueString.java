package laicode;

public class UniqueString {

    /*
     * Determine if the characters of a given string are all unique.
     * range : 'a' - 'z'
     */
    public boolean allUnique(String word) {
        if (word == null) {
            return false;
        }

        int occurCount = 0;
        for (int i = 0; i < word.length(); i++) {
            int c = word.charAt(i) - 'a';
            if (((occurCount >> c) & 1) == 0) {
                occurCount |= (1 << c);
            } else {
                return false;
            }
        }
        return true;
    }

    /*
     * The range is all ascii code.
     */
    public boolean allUnique2(String word) {
        if (word == null) {
            return false;
        }
        int[] occurCount = new int[8];
        for (int i = 0; i < word.length(); i++) {
            int c = word.charAt(i);
            int row = c / 32, col = c % 32;
            if (((occurCount[row] >> col) & 1) == 0) {
                occurCount[row] |= (1 << c);
            } else {
                return false;
            }
        }
        return true;
    }
}
