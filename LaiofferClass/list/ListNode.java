package list;

public class ListNode {
	int value;
	ListNode next;
	
	public ListNode(int x) {
		this.value = x;
	}
}
