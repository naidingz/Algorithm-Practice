package laicode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MajorityNumber {

	public static void main(String[] args) {
		MajorityNumber test = new MajorityNumber();
		System.out.println(test.majorityI(new int[] {1, 2, 1, 2, 1}));
		System.out.println(test.majorityII(new int[] {3,3,1,1,1,4}));
	}
	
	public class Arena<T> {
		int limit;
		Map<T, Integer> arena;
		public Arena(int limit) {
			this.limit = limit;
			arena = new HashMap<T, Integer>();
		}
		
		public boolean isFull() {
			return arena.size() == limit;
		}
		
		public void fight(T player) {
			if (arena.containsKey(player) || arena.size() < limit) {
				arena.put(player, arena.getOrDefault(player, 0) + 1);
				return;
			}
			Map<T, Integer> oldArena = arena;
			arena = new HashMap<T, Integer>();
			for (Map.Entry<T, Integer> entry : oldArena.entrySet()) {
				if (entry.getValue() - 1 > 0) {
					arena.put(entry.getKey(), entry.getValue() - 1);
				}
			}
		}
		
		public Set<T> winners() {
			return arena.keySet();
		}
	}
	
	
	/*
	 * Given an integer array of length L, 
	 * find the number that occurs more than 0.5 * L times.
	 * 
	 * Assumptions: The given array is not null or empty
	 * It is guaranteed there exists such a majority number
	 * 
	 * Examples
	 * A = {1, 2, 1, 2, 1}, return 1
	 */
	public int majorityI(int[] array) {
		Arena<Integer> arena = new Arena<>(1);
		for (Integer i : array) {
			arena.fight(i);
		}
		int result = -1;
		for (Integer winner : arena.winners()) {
			result = winner;
		}
		return result;
	}
	
	/*
	 * Given an integer array of length L, find all numbers that occur more than 1/3 * L times if any exist.
	 * 
	 * Assumptions: The given array is not null
	 * 
	 * Examples
	 * A = {1, 2, 1, 2, 1}, return [1, 2]
	 * A = {1, 2, 1, 2, 3, 3, 1}, return [1]
	 * A = {1, 2, 2, 3, 1, 3}, return []
	 */
	public List<Integer> majorityII(int[] array) {
		Arena<Integer> arena = new Arena<>(2);
		for (Integer i : array) {
			arena.fight(i);
		}
		Set<Integer> winners = arena.winners();
		List<Integer> result = new ArrayList<>();
		
		for (Integer winner : winners) {
			int count = 0;
			for (int i = 0; i < array.length; i++) {
				count += array[i] == winner ? 1 : 0;
			}
			if (count * 3 > array.length) {
				result.add(winner);
			}
		}
		return result;
	}
	
	public List<Integer> majorityIII(int[] array, int k) {
		Arena<Integer> arena = new Arena<>(k - 1);
		for (Integer i : array) {
			arena.fight(i);
		}
		Set<Integer> winners = arena.winners();
		List<Integer> result = new ArrayList<>();
		
		for (Integer winner : winners) {
			int count = 0;
			for (int i = 0; i < array.length; i++) {
				count += array[i] == winner ? 1 : 0;
			}
			if (count * k > array.length) {
				result.add(winner);
			}
		}
		return result;
	}
}
