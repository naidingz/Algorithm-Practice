package laicode;

import java.util.ArrayList;
import java.util.List;

public class LargestAndSecondLargest {

    public static void main(String[] args) {
        int[] array = {5,3,1,2,6,4};
        LargestAndSecondLargest test = new LargestAndSecondLargest();
        for (Integer i : test.largestAndSecond(array)) {
            System.out.println(i);
        }
    }

    public int[] largestAndSecond(int[] array) {
        if (array.length <= 2) {
            return array;
        }

        List<List<Integer>> compared = new ArrayList<>();
        List<Integer> toCompare = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            toCompare.add(i);
            compared.add(new ArrayList<Integer>());
        }
        while (toCompare.size() > 1) {
            List<Integer> tmp = new ArrayList<>();
            int size = toCompare.size();
            for (int i = 0; i < size / 2; i++) {
                int idx = array[toCompare.get(i)] > array[toCompare.get(size - i - 1)] ? i : size - i - 1;
                tmp.add(toCompare.get(idx));
                compared.get(toCompare.get(idx)).add(array[toCompare.get(size - idx - 1)]);
            }
            if (size % 2 == 1) {
                tmp.add(toCompare.get(size / 2));
            }
            toCompare = tmp;
        }
        int[] result = new int[2];
        result[0] = array[toCompare.get(0)];
        List<Integer> l = compared.get(toCompare.get(0));
        result[1] = l.get(0);
        for (int i = 1; i < l.size(); i++) {
            result[1] = result[1] > l.get(i) ? result[1] : l.get(i);
        }
        return result;
    }
}
