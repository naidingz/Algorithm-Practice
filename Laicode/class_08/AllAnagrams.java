package laicode.class_08;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AllAnagrams {
    public static void main(String[] args) {
        String s = "abcbacab";
        AllAnagrams test = new AllAnagrams();
        for (Integer i : test.allAnagrams(s, "ab")) {
            System.out.print(i + " ");
        }
    }

    public List<Integer> allAnagrams(String s, String l) {
        // corner case
        if (s == null || l == null) {
            return null;
        }
        List<Integer> result = new ArrayList<>();
        if (s.length() == 0 || l.length() == 0 || s.length() < l.length()) {
            return result;
        }
        // count occurrence map
        HashMap<Character, Integer> occurMap = new HashMap<>();
        for (int i = 0; i < l.length(); i++) {
            Character c = l.charAt(i);
            occurMap.put(c, occurMap.getOrDefault(c, 0) + 1);
        }

        // initializa
        int left = 0, right = l.length() - 1, toMatch = occurMap.size();
        for (int i = 0; i < l.length(); i++) {
            Character c = s.charAt(i);
            if (occurMap.containsKey(c)) {
                occurMap.put(c, occurMap.get(c) - 1);
                if (occurMap.get(c) == 0) {
                    toMatch--;
                }
            }
        }

        while (right < s.length()) {
            if (toMatch == 0) {
                result.add(left);
            }
            if (right + 1 >= s.length()) {
                break;
            }
            Character c1 = s.charAt(left++);
            Character c2 = s.charAt(++right);
            if (occurMap.containsKey(c1)) {
                occurMap.put(c1, occurMap.get(c1) + 1);
                if (occurMap.get(c1) == 1) {
                    toMatch++;
                }
            }
            if (occurMap.containsKey(c2)) {
                occurMap.put(c2, occurMap.get(c2) - 1);
                if (occurMap.get(c2) == 0) {
                    toMatch--;
                }
            }
        }
        return result;
    }
}
