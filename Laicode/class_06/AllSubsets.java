package laicode.class_06;

import java.util.ArrayList;
import java.util.List;

public class AllSubsets {

    public static void main(String[] args) {
        String a = "abc";
        AllSubsets test = new AllSubsets();
        for (String s : test.subSets(a)) {
            System.out.println(s);
        }
    }

    public List<String> subSets(String set) {
        List<String> result = new ArrayList<>();
        if (set == null) {
            return result;
        }
        subSets(set.toCharArray(), 0, new StringBuilder(), result);
        return result;
    }

    private void subSets(char[] input, int index, StringBuilder sb, List<String> result) {
        if (input.length == index) {
            result.add(sb.toString());
            return;
        }

        sb.append(input[index]);
        subSets(input, index + 1, sb, result);
        sb.deleteCharAt(sb.length() - 1);
        subSets(input, index + 1, sb, result);
    }
}
