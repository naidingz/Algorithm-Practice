package laicode;

public class CuttingRope {

	public static void main(String[] args) {
		CuttingRope test = new CuttingRope();
		System.out.println(test.maxProductIII(8));
	}
	
	/*
	 * Given a rope with positive integer-length n, 
	 * how to cut the rope into m integer-length parts 
	 * with length p[0], p[1], ...,p[m-1], 
	 * in order to get the maximal product of p[0]p[1] ... p[m-1]? 
	 * m is determined by you and must be greater than 0 (at least one cut must be made). 
	 * Return the max product you can have.
	 * 
	 * Assumption: n >= 2
	 * 
	 * maxProductI: pure recursion method
	 * maxProductII: dp 1
	 * maxProductIII: dp 2
	 */
	public int maxProductI(int n) {
		if (n <= 2) {
			return 1;
		}
		int globalMax = 0;
		for (int i = 1; i < n; i++) {
			globalMax = Math.max(globalMax, i * Math.max(n - i, maxProductI(n - i)));
		}
		return globalMax;
	}
	
	public int maxProductII(int n) {
		int[] M = new int[n + 1];
		M[0] = 0;
		M[1] = 0;
		for (int i = 2; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				M[i] = Math.max(M[i], Math.max(j, M[j]) * Math.max(i - j, M[i - j]));
			}
		}
		return M[n];
	}
	
	public int maxProductIII(int n) {
		int[] M = new int[n + 1];
		M[0] = 0;
		M[1] = 0;
		for (int i = 2; i <= n; i++) {
			for (int j = 1; j < i; j++) {
				M[i] = Math.max(M[i], j * Math.max(i - j, M[i - j]));
			}
		}
		return M[n];
	}

}
