package sort;

public class MergeSort {
	public static void main(String[] args) {
		int [] array = {5, 3, -7, -1, 9};
		MergeSort sol = new MergeSort();
		sol.sort(array);
		for (int i : array) {
			System.out.print(i + " ");
		}
	}
	
	public void sort(int[] array) {
		if (array == null || array.length == 0) {
			return;
		}
		
		mergeSort(array, 0, array.length - 1);
	}
	
	private void mergeSort(int[] array, int left, int right) {
		if (right == left) {
			return;
		} else {
			int mid = left + (right - left) / 2;
			mergeSort(array, left, mid);
			mergeSort(array, mid + 1, right);
			merge(array, left, mid, right);
		}
	}
	
	private void merge(int[] array, int left, int mid, int right) {
		int[] helper = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			helper[i] = array[i];
		}
		
		int lp = left;
		int rp = mid + 1;
		int p = left;
		while((lp < mid + 1) && (rp < right + 1)) {
			if (helper[lp] < helper[rp]) {
				array[p++] = helper[lp++];
			} else {
				array[p++] = helper[rp++];
			}
		}

		while (lp <= mid) {
			array[p++] = helper[lp++];
		}
	}
}
