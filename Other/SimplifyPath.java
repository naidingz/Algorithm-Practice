import java.util.Deque;
import java.util.LinkedList;

public class SimplifyPath {
    public static void main(String[] args) {
        String[] ss = "/a/b/c".split("/");
        for (String si : ss) {
            System.out.println(si + " - ");
        }

        SimplifyPath test = new SimplifyPath();
        System.out.println(test.simplify("/a/./b/../../c/"));
    }


    public String simplify(String path) {
        if (path == null) {
            return null;
        }
        if (path.length() == 0) {
            return "/";
        }
        String[] pathArray = path.split("/");
        Deque<String> stack = new LinkedList<>();
        for (String s : pathArray) {
            if (s.equals(".")) {
                continue;
            } else if (s.equals("..")) {
                stack.pollFirst();
            } else {
                stack.offerFirst(s);
            }
        }
        StringBuilder sb = new StringBuilder();
        if (!stack.isEmpty() && stack.peekLast().equals("")) {
            stack.pollLast();
        }
        while (!stack.isEmpty()) {
            sb.append("/" + stack.pollLast());
        }
        if (sb.length() == 0) {
            sb.append("/");
        }
        return sb.toString();

    }
}
