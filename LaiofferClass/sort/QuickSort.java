package sort;
import java.util.*;

public class QuickSort {

	public static void main(String[] args) {
		int [] array = {0, 5, 3, 3, -7, -1, 9};
		QuickSort sol = new QuickSort();
		sol.sort(array);
		for (int i : array) {
			System.out.print(i + " ");
		}
	}
	
	public void sort(int[] array) {
		if (array == null || array.length <= 1) {
			return;
		}
		sort(array, 0, array.length - 1);
	}
	
	private void sort(int[] array, int left, int right) {
		if (left >= right) {
			return;
		}
		
		Random rand = new Random();
		int pivotIndex = left + rand.nextInt(right - left + 1);
		swap(array, pivotIndex, right);
		
		int l = left, r = right - 1;
//		while (l <= r) {
//			if (array[l] <= array[right]) {
//				l++;
//			} else {
//				swap(array, l, r--);
//			}
//		}
		while (l <= r) {
			if (array[l] < array[right]) {
				l++;
			} else if (array[r] >= array[right]) {
				r--;
			} else {
				swap(array, l++, r--);
			}
		}
		
		swap(array, right, l);
		sort(array, left, l - 1);
		sort(array, l + 1, right);
	}
	
	private void swap(int[] array, int x, int y) {
		int temp = array[x];
		array[x] = array[y];
		array[y] = temp;
	}
}
