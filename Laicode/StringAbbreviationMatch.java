package laicode;

public class StringAbbreviationMatch {

	public static void main(String[] args) {
		StringAbbreviationMatch test = new StringAbbreviationMatch();
		System.out.println(test.match("sophisticated", "13"));
	}

	public boolean match(String input, String pattern) {
		return helper(input, pattern, 0, 0);
	}

	private boolean helper(String input, String pattern, int in, int pt) {
		if (in == input.length() && pt == pattern.length()) {
			return true;
		} else if (in == input.length() || pt == pattern.length()) {
			return false;
		}

		if (pattern.charAt(pt) >= 'A') {
			if (pattern.charAt(pt) == input.charAt(in)) {
				return helper(input, pattern, in + 1, pt + 1);
			} else {
				return false;
			}
		} else {
			int len = 0;
			while (pt < pattern.length() && pattern.charAt(pt) < 'A') {
				len = len * 10 + pattern.charAt(pt++) - '0';
			}
			in += len;
			if (in <= input.length()) {
				return helper(input, pattern, in, pt);
			} else {
				return false;
			}
		}
	}
}
