import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NumberOfIsland {
	
	public static void main(String[] args) {
		NumberOfIsland test = new NumberOfIsland();
		Point[] points = new Point[4];
		points[0] = new Point(0, 0);
		points[1] = new Point(1, 1);
		points[2] = new Point(1, 0);
		points[3] = new Point(0, 1);
		
		System.out.println(test.numIslands2(2, 2, points));
	}
	
	static class Point {
		int x;
		int y;
		Point() { x = 0; y = 0; }
		Point(int a, int b) { x = a; y = b; }
	}
	
	static class Node {
        boolean island;
        int r;
        int c;
        Node() { this.island = false; }
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + c;
			result = prime * result + r;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Node other = (Node) obj;
			if (c != other.c)
				return false;
			if (r != other.r)
				return false;
			return true;
		}
    }
	
	public List<Integer> numIslands2(int n, int m, Point[] operators) {
        List<Integer> result = new ArrayList<>();
        if (n == 0 || m == 0 || operators == null) {
            return result;
        }
        int total = 0;
        Node[][] map = new Node[n][m];
        	for (int i = 0; i < n; i++) {
        		for (int j = 0; j < m; j++) {
        			map[i][j] = new Node();
        		}
        	}
        for (int i = 0; i < operators.length; i++) {
            Point p = operators[i];
            Set<Node> set = new HashSet<>();
            if (p.x - 1 >= 0 && map[p.x - 1][p.y].island) {
                set.add(root(map[p.x - 1][p.y], map));
            }
            if (p.x + 1 < n && map[p.x + 1][p.y].island) {
                set.add(root(map[p.x + 1][p.y], map));
            }
            if (p.y - 1 >= 0 && map[p.x][p.y - 1].island) {
                set.add(root(map[p.x][p.y - 1], map));
            }
            if (p.y + 1 < m && map[p.x][p.y + 1].island) {
                set.add(root(map[p.x][p.y + 1], map));
            }
            if (set.size() == 0) {
                map[p.x][p.y].r = p.x;
                map[p.x][p.y].c = p.y;
                map[p.x][p.y].island = true;
                total++;
            } else {
                Node root = null;
                for (Node node : set) {
                    if (root == null) {
                        root = node;
                    } else {
                        node.r = root.r;
                        node.c = root.c;
                        total--;
                    }
                }
                map[p.x][p.y].r = root.r;
                map[p.x][p.y].c = root.c;
                map[p.x][p.y].island = true;
            }
            result.add(total);
        }    
        return result;
    }
    
    private Node root(Node node, Node[][] map) {
        while (node.r != map[node.r][node.c].r || 
               node.c != map[node.r][node.c].c) {
            node = map[node.r][node.c];
        }
        return node;
    }
}
