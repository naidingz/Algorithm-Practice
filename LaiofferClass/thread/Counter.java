package thread;

public class Counter {

	private int value;
	
	public Counter(int amount) {
		value = amount;
	}
	
	public void decrease(int amount) {
//		synchronized (this) {
			System.out.println(Thread.currentThread());
			if (value >= amount) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				value -= amount;
				System.out.println("Withdraw " + amount + " dollars successfully.");
			} else {
				System.out.println("Balance not enough.");
			}

//		}
	}
	
	
	public static void main(String[] args) {
		System.out.println("Hello");
		Counter c = new Counter(1500);
		
		Thread t1 = new Thread() {
			@Override
			public void run() {
				c.decrease(1000);
			}
		};
		
		Thread t2 = new Thread() {
			@Override
			public void run() {
				c.decrease(1000);
			}
		};
		
		t1.start();
		t2.start();
		
		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} finally {
			System.out.println(c.value);
		}
	}
		
		

}
