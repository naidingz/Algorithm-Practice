public class BSTToDoublyList {
    class DoublyListNode {
        int val;
        DoublyListNode next, prev;

        DoublyListNode(int val) {
            this.val = val;
            this.next = this.prev = null;
        }
    }

    class TreeNode {
        public int val;
        public TreeNode left, right;

        public TreeNode(int val) {
            this.val = val;
            this.left = this.right = null;
        }
    }

    public DoublyListNode bstToDoublyList(TreeNode root) {
        if (root == null) {
            return null;
        }
        DoublyListNode node = helper(root);
        while (node.prev != null) {
            node = node.prev;
        }
        return node;
    }
    
    public DoublyListNode helper(TreeNode root) {
        if (root == null) {
            return null;
        }

        DoublyListNode node = new DoublyListNode(root.val);
        if (root.left != null) {
            DoublyListNode left = helper(root.left);
            while (left.next != null) {
                left = left.next;
            }
            node.prev = left;
            left.next = node;
        }
        if (root.right != null) {
            DoublyListNode right = helper(root.right);
            while (right.prev != null) {
                right = right.prev;
            }
            node.next = right;
            right.prev = node;
        }
        return node;
    }
}