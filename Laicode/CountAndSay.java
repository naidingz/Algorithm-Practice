package laicode;

public class CountAndSay {


    public static void main(String[] args) {
        CountAndSay test = new CountAndSay();
        System.out.println(test.countAndSay(4));
    }

    /*
     * Given a sequence of number: 1, 11, 21, 1211, 111221, …
     * The rule of generating the number in the sequence is as follow:
     * 1 is "one 1" so 11.
     * 11 is "two 1s" so 21.
     * 21 is "one 2 followed by one 1" so 1211.
     * Find the nth number in this sequence.
     */
    public String countAndSay(int n) {
        if (n == 0) {
            return "";
        }
        if (n == 1) {
            return "1";
        }
        return count(say(countAndSay(n - 1)));
    }

    private String count(String s) {
        String[] check = s.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String si : check) {
            if (si.equals("followed") || si.equals("by")) {
                continue;
            }
            if (si.length() == 1 || si.charAt(si.length() - 1) == 's') {
                sb.append(si.charAt(0));
            } else {
                sb.append(stringToNumber(si));
            }
        }

        return sb.toString();
    }

    private String say(String num) {
        StringBuilder sb = new StringBuilder();
        int ptr = 0, count = 1;
        while (ptr < num.length()) {
            while (ptr != num.length() - 1 && num.charAt(ptr) == num.charAt(ptr + 1)) {
                ptr++;
                count++;
            }
            sb.append(numToString(String.valueOf(count)));
            sb.append(" ");
            sb.append(num.charAt(ptr));
            sb.append(count == 1 ? " " : "s ");
            if (ptr != num.length() - 1) {
                sb.append("followed by ");
            }
            ptr++;
            count = 1;
        }

        return sb.toString();
    }

    private String stringToNumber(String s) {
        if (s == null) {
            return null;
        } else if (s.equals("zero")) {
            return "0";
        } else if (s.equals("one")) {
            return "1";
        } else if (s.equals("two")) {
            return "2";
        } else if (s.equals("three")) {
            return "3";
        } else if (s.equals("four")) {
            return "4";
        } else if (s.equals("five")) {
            return "5";
        } else if (s.equals("six")) {
            return "6";
        } else if (s.equals("seven")) {
            return "7";
        } else if (s.equals("eight")) {
            return "8";
        } else if (s.equals("nine")) {
            return "9";
        } else {
            return "0";
        }
    }

    private String numToString(String num) {
        if (num == null) {
            return null;
        } else if (num.equals("0")) {
            return "zero";
        } else if (num.equals("1")) {
            return "one";
        } else if (num.equals("2")) {
            return "two";
        } else if (num.equals("3")) {
            return "three";
        } else if (num.equals("4")) {
            return "four";
        } else if (num.equals("5")) {
            return "five";
        } else if (num.equals("6")) {
            return "six";
        } else if (num.equals("7")) {
            return "seven";
        } else if (num.equals("8")) {
            return "eight";
        } else if (num.equals("9")) {
            return "night";
        } else {
            return "zero";
        }
    }

}
