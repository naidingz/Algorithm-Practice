package laicode;

import java.util.ArrayList;
import java.util.List;

public class BinaryTreePaths {

    public static void main(String[] args) {

        TreeNode t1 = new TreeNode(3);
        TreeNode t2 = new TreeNode(9);
        TreeNode t3 = new TreeNode(8);
        TreeNode t4 = new TreeNode(4);
        TreeNode t5 = new TreeNode(0);
        TreeNode t6 = new TreeNode(1);
        TreeNode t7 = new TreeNode(7);
        TreeNode t8 = new TreeNode(2);
        TreeNode t9 = new TreeNode(5);

        t1.left = t2;
        t1.right = t3;
        t2.left = t4;
        t2.right = t5;
        t5.right = t8;
        t3.left = t6;
        t3.right = t7;
        t6.left = t9;

        BinaryTreePaths test = new BinaryTreePaths();
        for (String s : test.binaryTreePaths(t1)) {
            System.out.println(s);
        }
    }

    private static class TreeNode {
        public int key;
        public TreeNode left;
        public TreeNode right;
        public TreeNode(int key) {
            this.key = key;
        }
    }

    public String[] binaryTreePaths(TreeNode root) {
        if (root == null) {
            return new String[0];
        }

        List<String> result = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        helper(root, sb, result);
        String[] arr = new String[result.size()];
        arr = result.toArray(arr);
        return arr;
    }

    private void helper(TreeNode root, StringBuilder sb, List<String> result) {
        if (root.left == null && root.right == null) {
            sb.append(root.key);
            result.add(sb.toString());
            return;
        }
        if (root.left != null) {
            sb.append(root.key + "->");
            helper(root.left, sb, result);
            backTrack(sb);
            if (root.right != null) {
                backTrack(sb);
            }
        }
        if (root.right != null) {
            sb.append(root.key + "->");
            helper(root.right, sb, result);
            backTrack(sb);
            backTrack(sb);
        }
    }

    private void backTrack(StringBuilder sb) {
        if (sb.length() == 0) {
            return ;
        }

        if (sb.charAt(sb.length() - 1) == '>') {
            sb.delete(sb.length() - 2, sb.length());
        }
        int i = sb.length() - 1;
        while (i >= 0 && sb.charAt(i) != '>') {
            i--;
        }
        sb.delete(i + 1, sb.length());
    }
}
