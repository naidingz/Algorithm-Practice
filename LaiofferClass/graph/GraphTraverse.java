package graph;
import java.util.*;

public class GraphTraverse {

    public static void main(String[] args) {

    }
    
    public static void bfs(List<GraphNode> graph) {
        for (GraphNode start : graph) {
            if (!start.visited) {
                Queue<GraphNode> q = new LinkedList<>();
                start.visited = true;
                q.offer(start);
                while (!q.isEmpty()) {
                    GraphNode curr = q.poll();
                    System.out.println(curr.value);
                    for (GraphNode nei : curr.nei) {
                        if (!nei.visited) {
                            nei.visited = true;
                            q.offer(nei);
                        }
                    }
                }
            }
        }
    }
}
