package laicode;

import java.util.List;

public class RemoveDuplicatedKeyInLinkedList {

    public static void main(String[] args) {
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(1);
        ListNode n3 = new ListNode(2);
        ListNode n4 = new ListNode(2);
        ListNode n5 = new ListNode(3);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        RemoveDuplicatedKeyInLinkedList test = new RemoveDuplicatedKeyInLinkedList();

        test.printLL(test.removeDup(n1));
    }

    private void printLL(ListNode head) {
        while (head != null) {
            System.out.print(head.value + " ");
            head = head.next;
        }
    }

    private static class ListNode {
        int value;
        ListNode next;

        ListNode(int value) {
            this.value = value;
            next = null;
        }
    }

    public ListNode removeDup(ListNode head) {
        if (head == null) {
            return null;
        }

        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        ListNode prev = dummyHead, curr = head;
        while (curr != null) {
            boolean isDup = false;
            while (curr.next != null && curr.value == curr.next.value) {
                isDup = true;
                curr = curr.next;
            }
            if (isDup) {
                curr = curr.next;
                prev.next = curr;
            } else {
                prev = prev.next;
                curr = curr.next;
            }
        }
        return dummyHead.next;
    }

}
