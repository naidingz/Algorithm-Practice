package stack;
import java.util.*;

public class MinStack {
	
	Deque<Integer> stack = new LinkedList<>();
	Deque<Integer> min = new LinkedList<>();
	
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	public int size() {
		return stack.size();
	}
	
	public void push(int element) {
		stack.push(element);
		if (min.isEmpty() ||  element <= min.peek()) {
			min.push(element);
		}
	}
	
	public Integer pop() {
		if (stack.isEmpty()) {
			return null;
		}
		int t = stack.pop();
		if (min.peek() == t) {
			min.pop();
		}
		return t;
	}
	
	public Integer peek() {
		if (stack.isEmpty()) {
			return null;
		}
		return stack.peek();
	}
	
	public Integer min() {
		if (min.isEmpty()) {
			return null;
		}
		return min.peek();
	}

}
