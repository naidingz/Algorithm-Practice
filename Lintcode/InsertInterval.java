import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InsertInterval {

	public static void main(String[] args) {
		InsertInterval test = new InsertInterval();
		
		List<Interval> list = new ArrayList<>();
		list.add(new Interval(1, 5));
		list.add(new Interval(7, 8));
		list.add(new Interval(10, 13));
//		list.add(new Interval(13, 15));
		
		List<Interval> result = test.insert(list, new Interval(6, 6));
		for (Interval i : result) {
			System.out.println("(" + i.start + ", " + i.end + ")");
		}
	}
	
	public static class Interval {
		int start, end;
		Interval(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}
	
	public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
		List<Interval> result = new ArrayList<>();
        if (intervals == null || intervals.size() == 0) {
            result.add(newInterval);
            return result;
        }
        
		int[] help = search(intervals, newInterval);
		for (int i = 0; i < help[0]; i++) {
			result.add(intervals.get(i));
		}
		
		if (help[0] != intervals.size()) {
			newInterval.start = Math.min(intervals.get(help[0]).start, newInterval.start);
		}
		
		if (help[1] == intervals.size() || help[1] != 0) {
			newInterval.end = Math.max(intervals.get(help[1] - 1).end, newInterval.end);
		}
		
		if (result.size() > 0 && result.get(result.size() - 1).end == newInterval.start) {
			result.get(result.size() - 1).end = newInterval.end;
		} else {
			result.add(newInterval);
		}
		
		for (int i = help[1]; i < intervals.size(); i++) {
			if (i == help[1] && intervals.get(i).start == result.get(result.size() - 1).end) {
				result.get(result.size() - 1).end = intervals.get(i).end;
			} else {
				result.add(intervals.get(i));
			}
		}
		
		return result;
    }
	
	private int[] search(List<Interval> intervals, Interval newInterval) {
        int[] result = new int[]{-1, -1};
        int left = 0, right = intervals.size() - 1;
        while (left < right - 1) {
            int mid = left + (right - left) / 2;
            Interval midInterval = intervals.get(mid);
            if (midInterval.end == newInterval.start) {
                result[0] = mid + 1;
                break;
            } else if (midInterval.end < newInterval.start) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        
        if (result[0] == -1) {
            if (intervals.get(left).end > newInterval.start) {
                result[0] = left;
            } else if (intervals.get(right).end > newInterval.start) {
                result[0] = right;
            } else {
                result[0] = right + 1;
            }
        }
        
        left = 0; right = intervals.size() - 1;
        while (left < right - 1) {
            int mid = left + (right - left) / 2;
            Interval midInterval = intervals.get(mid);
            if (midInterval.start == newInterval.end) {
                result[1] = mid + 1;
                break;
            } else if (midInterval.start < newInterval.end) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        
        if (result[1] == -1) {
            if (intervals.get(left).start > newInterval.end) {
                result[1] = left;
            } else if (intervals.get(right).start > newInterval.end) {
                result[1] = right;
            } else {
                result[1] = right + 1;
            }
        }
        return result;
    }
}
