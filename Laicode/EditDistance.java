package laicode;

public class EditDistance {

	public static void main(String[] args) {
		EditDistance test = new EditDistance();
		System.out.println(test.editDistanceII("naiding", "nding"));
	}
	
	/*
	 * Given two strings of alphanumeric characters, 
	 * determine the minimum number of Replace, Delete, and Insert operations 
	 * needed to transform one string into the other.
	 * 
	 * Assumption: Both strings are not null
	 * Method I: recursion
	 * Method II: dp
	 */
	public int editDistanceI(String one, String two) {
		if (one.isEmpty()) {
			return two.length();
		}
		if (two.isEmpty()) {
			return one.length();
		}
		
		if (one.charAt(0) == two.charAt(0)) {
			return editDistanceI(one.substring(1), two.substring(1));
		} else {
			int replace = 1 + editDistanceI(one.substring(1), two.substring(1));
			int insert = 1 + editDistanceI(one.substring(1), two);
			int delete = 1 + editDistanceI(one, two.substring(1));
			return Math.min(replace, Math.min(insert, delete));
		}
	}
	
	public int editDistanceII(String one, String two) {
		int[][] M = new int[one.length() + 1][two.length() + 1];
		for (int i = 1; i <= one.length(); i++) {
			M[i][0] = i;
		}
		for (int j = 1; j <= two.length(); j++) {
			M[0][j] = j;
		}
		
		for (int i = 1; i <= one.length(); i++) {
			for (int j = 1; j <= two.length(); j++) {
				if (one.charAt(i - 1) == two.charAt(j - 1)) {
					M[i][j] = M[i - 1][j - 1];
				} else {
					M[i][j] = 1 + Math.min(M[i - 1][j - 1], Math.min(M[i - 1][j], M[i][j - 1]));
				}
			}
		}
		
		return M[one.length()][two.length()];
	}

	
}
