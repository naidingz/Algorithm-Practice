
import lintcode.InorderTraversal.TreeNode;
import sun.net.www.content.text.plain;

public class BinaryTreeLongestConsecutiveSequence {
    
    /**
     * vertical path
     * @param root
     * @return
     */
    public int longestConsecutive(TreeNode root) {
        int[] globalMax = new int[]{0};
        longestConsecutive(root, 0, 0, globalMax);
        return globalMax[0];
    }

    private void longestConsecutive(TreeNode node, int lastVal, int lastLen, int[] globalMax) {
        if (node == null) {
            return ;
        }
        lastLen = node.val == lastVal + 1 ? lastLen + 1 : 1;
        globalMax[0] = Math.max(globalMax[0], lastLen);
        longestConsecutive(node.left, node.val, lastLen, globalMax);
        longestConsecutive(node.right, node.val, lastLen, globalMax);
    }

    /**
     * any node to any node
     */
    public int longestConsecutive2(TreeNode root) {
        int[] globalMax = new int[]{0};
        longestConsecutive2(root, globalMax);

        return globalMax[0];
    }

    private int[] longestConsecutive2(TreeNode node, int[] globalMax) {
        if (node == null) {
            return new int[]{0, 0};
        }
        int[] left = longestConsecutive2(node.left, globalMax);
        int[] right = longestConsecutive2(node.right, globalMax);

        int increasing = 0, decreasing = 0;
        if (node.left != null) {
            if (node.left.val == node.val + 1) {
                increasing = Math.max(increasing, left[0] + 1);
            }
            if (node.left.val == node.val - 1) {
                decreasing = Math.max(decreasing, left[1] + 1);
            }
        }
        if (node.right != null) {
            if (node.right.val == node.val + 1) {
                increasing = Math.max(increasing, right[0] + 1);
            }
            if (node.right.val == node.val - 1) {
                decreasing = Math.max(decreasing, right[1] + 1);
            }
        }
        globalMax[0] = Math.max(globalMax[0], increasing + 1 + decreasing);

        return new int[] {increasing, decreasing};
    }

}