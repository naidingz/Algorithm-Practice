package laicode;

import java.util.ArrayList;
import java.util.List;

public class NQueensMultiThreads {

	public static void main(String[] args) {
		NQueensMultiThreads test = new NQueensMultiThreads();
		List<List<Integer>> result = new ArrayList<>();
		
		int n = 15;
		
		Thread[] threads = new Thread[n];
		for (int i = 0; i < threads.length; i++) {
			int col = i;
			threads[i] = new Thread() {
				@Override
				public void run() {
					test.nqueens(n, new int[] {col, col+1}, result);
				}
			};
			threads[i].start();
		}
		
		try {
			for (int i = 0; i < threads.length; i++) {
				threads[i].join();
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} finally {
			for (List<Integer> l : result) {
				System.out.println(l);
			}
		}
	}
	
	public void nqueens(int n, int[] firstRange, List<List<Integer>> result) {
		helper(0, new ArrayList<Integer>(), n, firstRange, result);
	}
	
	private void helper(int curr, List<Integer> oneSol, int n, int[] firstRange, List<List<Integer>> result) {
		if (curr == n) {
			result.add(new ArrayList<Integer>(oneSol));
		}
		int left = 0, right = n;
		if (curr == 0) {
			left = firstRange[0];
			right = firstRange[1];
		}
		for (int i = left; i < right; i++) {
			if (isValid(oneSol, i)) {
				oneSol.add(i);
				helper(curr + 1, oneSol, n, firstRange, result);
				oneSol.remove(oneSol.size() - 1);
			}
		}
	}
	
	private boolean isValid(List<Integer> sol, int col) {
		int currRow = sol.size();
		for (int row = 0; row < sol.size(); row++) {
			if (sol.get(row) == col) {
				return false;
			} else if (Math.abs(sol.get(row) - col) == Math.abs(row - currRow)) {
				return false;
			}
		}
		return true;
	}

}
