package laicode;

public class LongestCommonSubstring {

    public static void main(String[] args) {
        LongestCommonSubstring test = new LongestCommonSubstring();
        System.out.println(test.longestCommon("ababcab","bdfe"));
        
        	String a = "";
        	System.out.println(a.length());
    }

	public String longestCommon(String s, String t) {
		int M = s.length();
		int N = t.length();
		String result = "";

		int[][] mat = new int[M][N];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				if (i == 0) {
					mat[i][j] = s.charAt(i) == t.charAt(j) ? 1 : 0;
				} else if (j == 0) {
					mat[i][j] = s.charAt(i) == t.charAt(j) ? 1 : 0;
				} else {
					if (s.charAt(i) == t.charAt(j)) {

						mat[i][j] = mat[i - 1][j - 1] + 1;
					} else {
						mat[i][j] = 0;
					}
				}
				if (mat[i][j] > result.length()) {
					result = s.substring(i - mat[i][j] + 1, i + 1);
				}
			}
		}
		return result;
	}
    
//    public String longestCommon(String s, String t) {
//        if (s == null || t == null || s.length() == 0 || t.length() == 0) {
//            return "";
//        }
//        int m = s.length();
//        int n = t.length();
//
//        String result = "";
//
//        for (int left = - n + 1; left <= m - 1; left++) {
//            int count = 0;
//            for (int i = 0; i < n; i++) {
//                if (left + i < 0) {
//                    continue;
//                }
//                if (left + i >= m) {
//                    break;
//                }
//                if (s.charAt(left + i) == t.charAt(i)) {
//                    count++;
//                    if (count > result.length()) {
//                        result = t.substring(i - count + 1, i + 1);
//                    }
//                } else {
//                    count = 0;
//                }
//            }
//        }
//        return result;
//    }
}
