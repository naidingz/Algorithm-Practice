import java.util.List;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.ArrayList;

public class DirectedGraphLoop {

    public static void main(String[] args) {
        DirectedGraphLoop test = new DirectedGraphLoop();
        System.out.println(test.isCyclicGraph(new int[] {1,2,3}, new int[] {2,3,1}));    
    }

    /**
     * @param start: The start points set
     * @param end: The end points set
     * @return: Return if the graph is cyclic
     */
    public boolean isCyclicGraph(int[] start, int[] end) {
        Map<Integer, Integer> inDegree = new HashMap<>();
        Map<Integer, List<Integer>> neiMap = new HashMap<>();
        for (int i = 0; i < start.length; i++) {
            inDegree.put(start[i], inDegree.getOrDefault(start[i], 0));
            inDegree.put(end[i], inDegree.getOrDefault(end[i], 0) + 1);

            List<Integer> neis = neiMap.get(start[i]);
            neis = (neis == null ? new ArrayList<Integer>() : neis);
            neis.add(end[i]);
            neiMap.put(start[i], neis);
       }

       Queue<Integer> queue = new LinkedList<>();
       for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
           if (entry.getValue() == 0) {
               queue.offer(entry.getKey());
           }
       }
       int count = 0;
       while (!queue.isEmpty()) {
            Integer node = queue.poll();
            count++;
            List<Integer> neis = neiMap.get(node);
            if (neis != null) {
                for (Integer nei : neis) {
                    Integer in = inDegree.get(nei);
                    inDegree.put(nei, in - 1);
                    if (in - 1 == 0) {
                        queue.offer(nei);
                    }
                } 
            }
        }
        
        return count != inDegree.size();
    }
}