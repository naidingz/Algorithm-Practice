import java.util.Random;

public class ShuffleLL {

    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        
        print(n1);
        print(shuffle(n1));
    }
    
    private static void print(Node head) {
        while(head != null) {
            System.out.print(head.value + " ");
            head = head.next;
        }
        System.out.println();
    }
    
    private static class Node {
        int value;
        Node next;
        Node(int v) {
            value = v;
        }
    }
    
    
    public static Node shuffle(Node head) {
        
        if (head == null || head.next == null) {
            return head;
        }
        
        Node middle = findMiddle(head);
        Node middleNext = middle.next;
        middle.next = null;
        
        Node h1 = shuffle(head);
        Node h2 = shuffle(middleNext);
  
        return randomMerge(h1, h2);
    }
    
    private static Node findMiddle(Node head) {
        Node slow = head;
        Node fast = head;
        
        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }
    
    private static Node randomMerge(Node h1, Node h2) {
        
        Node newHead = new Node(0);
        Node pos = newHead;
        Random rand = new Random();
        while (h1 != null || h2 != null) {
            int tag = rand.nextInt(2);
            if (tag == 0 && h1 != null) {
                pos.next = h1;
                h1 = h1.next;
                pos = pos.next;
            }
            if (tag == 1 && h2 != null) {
                pos.next = h2;
                h2 = h2.next;
                pos = pos.next;
            }
        }
        
        return newHead.next;
    }
}
