package laicode;

import java.util.PriorityQueue;

public class MaxWaterTrapped {

	public static void main(String[] args) {
		MaxWaterTrapped test = new MaxWaterTrapped();
		System.out.println(test.maxTrapped(new int[] {5,3,1,4,6,2,3}));
		System.out.println(test.maxTrapped(new int[][] {{2, 3, 4, 2},
				   									    {3, 1, 2, 3},
				                                        {4, 3, 5, 4}}));

			 
	}
	
	/*
	 * Given a non-negative integer array representing the heights of a list of adjacent bars. 
	 * Suppose each bar has a width of 1. 
	 * Find the largest amount of water that can be trapped in the histogram.
	 * 
	 * Assumptions: The given array is not null
	 * Examples: { 2, 1, 3, 2, 4 }, 
	 * the amount of water can be trapped is 1 + 1 = 2 
	 * (at index 1, 1 unit of water can be trapped and index 3, 1 unit of water can be trapped)
	 */
	public int maxTrapped(int[] array) {
		if (array == null || array.length == 0) {
			return 0;
		}
		int i = 0, j = array.length - 1;
		int leftMax = 0, rightMax = 0;
		int result = 0;
		while (i <= j) {
			leftMax = Math.max(array[i], leftMax);
			rightMax = Math.max(array[j], rightMax);
			if (leftMax < rightMax) {
				result += (leftMax - array[i]);
				i++;
			} else {
				result += (rightMax - array[j]);
				j--;
			}
		}
		return result;
	}
	
	/*
	 * Given a non-negative integer 2D array representing the heights of bars in a matrix. 
	 * Suppose each bar has length and width of 1. 
	 * Find the largest amount of water that can be trapped in the matrix. 
	 * The water can flow into a neighboring bar if the neighboring bar's height is smaller than the water's height. 
	 * Each bar has 4 neighboring bars to the left, right, up and down side.
	 * 
	 * Assumptions: The given matrix is not null and has size of M * N, 
	 * where M > 0 and N > 0, all the values are non-negative integers in the matrix.
	 * 
	 * Examples:
	 * { { 2, 3, 4, 2 },
	 *   { 3, 1, 2, 3 },
	 *   { 4, 3, 5, 4 } }
	 *   the amount of water can be trapped is 3, 
	 *   at position (1, 1) there is 2 units of water trapped,
	 *   at position (1, 2) there is 1 unit of water trapped.
	 */
	public int maxTrapped(int[][] matrix) {
		int result = 0;		
		int M = matrix.length, N = matrix[0].length;
		boolean[][] visited = new boolean[M][N];
		PriorityQueue<Node> pq = new PriorityQueue<>();
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				if (i == 0 || j == 0 || i == M - 1 || j == N - 1) {
					visited[i][j] = true;
					pq.offer(new Node(i, j, matrix[i][j]));
				} else {
					visited[i][j] = false;
				}
			}
		}
		
		while (!pq.isEmpty()) {
			Node node = pq.poll();
			result += (node.level - matrix[node.x][node.y]);
			if (node.x - 1 >= 0 && !visited[node.x - 1][node.y]) {
				pq.offer(new Node(node.x - 1, node.y, Math.max(node.level, matrix[node.x - 1][node.y])));
				visited[node.x - 1][node.y] = true;
			}
			if (node.x + 1 < M && !visited[node.x + 1][node.y]) {
				pq.offer(new Node(node.x + 1, node.y, Math.max(node.level, matrix[node.x + 1][node.y])));
				visited[node.x + 1][node.y] = true;
			}
			if (node.y - 1 >= 0 && !visited[node.x][node.y - 1]) {
				pq.offer(new Node(node.x, node.y - 1, Math.max(node.level, matrix[node.x][node.y - 1])));
				visited[node.x][node.y - 1] = true;
			}
			if (node.y + 1 < N && !visited[node.x][node.y + 1]) {
				pq.offer(new Node(node.x, node.y + 1, Math.max(node.level, matrix[node.x][node.y + 1])));
				visited[node.x][node.y + 1] = true;
			}
		}
		
		return result;
	}
	
	class Node implements Comparable<Node> {
		int x;
		int y;
		int level;
		public Node(int x, int y, int level) {
			this.x = x;
			this.y = y;
			this.level = level;
		}
		public int compareTo(Node that) {
			return Integer.compare(this.level, that.level);
		}
	}
}
