package tree;
import java.util.*;
public class BinarySearchTree {

	public static void main(String[] args) {
	    
        //     10
        //    /  \
        //   5   15
        //  / \    \
        // 2   7    20
        TreeNode node1 = new TreeNode(10);
        TreeNode node2 = new TreeNode(5);
        TreeNode node3 = new TreeNode(15);
        TreeNode node4 = new TreeNode(2);
        TreeNode node5 = new TreeNode(7);
        TreeNode node6 = new TreeNode(20);
        
        node1.left = node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.right = node6;
		
//		System.out.println(isBST(node1));
//		System.out.println(getRange(node1, 6, 21));
		TreeTraverse.inOrder(node1);
		System.out.println();
		TreeTraverse.inOrder(delete(node1, 15));
	}
	
	/*----------------------------------------------------------*/
	public static boolean isBST(TreeNode root) {
		return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	private static boolean isBST(TreeNode root, int min, int max) {
		if (root == null) {
			return true;
		}
		
		if (root.value <= min || root.value >= max) {
			return false;
		}
		
		return isBST(root.left, min, root.value) && isBST(root.right, root.value, max);
	}
	
	/*----------------------------------------------------------*/
	private int lastSeen = Integer.MIN_VALUE;
	public boolean isBST2(TreeNode root) {
		lastSeen = Integer.MIN_VALUE;
		return inOrderJustify(root);
	}
	
	private boolean inOrderJustify(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		boolean isLeftBST = inOrderJustify(root.left);
		if (!isLeftBST) {
			return false;
		}
		if (root.value < lastSeen) {
			return false;
		}
		lastSeen = root.value;
		
		return inOrderJustify(root.right);
	}
	
	/*----------------------------------------------------------*/
	public static List<Integer> getRange(TreeNode root, int min, int max) {
		List<Integer> list = new ArrayList<Integer>();
		helpGetRange(root, min, max, list);
		return list;
	}
	
	private static void helpGetRange(TreeNode root, int min, int max, List<Integer> list) {
		if (root == null) {
			return;
		}
		
		if (root.value > min) {
			helpGetRange(root.left, min, max, list);
		}
		if (root.value >= min && root.value <= max) {
			list.add(root.value);
		}
		if (root.value < max) {
			helpGetRange(root.right, min, max, list);
		}
	}
	
    /*----------------------------------------------------------*/
	public static TreeNode search(TreeNode root, int target) {
	    if (root == null) {
	        return null;
	    }
	    
	    if (root.value == target) {
	        return root;
	    } else if (root.value < target) {
	        return search(root.right, target);
	    } else {
	        return search(root.left, target);
	    }
	}
	
    /*----------------------------------------------------------*/
	public static TreeNode search2(TreeNode root, int target) {
	    TreeNode curr = root;
	    while (curr != null) {
	        if (curr.value == target) {
	            return curr;
	        } else if (curr.value < target) {
	            curr = curr.right;
	        } else {
	            curr = curr.left;
	        }
	    }
	    return null;
	}
	
    /*----------------------------------------------------------*/
	public static TreeNode insert(TreeNode root, int target) {
	    if (root == null) {
	        return new TreeNode(target);
	    }
	    
	    if (root.value < target) {
	        root.right = insert(root.right, target);
	    } else if (root.value > target) {
	        root.left = insert(root.left, target);
	    }
	    return root;
	}
	
    /*----------------------------------------------------------*/
	public static TreeNode insert2(TreeNode root, int target) {
	    if (root == null) {
	        return new TreeNode(target);
	    }
	    
	    TreeNode targetNode = new TreeNode(target);
	    TreeNode curr = root;
	    while (curr.value != target) {
	        if (target < curr.value) {
	            if (curr.left == null) {
	                curr.left = targetNode;
	                break;
	            }
	            curr = curr.left;
	        } else if (target > curr.value) {
	            if (curr.right == null) {
	                curr.right = targetNode;
	                break;
	            }
	            curr = curr.right;
	        }
	    }
	    return root;
	}
	
    /*----------------------------------------------------------*/
	public static TreeNode delete(TreeNode root, int target) {
	    // corner case
	    if (root == null) {
	        return null;
	    }
	    
	    if (root.value < target) {
	        root.right = delete(root.right, target);
	        return root;
	    } else if (root.value > target) {
	        root.left = delete(root.left, target);
	        return root;
	    }
	    
	    // base case
	    
	    // corner case
	    if (root.left == null) {
	        return root.right;
	    }
	    if (root.right == null) {
	        return root.left;
	    }
	    
	    // corner case
	    if (root.right.left == null) {
	        root.right.left = root.left;
	        return root.right;
	    }
	    TreeNode smallestInRight = deleteSmallest(root.right); 
	    smallestInRight.left = root.left;
	    smallestInRight.right = root.right;
	    
	    return smallestInRight;
	}
	
	private static TreeNode deleteSmallest(TreeNode root) {
	    TreeNode prev = root;
	    TreeNode curr = root.left;
	    
	    while (curr.left != null) {
	        prev = curr;
	        curr = curr.left;
	    }
	    prev.left = curr.right;
	    return curr;
	}
	
    /*----------------------------------------------------------*/
	
}
