package laicode.class_08;

public class ReverseWords {

    public static void main(String[] args) {
        String a = "an apple";
        ReverseWords test = new ReverseWords();
        System.out.println(test.reverseWords(a));
    }

    public String reverseWords(String input) {
        if (input == null) {
            return null;
        }

        char[] inputChars = input.toCharArray();
        reverseCharacters(inputChars, 0, inputChars.length - 1);
        int left = -1;
        for (int i = 0; i <= inputChars.length; i++) {
            if (i == inputChars.length) {
                reverseCharacters(inputChars, left + 1, i - 1);
            } else if (inputChars[i] == ' ') {
                reverseCharacters(inputChars, left + 1, i - 1);
                left = i;
            }
        }
        return new String(inputChars);
    }

    private void reverseCharacters(char[] input, int left, int right) {
        if (right <= left) {
            return;
        }
        while (right > left && left >= 0) {
            char tmp = input[left];
            input[left] = input[right];
            input[right] = tmp;
            left++;
            right--;
        }
    }
}
