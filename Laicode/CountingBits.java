package laicode;

public class CountingBits {

    public static void main(String[] args) {
        CountingBits test = new CountingBits();
        for (Integer i : test.countBits(5)) {
            System.out.println(i);
        }
    }

    /*
     * Given a non negative integer number num.
     * For every numbers i in the range 0 ≤ i ≤ num
     * calculate the number of 1's in their binary representation and return them as an array.

     * Example:
     * For num = 5 you should return [0,1,1,2,1,2].
     *
     * Time & Space : O(n)
     */
    public int[] countBits(int num) {
        if (num < 0) {
            return new int[0];
        }
        int[] result = new int[num + 1];
        if (num == 0) {
            return result;
        } else if (num == 1) {
            result[1] = 1;
            return result;
        }

        result[1] = 1;
        int N = 2;
        while (num > pow2(N) - 1) {
            for (int i = pow2(N - 1); i < pow2(N); i++) {
                result[i] = result[i - pow2(N - 1)] + 1;
            }
            N++;
        }
        for (int i = pow2(N - 1); i < result.length; i++) {
            result[i] = result[i - pow2(N - 1)] + 1;
        }
        return result;
    }

    private int pow2(int k) {
        return (int) Math.pow(2, k);
    }
}
