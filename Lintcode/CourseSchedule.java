public class CourseSchedule {
     /*
     * @param numCourses: a total of n courses
     * @param prerequisites: a list of prerequisite pairs
     * @return: true if can finish all courses or false
     */
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        Map<Integer, Integer> inDegree = new HashMap<>();
        Map<Integer, List<Integer>> neiMap = new HashMap<>();
        for (int i = 0; i < prerequisites.length; i++) {
            inDegree.put(prerequisites[i][0], inDegree.getOrDefault(prerequisites[i][0], 0));
            inDegree.put(prerequisites[i][1], inDegree.getOrDefault(prerequisites[i][1], 0) + 1);

            List<Integer> neis = neiMap.get(prerequisites[i][0]);
            neis = (neis == null ? new ArrayList<Integer>() : neis);
            neis.add(prerequisites[i][1]);
            neiMap.put(prerequisites[i][0], neis);
       }

       Queue<Integer> queue = new LinkedList<>();
       for (Map.Entry<Integer, Integer> entry : inDegree.entrySet()) {
           if (entry.getValue() == 0) {
               queue.offer(entry.getKey());
           }
       }
       int count = 0;
       while (!queue.isEmpty()) {
            Integer node = queue.poll();
            count++;
            List<Integer> neis = neiMap.get(node);
            if (neis != null) {
                for (Integer nei : neis) {
                    Integer in = inDegree.get(nei);
                    inDegree.put(nei, in - 1);
                    if (in - 1 == 0) {
                        queue.offer(nei);
                    }
                } 
            }
        }
        
        return count != inDegree.size();
    }
}