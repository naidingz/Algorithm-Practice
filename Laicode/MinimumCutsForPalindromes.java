package laicode;

public class MinimumCutsForPalindromes {

	public static void main(String[] args) {
		MinimumCutsForPalindromes test = new MinimumCutsForPalindromes();
		System.out.println(test.minCuts("aabba"));
	}
	
	public int minCuts(String input) {
		if (input.length() == 0) {
			return 0;
		}
		int[] M = new int[input.length()];
		M[0] = 0;
		for (int i = 1; i < input.length(); i++) {
			int min = Integer.MAX_VALUE;
			for (int j = i; j >= 0; j--) {
				if (isPalindromes(input, j, i)) {
					min = Math.min(min, j > 0 ? M[j - 1] + 1 : 0);
				} else {
					min = Math.min(min, j > 0 ? M[j - 1] + i - j + 1 : i - j);
				}
			}
			M[i] = min;
		}
		return M[input.length() - 1];
	}

	private boolean isPalindromes(String input, int left, int right) {
		while (left < right) {
			if (input.charAt(left) != input.charAt(right)) {
				return false;
			} else {
				left++;
				right--;
			}
		}
		return true;
	}
}
