package dp;


public class CuttingRope {

    public static void main(String[] args) {
        CuttingRope test = new CuttingRope();
        for (int i = 1; i < 15; i++) {
            System.out.println(test.getMaxProduct3(i));
        }
    }

    /*
     * Solution 1 : recursion
     */
    public int getMaxProduct(int n) {
        if (n <= 1) {
            return 0;
        }
        int maxProduct = 0;
        for (int i = 1; i <= n - 1; i++) {
            int best = Math.max(getMaxProduct(n - i), n - i);
            maxProduct = Math.max(maxProduct, i * best);
        }
        return maxProduct;
    }

    /*
     * Solution 2 : dp
     */
    public int getMaxProduct2(int n) {
        int[] M = new int[n + 1];
        M[0] = 0;
        M[1] = 0;
        for (int i = 1; i <= n; i++) {
            int currMax = 0;
            for (int j = 1; j <= i/2; j++) {
                currMax = Math.max(currMax, Math.max(j, M[j]) * Math.max(i - j, M[i - j]));
            }
            M[i] = currMax;
        }
        return M[n];
    }

    /*
     * Solution 3 : dp
     */
    public int getMaxProduct3(int n) {
        int[] M = new int[n + 1];
        M[0] = 0;
        M[1] = 0;
        for (int i = 1; i <= n; i++) {
            int currMax = 0;
            for (int j = 1; j <= i - 1; j++) {
                currMax = Math.max(currMax, j * Math.max(i - j, M[i - j]));
            }
            M[i] = currMax;
        }
        return M[n];
    }

}
