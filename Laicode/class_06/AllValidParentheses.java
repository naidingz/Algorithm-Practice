package laicode.class_06;

import java.util.ArrayList;
import java.util.List;

public class AllValidParentheses {

    public static void main(String[] args) {
        AllValidParentheses test = new AllValidParentheses();
        for (String i : test.validParentheses(3)) {
            System.out.println(i);
        }
    }

    public List<String> validParentheses(int n) {
        List<String> result = new ArrayList<>();
        if (n == 0) {
            return result;
        }
        validParentheses(n, 0, 0, 0, new StringBuilder(), result);
        return result;
    }

//    private void validParentheses(int n, int index, int left, int right, StringBuilder sb, List<String> result) {
//        if (left == n && right == n) {
//            result.add(sb.toString());
//            return;
//        }
//
//        if (left < n) {
//            sb.append("(");
//            validParentheses(n, index + 1, left + 1, right, sb, result);
//            sb.deleteCharAt(sb.length() - 1);
//        }
//
//        if (right < left) {
//            sb.append(")");
//            validParentheses(n, index + 1, left, right + 1, sb, result);
//            sb.deleteCharAt(sb.length() - 1);
//        }
//    }

    private void validParentheses(int n, int index, int left, int right, StringBuilder sb, List<String> result) {
        if (index == 2 * n) {
            if (left == n && right == n) {
                result.add(sb.toString());
            }
            return;
        }
        if (left < n) {
            sb.append("(");
            validParentheses(n, index + 1, left + 1, right, sb, result);
            sb.deleteCharAt(sb.length() - 1);
        }
        if (right < left) {
            sb.append(")");
            validParentheses(n, index + 1, left, right + 1, sb, result);
            sb.deleteCharAt(sb.length() - 1);
        }
    }
}
