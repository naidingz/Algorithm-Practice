package laicode;

public class LargestCrossOf1s {

	public static void main(String[] args) {
		LargestCrossOf1s test = new LargestCrossOf1s();
		int[][] matrix = new int[][] {{1, 1, 1, 1},
									  {1, 1, 1, 0},
									  {1, 1, 1, 1},
								      {1, 1, 0, 1}};
		System.out.println(test.largest(matrix));
	}
	
	/*
	 * Given a matrix that contains only 1s and 0s, 
	 * find the largest cross which contains only 1s, 
	 * with the same arm lengths and the four arms joining at the central point.
	 * Return the arm length of the largest cross.
	 * 
	 * Assumption: The given matrix is not null, has size of N * M, N >= 0 and M >= 0.
	 */
	public int largest(int[][] matrix) {
		if (matrix.length == 0 || matrix[0].length == 0) {
			return 0;
		}
		
		int M = matrix.length;
		int N = matrix[0].length;
		
		int[][] l2r = getAux(matrix, M, N, "l2r");
		int[][] r2l = getAux(matrix, M, N, "r2l");
		int[][] t2d = getAux(matrix, M, N, "t2d");
		int[][] d2t = getAux(matrix, M, N, "d2t");
		
		int globalMax = 0;
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				int len1 = Math.min(l2r[i][j], r2l[i][j]);
				int len2 = Math.min(t2d[i][j], d2t[i][j]);
				int len = Math.min(len1, len2);
				globalMax = Math.max(globalMax, len);
			}
		}
		return globalMax;
	}

	private int[][] getAux(int[][] matrix, int M, int N, String type) {
		int[][] aux = new int[M][N];
		switch (type) {
		case "l2r":
			for (int i = 0; i < M; i++) {
				aux[i][0] = matrix[i][0];
			}
			for (int i = 0; i < M; i++) {
				for (int j = 1; j < N; j++) {
					aux[i][j] = matrix[i][j] == 0 ? 0 : aux[i][j - 1] + 1;
				}
			}
			break;
			
		case "r2l":
			for (int i = 0; i < M; i++) {
				aux[i][N - 1] = matrix[i][N - 1];
			}
			for (int i = 0; i < M; i++) {
				for (int j = N - 2; j >= 0; j--) {
					aux[i][j] = matrix[i][j] == 0 ? 0 : aux[i][j + 1] + 1;
				}
			}
			break;
			
		case "t2d":
			for (int j = 0; j < N; j++) {
				aux[0][j] = matrix[0][j];
			}
			for (int j = 0; j < N; j++) {
				for (int i = 1; i < M; i++) {
					aux[i][j] = matrix[i][j] == 0 ? 0 : aux[i - 1][j] + 1; 
				}
			}
			break;

		case "d2t":
			for (int j = 0; j < N; j++) {
				aux[M - 1][j] = matrix[M - 1][j];
			}
			for (int j = 0; j < N; j++) {
				for (int i = M - 2; i >= 0; i--) {
					aux[i][j] = matrix[i][j] == 0 ? 0 : aux[i + 1][j] + 1;
				}
			}
			break;
			
		default:
			throw new IllegalArgumentException("Illegal argument for type");
		}
		return aux;
	}
	
}
