import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AmazonOAP2 {

	public static void main(String[] args) {
		int[][] matrix = new int[][] {{5, 1}, {3, 5}};;
//		int[][] matrix = new int[][] {{5, 1, 6}, {4, 5, 8}, {2, 3, 9}};
		AmazonOAP2 test = new AmazonOAP2();
		System.out.println(test.maxOfMinAltitudes(matrix.length, matrix[0].length, matrix));
	}
	
	public int maxOfMinAltitudes(int columnCount, int rowCount, int[][] mat) {
		int[] globalMax = new int[] {Integer.MIN_VALUE};
		helper(0, 0, Integer.MAX_VALUE, mat, globalMax);
		return globalMax[0];
	}
	
	private void helper(int m, int n, int localMin, int[][] mat, int[] globalMax) {
		if (m == mat.length - 1 && n == mat[0].length - 1) {
			localMin = Math.min(localMin, mat[m][n]);
			globalMax[0] = Math.max(globalMax[0], localMin);
			return ;
		}
		
		localMin = Math.min(localMin, mat[m][n]);
		if (m + 1 < mat.length) {
			helper(m + 1, n, localMin, mat, globalMax);
		}
		if (n + 1 < mat[0].length) {
			helper(m, n + 1, localMin, mat, globalMax);
		}
	}
}

