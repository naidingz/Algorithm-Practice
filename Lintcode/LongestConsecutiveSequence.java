import java.util.Set;

public class LongestConsecutiveSequence {
    
    /**
     * Given an unsorted array of integers, find the length of the longest consecutive elements sequence.
     * Example
     * Given [100, 4, 200, 1, 3, 2],
     * The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.
     * Clarification: Your algorithm should run in O(n) complexity.
     */
    public int longestConsecutive(int[] num) {
        Set<Integer> set = new HashSet<>();
        for (Integer i : num) {
            set.add(i);
        }
        int result = 0;
        while (!set.isEmpty()) {
            int start = Integer.MIN_VALUE;
            for (Integer i : set) {
                start = i;
                break;
            }
            result = Math.max(result, countAndRemove(start, set));
        }
        return result;
    }

    private int countAndRemove(int start, Set<Integer> set) {
        set.remove(start);
        int i = 1;
        while (set.contains(start - i)) {
            set.remove(start - i);
            i++;
        }
        int j = 1;
        while (set.contains(start + j)) {
            set.remove(start + j);
            j++;
        }
        return i - 1 + j - 1 + 1;
    }
    
}