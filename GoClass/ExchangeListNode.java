import ds.*;

public class ExchangeListNode {

    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);

        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;

        node1.printList();

        ExchangeListNode test = new ExchangeListNode();
        test.exchange(node1).printList();;
    }

    public ListNode exchange(ListNode head) {

        ListNode dummyHead = new ListNode(-1);
        dummyHead.next = head;
        ListNode prev = dummyHead;
        ListNode curr = dummyHead.next;

        while (curr != null && curr.next != null) {
            ListNode temp = curr.next.next;
            prev.next = curr.next;
            prev.next.next = curr;
            curr.next = temp;
            prev = prev.next.next;
            curr = temp;
        }

        return dummyHead.next;
    }

    
}