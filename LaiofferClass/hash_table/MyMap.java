package hash_table;

public class MyMap<Key, Value> {

	public static class Node<Key, Value> {
		private final Key key;
		private Value value;
		private Node<Key, Value> next;
		
		public Node(Key key, Value value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}
	
		public Key getKey() {
			return key;
		}
		
		public Value getValue() {
			return value;
		}
		
		public void setValue(Value value) {
			this.value = value;
		}
	}
	
	private Node<Key, Value>[] map;
	private int size;
	private static final int INITIAL_CAPACITY = 11;
	private static final double LOAD_FACTOR = 0.8;
	
	@SuppressWarnings("unchecked")
	public MyMap() {
		map = (Node<Key, Value>[]) new Node[INITIAL_CAPACITY];
		size = 0;
	}
	
	public Value put(Key key, Value value) {
		int index = getIndex(key);
		Node<Key, Value> newNode = new Node<>(key, value);
		newNode.next = map[index];
		map[index] = newNode;
		size++;
		if (needRehash()) {
			rehash();
		}
		return value;
	}
	
	public Value get(Key key) {
		int index = getIndex(key);
		Node<Key, Value> oldNode = map[index];
		while (oldNode != null && !oldNode.getKey().equals(key)) {
			oldNode = oldNode.next;
		}
		return oldNode == null ? null : oldNode.getValue();
	}
	
	public int size() {
		return this.size;
	}
	
	public boolean isEmpty() {
		return size() == 0;
	}
	
	public boolean containsKey(Key key) {
		return get(key) != null;
	}
	
	public Value remove(Key key) {
		int index = getIndex(key);
		Node<Key, Value> dummyHead = new Node<>(null, null);
		Node<Key, Value> prev = dummyHead, curr = map[index];		
		while (curr != null) {
			if (curr.getKey().equals(key)) {
				prev.next = curr.next;
				break;
			} else {
				curr = curr.next;
				prev = prev.next;
			}
		}
		map[index] = dummyHead.next;
		if (curr == null) {
			return null;
		} else {
			size--;
			return curr.getValue();
		}
	}
	
	/*
	 * Some useful private methods
	 */
	
	private int getIndex(Key key) {
		if (key == null) {
			return 0;
		}
		
		int hash = key.hashCode() & 0x7FFFFFFF;
		return hash % map.length;
	}
	
	private boolean needRehash() {
		return LOAD_FACTOR * map.length < size;
	}
	
	@SuppressWarnings("unchecked")
	private void rehash() {
		Node<Key, Value>[] oldMap = map;
		map = (Node<Key, Value>[]) new Node[oldMap.length * 2];
		for (int i = 0; i < oldMap.length; i++) {
			Node<Key, Value> node = oldMap[i];
			if (node != null) {
				int index = getIndex(node.getKey());
				Node<Key, Value> head = map[index];
				if (head == null) {
					map[index] = node;
				} else {
					while (head.next != null) {
						head = head.next;
					}
					head.next = node;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		MyMap<String, String> map = new MyMap<>();
        System.out.println(map.put("a", "Apple"));
        System.out.println(map.put("b", "Bob"));
        System.out.println(map.put("c", "Candy"));
        System.out.println(map.put("d", "Dad"));
        System.out.println(map.put("e", "Echo"));
        System.out.println(map.put("f", "Fruit"));
        System.out.println(map.put("g", "Google"));
        System.out.println(map.remove("b"));
        System.out.println(map.size());
        
        System.out.println(map.get("a"));
        System.out.println(map.get("b"));
        System.out.println(map.get("c"));
        System.out.println(map.get("d"));
        System.out.println(map.get("e"));
        System.out.println(map.get("f"));
        System.out.println(map.get("g"));
	}

}
