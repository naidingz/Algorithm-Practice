package heap;
import java.util.*;

public class Operations {

	public static void main(String[] args) {
		int[] array = {7, 4, 1, 9, 8, 5, 2, 3};
//		int[] result = kSmallest(array, 3);
		int[] result = kSmallest2(array, 3);
		for (int i : result) {
			System.out.print(i + " ");
		}
		
	}
	
	/*-----------------------------------------------------------*/
	public static int[] kSmallest(int[] array, int k) {
		if (k == 0) {
			return new int[0];
	    }
			
		PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());
		
		// Time : O(k)
		for (int i = 0; i < k; i++) {
			pq.offer(array[i]);
		}
		
		// Time : O(n-k)
		for (int i = k; i < array.length; i++) {
			if (pq.peek() > array[i]) {
				pq.poll();
				pq.offer(array[i]);
			}
		}
			
	    int[] result = new int[k];
	    for (int i = 0; i < k; i++) {
	    	result[k - 1 - i] = pq.poll();
	    }
			
		return result;
	}
	
	/*-----------------------------------------------------------*/
	public static int[] kSmallest2(int[] array, int k) {
		if (k == 0) {
			return new int[0];
	    }
			
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		for (int i : array) {
			pq.offer(i);
		}
		
		int[] result = new int[k];
		for (int i = 0; i < k; i++) {
			result[i] = pq.poll();
		}
		return result;
	}
	
	/*-----------------------------------------------------------*/
	public static List<Integer> quickSelect(int[] array, int k) {
		helpQuickSort(array, k - 1, 0, array.length - 1);
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < k; i++) {
			list.add(array[i]);
		}
		return list;
	}
	
	private static void helpQuickSort(int[] array, int k, int left, int right) {
		if (left >= right) {
			return;
		}
		
		Random random = new Random();
		int pivotIndex = left + random.nextInt(right - left + 1);
		swap(array, pivotIndex, right);
		int l = left, r = right - 1;
		
		while (l <= r) {
			if (array[l] < array[right]) {
				l++;
			} else {
				swap(array, l, r);
				r--;
			}
			swap(array, l, right);
			if (k == l) {
				return;
			} else if (k < l) {
				helpQuickSort(array, k, left, l - 1);
			} else {
				helpQuickSort(array, k, l + 1, right);
			}
 		}
	}
	
	private static void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

}
