import java.util.Queue;
import java.util.LinkedList;
public class Main {
    public static void main(String[] args) {
        String prob = "0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0 0 0 1 0 0 0 1 0 1 0 0 0 1 0 1 0 1 0 1 0 0 0 1 0 1 0 1 0 1 0 0 0 1 0 0 0 1 0 1 1 0 0 1 0 1 1 1 0 1 1 1 0 1 1 1 0 0 1 0 0 0 0 0 0 0 1 1 1 1 1 0 0 0 0 0 1 0 0 1 0 0 0";
        String[] probs = prob.split(" ");
        int[][] matrix = new int[10][10];
        for (int i = 0; i < 100; i++) {
            matrix[i / 10][i % 10] = probs[i].charAt(0) - '0';
        }
    	
        Main test = new Main();
        test.solution(matrix);
    }
    
    public void solution(int[][] matrix) {
        int row = matrix.length, col = matrix[0].length;
        int result = Integer.MIN_VALUE;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
            		if (matrix[i][j] == 1) {
                    result = Math.max(result, helper(matrix, i, j));
            		}
            }
        }
        System.out.print(result);
    }
    
    private int helper(int[][] matrix, int m, int n) {
        int row = matrix.length, col = matrix[0].length;
        
        Queue<Integer> q = new LinkedList<>();
        q.offer(m * col + n);
        
        boolean[][] visited = new boolean[row][col];
        visited[m][n] = true;
        
        int result = 0;
        while (!q.isEmpty()) {
            Integer element = q.poll();
            result++;
            
            int i = element / col, j = element % col;
            if (i - 1 >= 0 && !visited[i - 1][j] && matrix[i - 1][j] == 1) {
                q.offer((i - 1) * col + j);
                visited[i - 1][j] = true;
            }
            if (i + 1 < row && !visited[i + 1][j] && matrix[i + 1][j] == 1) {
                q.offer((i + 1) * col + j);
                visited[i + 1][j] = true;
            }
            if (j - 1 >= 0 && !visited[i][j - 1] && matrix[i][j - 1] == 1) {
                q.offer(i * col + (j - 1));
                visited[i][j - 1] = true;
            }
            if (j + 1 < col && !visited[i][j + 1] && matrix[i][j + 1] == 1) {
                q.offer(i * col + (j + 1));
                visited[i][j + 1] = true;
            }
        }
        return result;
    }
}