package laicode.class_07;

public class MissingNumber {

    public static void main(String[] args) {
        int[] array = {};
        MissingNumber test = new MissingNumber();
        System.out.println(test.missing3(array));
    }

    public int missing(int[] array) {
        if (array == null || array.length == 0) {
            return 1;
        }

        int N = array.length + 1;
        int result = 0;
        for (int i = 0; i < N - 1; i++) {
            result += (i + 1 - array[i]);
        }
        return result + N;
    }

    public int missing2(int[] array) {
        if (array == null || array.length == 0) {
            return 1;
        }

        int N = array.length + 1;
        int xor1 = array[0], xor2 = 1;
        for (int i = 1; i < N - 1; i++) {
            xor1 ^= array[i];
            xor2 ^= (i + 1);
        }
        return xor1 ^ xor2 ^ N;
    }

    /*
     * Given an integer array of size N - 1 sorted by ascending order,
     * containing all the numbers from 1 to N except one, find the missing number.
     */
    public int missing3(int[] array) {
        if (array == null) {
            return 0;
        }
        if (array.length == 0) {
            return 1;
        }
        int left = 0, right = array.length - 1;
        while (right - left > 1) {
            int mid = left + (right - left) / 2;
            if (array[mid] == mid + 1) {
                left = mid;
            } else if (array[mid] > mid + 1) {
                right = mid;
            }
        }
        if (array[right] - array[left] > 1) {
            return array[left] + 1;
        } else {
            if (left == 0) {
                return array[left] - 1;
            } else {
                return array[right] + 1;
            }
        }
    }


}

