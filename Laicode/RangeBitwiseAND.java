package laicode;

public class RangeBitwiseAND {

    /*
     * Given a range [m, n] where 0 <= m <= n <= 2147483647,
     * return the bitwise AND of all numbers in this range, inclusive.
     * For example, given the range [5, 7], you should return 4.
     */
    public int rangeBitwiseAnd(int m, int n) {
        int result = m;
        for (int i = m + 1; i <= n; i++) {
            result &= i;
        }
        return result;
    }
    // TODO : is there any clever method
}
