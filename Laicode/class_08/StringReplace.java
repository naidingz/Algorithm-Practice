package laicode.class_08;

import java.util.ArrayList;
import java.util.List;

public class StringReplace {

    public static void main(String[] args) {
        String s = "docomomomocomo";
        StringReplace test = new StringReplace();
        System.out.println(test.replace(s, "omo", "baag"));
    }

    /*
     * in-place method
     */
    public String replace(String input, String s, String t) {
        if (input == null || s == null || t == null || s.length() == 0) {
            return input;
        }

        char[] inputChars = input.toCharArray();
        if (t.length() <= s.length()) {
            return replaceShorter(inputChars, s, t);
        } else {
            return replaceLonger(inputChars, s, t);
        }
    }

    private String replaceShorter(char[] input, String s, String t) {
        int slow = 0, fast = 0;
        while (fast < input.length) {
            if (fast + s.length() - 1 >= input.length || input[fast] != s.charAt(0)) {
                input[slow++] = input[fast++];
            } else {
                boolean isMatch = true;
                for (int i = 1; i < s.length(); i++) {
                    if (input[fast+i] != s.charAt(i)) {
                        isMatch = false;
                    }
                }
                if (isMatch) {
                    for (int i = 0; i < t.length(); i++) {
                        input[slow++] = t.charAt(i);
                    }
                    fast = fast + s.length();
                } else {
                    input[slow++] = input[fast++];
                }
            }
        }
        return new String(input, 0, slow);
    }

    private String replaceLonger(char[] input, String s, String t) {

        List<Integer> index = countMatches(input, s);
        if (index.size() == 0) {
            return new String(input);
        }

        char[] result = new char[input.length + index.size() * (t.length() - s.length())];
        int resultPtr = result.length - 1, indexPtr = index.size() - 1, inputPtr = input.length - 1;
        while (inputPtr >= 0) {
            if (indexPtr >= 0 && inputPtr == index.get(indexPtr)) {
                for (int i = t.length() - 1; i >= 0; i--) {
                    result[resultPtr--] = t.charAt(i);
                }
                inputPtr -= s.length();
                indexPtr--;
            } else {
                result[resultPtr--] = input[inputPtr--];
            }

        }
        return new String(result);
    }

    private List<Integer> countMatches(char[] input, String s) {
        int ptr = 0;
        List<Integer> index = new ArrayList<>();
        while (ptr <= input.length - s.length()) {
            if (input[ptr] != s.charAt(0)) {
                ptr++;
            } else {
                boolean isMatch = true;
                for (int i = 1; i < s.length(); i++) {
                    if (input[ptr+i] != s.charAt(i)) {
                        isMatch = false;
                    }
                }
                if (isMatch) {
                    ptr = ptr + s.length();
                    index.add(ptr - 1);
                } else {
                    ptr++;
                }
            }
        }
        return index;
    }
}
