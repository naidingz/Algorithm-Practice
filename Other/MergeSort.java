public class MergeSort {
    public static void main(String[] args) {
        MergeSort test = new MergeSort();
        int[] array = {1,6,3,5,2,7};
        test.sort(array);
        for (Integer i : array) {
            System.out.print(" " + i);
        }
    }

    public void sort(int[] array) {
        sort(array, new int[array.length], 0, array.length - 1);
    }

    private void sort(int[] array, int[] aux, int left, int right) {
        if (left >= right) {
            return;
        }
        int mid = left + (right - left) / 2;
        sort(array, aux, left, mid);
        sort(array, aux, mid + 1, right);
        merge(array, aux, left, mid, right);
    }

    private void merge(int[] array, int[] aux, int left, int mid, int right) {
        for (int i = left; i <= right; i++) {
            aux[i] = array[i];
        }

        int i = left, j = mid + 1;
        for (int k = left; k <= right; k++) {
            if (i > mid) {
                array[k] = aux[j++];
            } else if (j > right) {
                array[k] = aux[i++];
            } else if (aux[i] <= aux[j]) {
                array[k] = aux[i++];
            } else {
                array[k] = aux[j++];
            }
        }
    }
}