package laicode;

/*
 * https://stackoverflow.com/questions/19372930/given-a-bitonic-array-and-element-x-in-the-array-find-the-index-of-x-in-2logn
 * achieved O(2lgn)
 */

public class SearchBitonicFast {

    public static void main(String[] args) {
        
    }
    
    public static int search(int[] array, int target) {
        if (array == null || array.length == 0) {
            return -1;
        }
        
        return helpSearch(array, target, 0, array.length);
    }
    
    private static int helpSearch(int[] array, int target, int left, int right) {
        int mid = left + (right - left) / 2;
        
        // TODO : Still can't handle the situation when 1, 2, 3, 4, 4, 4, 6, 5 happens
        // don't know in which side the max are.
        return -1;
    }

}
