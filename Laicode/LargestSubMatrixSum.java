package laicode;

public class LargestSubMatrixSum {

	public static void main(String[] args) {
		LargestSubMatrixSum test = new LargestSubMatrixSum();
		int[][] matrix = new int[][] {{-1, -2, -3},
									  {-4, -3, -2},
									  {-3,  0, -1}};
		System.out.println(test.largest(matrix));
		
	}
	
	/*
	 * Given a matrix that contains integers, find the submatrix with the largest sum.
	 * Return the sum of the submatrix.
	 * 
	 * Assumptions: The given matrix is not null and has size of M * N, where M >= 1 and N >= 1
	 * 
	 * Example:
	 * { {1, -2, -1, 4},
	 *   {1, -1,  1, 1},
	 *   {0, -1, -1, 1},
	 *   {0,  0,  1, 1} }
	 *   the largest submatrix sum is (-1) + 4 + 1 + 1 + (-1) + 1 + 1 + 1 = 7.
	 */
	public int largest(int[][] matrix) {
		int M = matrix.length, N = matrix[0].length;
		int[][] cumsum = new int[M][N];
		
		for (int j = 0; j < N; j++) {
			for (int i = 0; i < M; i++) {
				cumsum[i][j] = i == 0 ? matrix[i][j] : cumsum[i - 1][j] + matrix[i][j];
			}
		}
		
		int globalMax = Integer.MIN_VALUE;
		for (int top = 0; top < M; top++) {
			for (int down = top; down < M; down++) {
				int[] colSum = new int[N];
				for (int i = 0; i < N; i++) {
					colSum[i] = top == 0 ? cumsum[down][i] :
										   cumsum[down][i] - cumsum[top - 1][i];
				}
				int lastMax = colSum[0];
				globalMax = Math.max(globalMax, lastMax);
				for (int i = 1; i < N; i++) {
					lastMax = Math.max(0, lastMax) + colSum[i];
					globalMax = Math.max(globalMax, lastMax);
				}
			}
		}
		return globalMax;
	}
}
