import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MergeIntervals {

	public static void main(String[] args) {
		
		
	}
	
	public class Interval {
		int start, end;
		Interval(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}

	public List<Interval> merge(List<Interval> intervals) {
		List<Interval> result = new ArrayList<>();
		if (intervals == null || intervals.size() == 0) {
			return result;
		}
		Collections.sort(intervals, new Comparator<Interval>() {
			@Override
			public int compare(Interval i1, Interval i2) {
				return Integer.compare(i1.start, i2.start);
			}
		});
		
		for (Interval i : intervals) {
			System.out.println(i.start + " " + i.end);
		}
		
		Interval candidate = null;
		for (int i = 0; i < intervals.size(); i++) {
			if (candidate == null) {
				candidate = intervals.get(i);
				if (i == intervals.size() - 1) {
					result.add(candidate);
				}
				continue;
			}

			Interval curr = intervals.get(i);
			if (curr.start <= candidate.end) {
				candidate.end = Math.max(candidate.end, curr.end);
				if (i == intervals.size() - 1) {
					result.add(candidate);
				}
			} else {
				result.add(candidate);
				candidate = null;
			}
		}
		return result;
	}

}
