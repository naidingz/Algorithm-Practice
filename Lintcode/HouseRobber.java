public class HouseRobber {

    public class TreeNode {
        public int val;
        public TreeNode left, right;

        public TreeNode(int val) {
            this.val = val;
            this.left = this.right = null;
        }
    }

    /**
     * @param root: The root of binary tree.
     * @return: The maximum amount of money you can rob tonight
     */
    public int houseRobber3(TreeNode root) {
        if (root == null) {
            return 0;
        }
        
        int[] result = houseRobber3Helper(root);
        return Math.max(result[0], result[1]);
    }

    private int[] houseRobber3Helper(TreeNode root) {
        int[] result = new int[] {0, 0};
        if (root == null) {
            return result;
        }

        int[] left = houseRobber3Helper(root.left);
        int[] right = houseRobber3Helper(root.right);

        result[0] = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
        result[1] = left[0] + root.val + right[0];
        return result;
    }

    /**
     * Line
     * 
     * @param A: An array of non-negative integers
     * @return: The maximum amount of money you can rob tonight
     */
    public long houseRobber(int[] A) {
        if (A.length == 0) {
            return 0;
        } else if (A.length == 1) {
            return A[0];
        }
        long prepre = A[0], pre = Math.max(A[0], A[1]);
        for (int i = 2; i < A.length; i++) {
            long temp = Math.max(prepre + A[i], pre);
            prepre = pre;
            pre = temp;
        }
        return pre;
    }

    /**
     * Circle
     * 
     * @param nums
     * @return
     */
    public int houseRobber2(int[] nums) {
        if (nums.length == 1) {
            return nums[0];
        }
        return Math.max(houseRobber2(nums, 0, nums.length - 2), houseRobber2(nums, 1, nums.length - 1));
    }

    private int houseRobber2(int[] A, int left, int right) {
        if (left > right) {
            return 0;
        } else if (left == right) {
            return A[left];
        }
        int prepre = A[left], pre = Math.max(A[left], A[left + 1]);
        for (int i = left + 2; i <= right; i++) {
            int temp = Math.max(prepre + A[i], pre);
            prepre = pre;
            pre = temp;
        }
        return pre;
    }
}