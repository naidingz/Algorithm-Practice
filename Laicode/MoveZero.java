package laicode;

import java.util.Arrays;

public class MoveZero {

    public static void main(String[] args) {
        int[] array = {0, 0, 1, 0, 3, 5, 0, 4, 0};
        System.out.println(Arrays.toString(array));
        moveZero2(array);
        System.out.println(Arrays.toString(array));
    }
    
    /*
     * The relative order will not be maintained
     */
    
    public static int[] moveZero(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }

        moveZero(array, 0, array.length - 1);
        return array;
    }
    
    private static void moveZero(int[] array, int left, int right) {
        while (right >= 0 && array[right] == 0) {
            right--;
        }
        
        if (right <= left) {
            return;
        }
        
        if (array[left] == 0) {
            array[left] = array[right];
            array[right] = 0;
            moveZero(array, left + 1, right - 1);
        } else {
            moveZero(array, left + 1, right);
        }
    }
    
    /*
     * The relative order will be maintained
     */
    public static int[] moveZero2(int[] array) {
        if (array == null || array.length == 0) {
            return array;
        }
        
        int pos = 0, move = 0;
        while (move < array.length) {
            if (array[move] != 0) {
                array[pos++] = array[move++];
            } else {
                move++;
            }
        }
        while (pos < array.length) {
            array[pos++] = 0;
        }
        return array;
    }
    
}
