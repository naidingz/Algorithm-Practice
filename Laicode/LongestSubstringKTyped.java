package laicode;

import java.util.HashMap;

public class LongestSubstringKTyped {
    public static void main(String[] args) {
        LongestSubstringKTyped test = new LongestSubstringKTyped();
        System.out.println(test.longest("aabccddda", 2));
    }

    public String longest(String input, int k) {
        if (input == null) {
            return null;
        }

        String result = "";
        HashMap<Character, Integer> map = new HashMap<>();
        int slow = 0, fast = 0;
        while (slow <= fast) {
            if (map.size() <= k) {
                if (map.size() == k && result.length() < fast - slow) {
                    result = input.substring(slow, fast);
                }
                if (fast >= input.length()) {
                    break;
                }
                char c = input.charAt(fast);
                map.put(c, map.getOrDefault(c, 0) + 1);
                fast++;
            } else {
                char c = input.charAt(slow);
                Integer i = map.get(c);
                if (i == 1) {
                    map.remove(c);
                } else {
                    map.put(c, i - 1);
                }
                slow++;
            }
        }

        return result;
    }
}
