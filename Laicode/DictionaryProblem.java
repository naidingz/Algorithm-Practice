package laicode;

import java.util.HashSet;

public class DictionaryProblem {

	public static void main(String[] args) {
		DictionaryProblem test = new DictionaryProblem();
		String[] dict = new String[]{"bob", "dad"};
		System.out.println(test.canBreak("dadbo", dict));
	}
	
	/*
	 * Given a word and a dictionary, 
	 * determine if it can be composed by concatenating words from the given dictionary.
	 * 
	 * Assumption: 
	 * The given word is not null and is not empty
	 * The given dictionary is not null and is not empty and 
	 * all the words in the dictionary are not null or empty
	 */
	public boolean canBreak(String input, String[] dict) {
		boolean[] M = new boolean[input.length() + 1];
		HashSet<String> set = new HashSet<>();
		for (String s : dict) {
			set.add(s);
		}
		M[0] = true;
		M[1] = set.contains(input.substring(0, 1));
		for (int i = 2; i <= input.length(); i++) {
			for (int j = 0; j < i; j++) {
				if (M[j] && set.contains(input.substring(j, i))) {
					M[i] = true;
					break;
				}
			}
		}
		return M[input.length()];
	}

}
