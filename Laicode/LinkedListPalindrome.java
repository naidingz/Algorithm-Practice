package laicode;

public class LinkedListPalindrome {

	public static void main(String[] args) {
		ListNode n1 = new ListNode(1);
		ListNode n2 = new ListNode(2);
		ListNode n3 = new ListNode(3);
		ListNode n4 = new ListNode(4);
		ListNode n5 = new ListNode(3);
		ListNode n6 = new ListNode(2);
		ListNode n7 = new ListNode(1);
		
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		n6.next = n7;
		
		LinkedListPalindrome test = new LinkedListPalindrome();
		System.out.println(test.isPalindrome(n1));
	}
	
	static class ListNode {
		public int value;
		public ListNode next;

		public ListNode(int value) {
			this.value = value;
			next = null;
		}
	}
	
	public boolean isPalindrome(ListNode head) {
		if (head == null || head.next == null) {
			return true;
		}

		ListNode slow = head, fast = head;
		while (fast.next != null && fast.next.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}

		ListNode middle = slow;
		middle = middle.next;
		slow.next = null;
		middle = reverse(middle);

		while (head != null && middle != null) {
			if (head.value != middle.value) {
				return false;
			} else {
				head = head.next;
				middle = middle.next;
			}
		}
		return true;
	}

	public ListNode reverse(ListNode head) {
		ListNode prev = null, curr = head;
		while (curr != null) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		return prev;
	}

}
