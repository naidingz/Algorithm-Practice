import java.util.ArrayList;
import java.util.List;


public class mergeKSortedInterval {

	public static void main(String[] args) {
		mergeKSortedInterval test = new mergeKSortedInterval();
		
		List<Interval> list1 = new ArrayList<>();
		list1.add(new Interval(1, 5));
		list1.add(new Interval(9, 13));
		list1.add(new Interval(16, 28));
		list1.add(new Interval(32, 53));
		
		List<Interval> list2 = new ArrayList<>();
		list2.add(new Interval(16, 22));
		list2.add(new Interval(31, 50));
		list2.add(new Interval(51, 53));
		list2.add(new Interval(57, 61));
		
		List<List<Interval>> list = new ArrayList<>();
		list.add(list1);
		list.add(list2);
		
		List<Interval> result = test.mergeKSortedIntervalLists(list);
		for (Interval i : result) {
			System.out.println("(" + i.start + ", " + i.end + ")");
		}
	}
	
	public static class Interval {
		int start, end;
		Interval(int start, int end) {
			this.start = start;
			this.end = end;
		}
	}
	
	public List<Interval> mergeKSortedIntervalLists(List<List<Interval>> intervals) {
        List<Interval> result = intervals.get(0);
        for (int i = 1; i < intervals.size(); i++) {
            List<Interval> merge = intervals.get(i);
            result = mergeTwoSortedIntervalLists(result, merge);
        }
        return result;
    }
    
    private List<Interval> mergeTwoSortedIntervalLists(List<Interval> one, List<Interval> two) {
        List<Interval> result = new ArrayList<>();
        int i = 0, j = 0;
        Interval candidate = null;
        if (one.get(i).start <= two.get(i).start) {
            candidate = one.get(i++);
        } else {
            candidate = two.get(j++);
        }
        while (i < one.size() || j < two.size()) {
            if (i == one.size()) {
                if (two.get(j).start <= candidate.end) {
                    candidate.end = Math.max(two.get(j).end, candidate.end);
                } else {
                    result.add(candidate);
                    candidate = two.get(j);
                }
                j++;
            } else if (j == two.size()){
                if (one.get(i).start <= candidate.end) {
                    candidate.end = Math.max(one.get(i).end, candidate.end);
                } else {
                    result.add(candidate);
                    candidate = one.get(i);
                }
                i++;
            } else if (one.get(i).start <= two.get(j).start) {
                if (one.get(i).start <= candidate.end) {
                    candidate.end = Math.max(one.get(i).end, candidate.end);
                } else {
                    result.add(candidate);
                    candidate = one.get(i);
                }
                i++;
            } else {
                if (two.get(j).start <= candidate.end) {
                    candidate.end = Math.max(two.get(j).end, candidate.end);
                } else {
                    result.add(candidate);
                    candidate = two.get(j);
                }
                j++;
            }
        }
        result.add(candidate);
        return result;
    }

}
