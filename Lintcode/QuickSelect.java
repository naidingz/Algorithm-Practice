public class QuickSelect {
    public static void main(String[] args) {
        int[] array = new int[] {1,5,2,4,3,8,6,7};
        QuickSelect test = new QuickSelect();
        System.out.println(test.quickSelect(array, 3));

    }

    /*
     * kth smallest
     */
    public int quickSelect(int[] array, int k) {
        int left = 0, right = array.length - 1;
        while (left <= right) {
            int mid = partition(array, left, right);
            if (mid == k - 1) {
                return array[mid];
            } else if (mid < k - 1) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return -1;
    }

    private int partition(int[] array, int left, int right) {
        int pivotIndex = left + (int) Math.random() * (right - left + 1);
        swap(array, right, pivotIndex);
        int i = left, j = right - 1;
        while (i <= j) {
            if (array[i] < array[right]) {
                i++;
            } else {
                swap(array, i, j--);
            }
        }
        swap(array, i, right);
        return i;
    }

    private void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

}