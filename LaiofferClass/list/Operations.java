package list;

public class Operations {

	public static void main(String[] args) {
		 ListNode node1 = new ListNode(2);
		 ListNode node2 = new ListNode(5);
		 ListNode node3 = new ListNode(3);
		 ListNode node4 = new ListNode(8);
		 ListNode node5 = new ListNode(7);
		 
		 node5.next = null;
		 node4.next = node5;
		 node3.next = node4;
		 node2.next = node3;
		 node1.next = node2;
		 
//		 printList(node1);
//		 printList(reOrder(node1));
		 
//		 ListNode head1 = reverse(node1);
//		 printList(head1);
//		 ListNode head2 = reverse(node1);
//		 printList(head2);

//		 System.out.println(middleNode(node1).value);
		 
//		 ListNode head3 = insertNode(node1, 100);
//		 printList(head3);

        printList(node1);
        printList(quickSort(node1));
		 
	}
	
	public static void printList(ListNode head) {
		while (head != null) {
			System.out.print(head.value + " ");
			head = head.next;
		}
		System.out.println();
	}
	
	 /*--------------------------------------------------------------*/
	// Time O(N), Space O(1)
	public static ListNode reverse(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode prev = null, curr = head;
		while(curr != null) {
			ListNode next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		return prev;
	}
	
	// Time O(N), Space O(N)
	public static ListNode reverse2(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode subHead = reverse2(head.next);
		ListNode tail = head.next;
		tail.next = head;
		head.next = null;
		
		return subHead;
	}
	
    /*--------------------------------------------------------------*/
	public static ListNode middleNode(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		ListNode slow = head, fast = head;
		while(fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow;
	}
	
    /*--------------------------------------------------------------*/
	public static boolean hasCycle(ListNode head) {
		if (head == null || head.next == null) {
			return false;
		}
		
		ListNode slow = head;
		ListNode fast = head;
		
		while(fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
			
			if (slow == fast) {
				return true;
			}
		}
		return false;
	}
	
    /*--------------------------------------------------------------*/
	public static ListNode insertNode(ListNode head, int target) {
		if (head == null || head.value > target) {
			ListNode newHead = new ListNode(target);
			newHead.next = head;
			return newHead;
		}
		
		ListNode prev = null, curr = head;
		while (curr != null) {
			if (target >= curr.value) {
				prev = curr;
				curr = curr.next;
			} else {
				break;
			}
		}
		
		ListNode newNode = new ListNode(target);
		prev.next = newNode;
		newNode.next = curr;
		return head;
	}
	
    /*--------------------------------------------------------------*/
	public static ListNode mergeList(ListNode head1, ListNode head2) {
		
		if (head1 == null) return head2;
		if (head2 == null) return head1;
		
		ListNode dummyHead = new ListNode(0);
		ListNode curr = dummyHead;
		ListNode curr1 = head1;
		ListNode curr2 = head2;
		
		while (curr1 != null && curr2 != null) {
			if (curr1.value <= curr2.value) {
				curr.next = curr1;
				curr1 = curr1.next;
			} else {
				curr.next = curr2;
				curr2 = curr2.next;
			}
			curr = curr.next;
		}
		
		if (curr1 == null) {
			curr.next = curr2;
		} 
		if (curr2 == null) {
			curr.next = curr1;
		}
		
		return dummyHead.next;
	}
	
	/*--------------------------------------------------------------*/
	
	public static ListNode partition(ListNode head, int target) {
		if (head == null || head.next == null) {
			return head;
		}
	
		ListNode dummyHead1 = new ListNode(0);
		ListNode dummyHead2 = new ListNode(0);
		
		ListNode curr = head;
		ListNode curr1 = dummyHead1;
		ListNode curr2 = dummyHead2;
		
		while (curr != null) {
			if (curr.value < target) {
				curr1.next = curr;
				curr1 = curr1.next;
			} else {
				curr2.next = curr;
				curr2 = curr2.next;
			}
			curr = curr.next;
		}
		
		curr1.next = dummyHead2.next;
		curr2.next = null;
		return dummyHead1.next;
	}
	
	/*--------------------------------------------------------------*/
	
	public static ListNode reOrder(ListNode head) {
	    if (head == null || head.next == null) {
	        return head;
	    }
	    
	    // find the middle point
	    ListNode slow = head, fast = head;
	    while (fast.next != null && fast.next.next != null) {
	        slow = slow.next;
	        fast = fast.next.next;
	    }
	    
	    ListNode middle = slow.next;
	    slow.next = null;
	    
	    middle = reverse(middle);
	    printList(middle);
	    return merge(head, middle);
	}
	
    private static ListNode merge(ListNode head1, ListNode head2) {
        if (head1 == null) {
            return head2;
        } else if (head2 == null) {
            return head1;
        }

        ListNode dummyHead = new ListNode(0);
        ListNode curr = dummyHead;
        while (true) {
            if (head1 != null) {
                curr.next = head1;
                head1 = head1.next;
                curr = curr.next;
            }
            if (head2 != null) {
                curr.next = head2;
                head2 = head2.next;
                curr = curr.next;
            }
            if (head1 == null && head2 == null) {
                break;
            }
        }
        return dummyHead.next;
    }

    /*--------------------------------------------------------------*/
    public static ListNode selectionSort(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode dummyHead = new ListNode(0);
        ListNode curr = dummyHead;
        while (head != null) {
            int min = findMinimum(head);
            ListNode tmpDummy = new ListNode(0);
            tmpDummy.next = head;
            ListNode tmpCurr = tmpDummy;
            while (tmpCurr != null && tmpCurr.next != null) {
                if (tmpCurr.next.value == min) {
                    ListNode minNode = tmpCurr.next;
                    tmpCurr.next = minNode.next;
                    tmpCurr = minNode.next;
                    curr.next = minNode;
                    curr = curr.next;
                    minNode.next = null;
                } else {
                    tmpCurr = tmpCurr.next;
                }
            }
            head = tmpDummy.next;
        }
        return dummyHead.next;
    }

    private static int findMinimum(ListNode head) {
        if (head == null) {
            return -1;
        }
        int minimum = head.value;
        while (head.next != null) {
            head = head.next;
            if (head.value < minimum) {
                minimum = head.value;
            }
        }
        return minimum;
    }

    /*--------------------------------------------------------------*/
    public static ListNode quickSort(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }

        ListNode dummyHead1 = new ListNode(0);
        ListNode dummyHead2 = new ListNode(0);

        ListNode targetNode = head;
        ListNode curr1 = dummyHead1;
        ListNode curr2 = dummyHead2;

        ListNode curr = targetNode.next;
        targetNode.next = null;
        while (curr != null) {
            if (curr.value < targetNode.value) {
                curr1.next = curr;
                curr1 = curr1.next;
            } else {
                curr2.next = curr;
                curr2 = curr2.next;
            }
            curr = curr.next;
        }
        curr1.next = null;
        curr2.next = null;

        ListNode leftHead = quickSort(dummyHead1.next);
        ListNode rightHead = quickSort(dummyHead2.next);

        if (leftHead == null) {
            targetNode.next = rightHead;
            return targetNode;
        }
        curr = leftHead;
        while (curr != null & curr.next != null) {
            curr = curr.next;
        }
        curr.next = targetNode;
        targetNode.next = rightHead;
        return leftHead;
    }
}
