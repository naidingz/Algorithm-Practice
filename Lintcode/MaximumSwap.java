import java.util.List;
import java.util.ArrayList;

public class MaximumSwap {
    public static void main(String[] args) {
        MaximumSwap test = new MaximumSwap();
        System.out.println(test.maximumSwap(759));
    }

    public int maximumSwap(int num) {
        if (num == 0) {
            return 0;
        }

        List<Integer> M = new ArrayList<>();
        List<Integer> nums = new ArrayList<>();
        nums.add(num % 10);
        M.add(num % 10);
        int temp = num / 10, count = 1;
        while (temp != 0) {
            nums.add(temp % 10);
            M.add(Math.max(M.get(count - 1), temp % 10));
            temp /= 10;
            count++;
        }

        int i = -1;
        for (i = nums.size() - 1; i >= 0; i--) {
            if (nums.get(i) < M.get(i)) {
                break;
            }
        }

        if (i == -1) {
            return num;
        }

        int j = 0;
        for (j = 0; j < count; j++) {
            if (nums.get(j) == M.get(i)) {
                break;
            }
        }

        swap(nums, i, j);

        int ans = 0;
        for (int k = count - 1; k >= 0; k--) {
            ans = 10 * ans + nums.get(k);
        }
        return ans;
    }

    private void swap(List<Integer> list, int i, int j) {
        int temp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, temp);
    }
}