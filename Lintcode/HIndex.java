public class HIndex {

    public static void main(String[] args) {
        HIndex test = new HIndex();
        System.out.println(test.hIndex(new int[]{3, 6, 1, 0, 5}));
    }

    public int hIndex(int[] citations) {
        int max = 0;
        for (Integer i : citations) {
            max = Math.max(max, i);
        }

        int[] buckets = new int[max + 1];
        for (Integer i : citations) {
            buckets[i]++;
        }

        int count = 0;
        for (int citaion = buckets.length - 1; citaion >= 1; citaion--) {
            count += buckets[citaion];
            if (count >= citaion) {
                return Math.min(count, citaion);
            }
        }

        return 0;
    }
}