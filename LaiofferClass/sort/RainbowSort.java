package sort;

public class RainbowSort {

	public static void main(String[] args) {
		char[] array = {'a', 'b', 'c', 'a', 'b', 'c', 'a', 'c', 'b', 'a'};
		RainbowSort sol = new RainbowSort();
		sol.sort(array);
		for (char i : array) {
			System.out.print(i + " ");
		}
	}
	
	public void sort(char[] array) {
		if (array == null || array.length == 0) {
			return;
		}
		
		int i = 0, j = 0, k = array.length - 1;
		while (j <= k) {
			if (array[j] == 'a') {
				swap(array, i++, j++);
			} else if (array[j] == 'b') {
				j++;
			} else if (array[j] == 'c') {
//				if (array[k] == 'c') {
//					k--;
//				} else {
//					swap(array, j, k--);
//				}
				swap(array, j, k--);
			}
		}
	}
	
	private void swap(char[] array, int i, int j) {
		char temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

}
