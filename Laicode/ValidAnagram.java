package laicode;

import java.util.HashMap;

public class ValidAnagram {
    public boolean isAnagram(String s, String t) {
        if (s == t) {
            return true;
        }
        if (s == null || t == null || s.length() != t.length()) {
            return false;
        }

        HashMap<Character, Integer> occurCount = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            Character c = s.charAt(i);
            occurCount.put(c, occurCount.getOrDefault(c, 0) + 1);
        }
        for (int i = 0; i < t.length(); i++) {
            Character c = t.charAt(i);
            if (!occurCount.containsKey(c)) {
                return false;
            } else {
                occurCount.put(c, occurCount.get(c) - 1);
                if (occurCount.get(c) == 0) {
                    occurCount.remove(c);
                }
            }
        }
        return occurCount.size() == 0;
    }
}
