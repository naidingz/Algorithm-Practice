package laicode.class_07;

import java.util.HashMap;
import java.util.PriorityQueue;

public class TopKFrequentWords {
    public static void main(String[] args) {
        String[] strs = {"d","a","c","b","d","a","b","b","a","d","d","a","d"};
        TopKFrequentWords test = new TopKFrequentWords();
        for (String s : test.topKFrequent(strs, 5)) {
            System.out.println(s);
        }

    }

    private class MapPair implements Comparable<MapPair>{
        String key;
        Integer value;
        public MapPair(String k, Integer v) {
            key = k;
            value = v;
        }

        public int compareTo(MapPair that) {
            if (this.value < that.value) {
                return -1;
            } else if (this.value > that.value) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public String[] topKFrequent(String[] combo, int k) {
        if (combo == null || combo.length == 0) {
            return new String[0];
        }
        HashMap<String, Integer> map = new HashMap<>();
        for (String s : combo) {
            int freq = map.getOrDefault(s, 0);
            map.put(s, ++freq);
        }

        int size = Math.min(k, map.size());
        PriorityQueue<MapPair> pq = new PriorityQueue<>(size);
        int count = 0;
        for (String key : map.keySet()) {
            if (count < size) {
                pq.offer(new MapPair(key, map.get(key)));
                count++;
            } else if (map.get(key) > pq.peek().value) {
                pq.poll();
                pq.offer(new MapPair(key, map.get(key)));
            }
        }
        String[] result = new String[size];
        for (int i = 0; i < size; i++) {
            result[size-i-1] = pq.poll().key;
        }
        return result;
    }
}
