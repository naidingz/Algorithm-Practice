public class FirstMissingPositive {

	public static void main(String[] args) {
		FirstMissingPositive test = new FirstMissingPositive();
		System.out.println(test.firstMissingPositive(new int[] {2,2,2,2,2}));
	}
	
	public int firstMissingPositive(int[] A) {
        if (A.length == 0) {
            return 1;
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] > 0 && A[i] < A.length) {
            		int tmp = A[i];
            		A[i] = A[tmp - 1];
            		A[tmp - 1] = tmp;
               
                if (tmp - 1 > i && tmp != A[i]) {
                    i--;
                }
            }
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] != i + 1) {
                return i + 1;
            }
        }
        return A[A.length - 1] + 1;
    }
}
