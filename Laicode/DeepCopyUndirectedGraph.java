package laicode;

import java.util.*;

public class DeepCopyUndirectedGraph {

    static class  GraphNode {
        public int key;
        public List<GraphNode> neighbors;
        public GraphNode(int key) {
            this.key = key;
            this.neighbors = new ArrayList<GraphNode>();
        }
    }

    public List<GraphNode> copy(List<GraphNode> graph) {
        List<GraphNode> result = new ArrayList<>();
        if (graph == null) {
            return result;
        }
        HashMap<GraphNode, GraphNode> map = new HashMap<>();
        dfs(graph, map);

        for (Map.Entry<GraphNode, GraphNode> e : map.entrySet()) {
            List<GraphNode> newNeighbors = new ArrayList<>();
            for (GraphNode nei : e.getKey().neighbors) {
                newNeighbors.add(map.get(nei));
            }
            e.getValue().neighbors = newNeighbors;
        }

        for (GraphNode node : graph) {
            result.add(map.get(node));
        }
        return result;
    }

    private void dfs(List<GraphNode> graph, HashMap<GraphNode, GraphNode> map) {
        HashSet<GraphNode> visited = new HashSet<>();
        Queue<GraphNode> queue = new LinkedList<>();
        for (GraphNode beginNode : graph) {
            if (!visited.contains(beginNode)) {
                queue.offer(beginNode);
                visited.add(beginNode);
            }
            while (!queue.isEmpty()) {
                GraphNode node = queue.poll();
                GraphNode newNode = new GraphNode(node.key);
                map.put(node, newNode);

                for (GraphNode nei : node.neighbors) {
                    if (!visited.contains(nei)) {
                        queue.offer(nei);
                        visited.add(nei);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        GraphNode g1 = new GraphNode(1);
        GraphNode g2 = new GraphNode(2);
        GraphNode g3 = new GraphNode(3);
        GraphNode g4 = new GraphNode(4);

        g1.neighbors = new ArrayList<GraphNode>() {{
            add(g2);
            add(g3);
            add(g4);
        }};

        g2.neighbors = new ArrayList<GraphNode>() {{
            add(g1);
            add(g4);
        }};

        g3.neighbors = new ArrayList<GraphNode>() {{
            add(g1);
        }};

        g4.neighbors = new ArrayList<GraphNode>() {{
            add(g1);
            add(g2);
        }};

        DeepCopyUndirectedGraph test = new DeepCopyUndirectedGraph();
        test.copy(new ArrayList<GraphNode>() {{
            add(g1);
            add(g2);
        }});


    }
}
