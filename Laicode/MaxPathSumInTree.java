package laicode;

import java.util.HashSet;

public class MaxPathSumInTree {

	public static void main(String[] args) {

	}

	public class TreeNode {
		public int key;
		public TreeNode left;
		public TreeNode right;

		public TreeNode(int key) {
			this.key = key;
		}
	}
	
	
	/*
	 * Max Path Sum I
	 * From leaf to leaf
	 */
	public int maxPathSumI(TreeNode root) {
		int[] result = new int[] { Integer.MIN_VALUE };
		if (root == null) {
			return result[0];
		}
		maxPathSumI(root, result);
		return result[0];
	}

	private int maxPathSumI(TreeNode root, int[] globalMax) {
		if (root == null) {
			return 0;
		}
		int leftNum = maxPathSumI(root.left, globalMax);
		int rightNum = maxPathSumI(root.right, globalMax);
		int sum = leftNum + rightNum + root.key;

		if (globalMax[0] < sum && (root.left != null && root.right != null)) {
			globalMax[0] = sum;
		}

		if (root.left != null && root.right == null) {
			return leftNum + root.key;
		} else if (root.left == null && root.right != null) {
			return rightNum + root.key;
		} else {
			return Math.max(leftNum, rightNum) + root.key;
		}
	}
	
	/*
	 * Max Path Sum III
	 * From node to node
	 * Assumption: the root is not null
	 */
	public int maxPathSumII(TreeNode root) {
		int[] result = new int[] {Integer.MIN_VALUE};
		maxPathSumII(root, result);
		return result[0];
	}
	
	private int maxPathSumII(TreeNode root, int[] globalMax) {
		if (root == null) {
			return 0;
		}

		int leftNum = maxPathSumII(root.left, globalMax);
		int rightNum = maxPathSumII(root.right, globalMax);

		leftNum = Math.max(0, leftNum);
		rightNum = Math.max(0, rightNum);
		int sum = leftNum + root.key + rightNum;
		globalMax[0] = sum > globalMax[0] ? sum : globalMax[0];

		return Math.max(leftNum, rightNum) + root.key;
	}
	
	/*
	 * Max Path Sum III
	 * A Sub-path from root to leaf
	 * Assumption: the root is not null
	 */
	public int maxPathSumIII(TreeNode root) {
		int[] result = new int[] {Integer.MIN_VALUE};
		maxPathSumIII(root, result, 0);
		return result[0];
	}
	
	private void maxPathSumIII(TreeNode root, int[] globalMax, int preMaxSum) {
		if (root == null) {
			return;
		}
		preMaxSum = root.key + Math.max(preMaxSum, 0);
		globalMax[0] = Math.max(globalMax[0], preMaxSum);
		
		maxPathSumIII(root.left, globalMax, preMaxSum);
		maxPathSumIII(root.right, globalMax, preMaxSum);
	}
	
	/*
	 * Max Path Sum IV
	 * A path from root to leaf
	 * Assumption: the root is not null
	 */
	public int maxPathSumIV(TreeNode root) {
		int[] result = new int[] {Integer.MIN_VALUE};
		maxPathSumIV(root, result, 0);
		return result[0];
	}
	
	private void maxPathSumIV(TreeNode root, int[] globalMax, int prefixSum) {
		if (root == null) {
			return;
		}
		prefixSum += root.key;
		if (root.left == null && root.right == null) {
			globalMax[0] = Math.max(globalMax[0], prefixSum);
		}
		
		maxPathSumIII(root.left, globalMax, prefixSum);
		maxPathSumIII(root.right, globalMax, prefixSum);
	}
	
	/*
	 * Max Path Sum V
	 * A sub-path from root to leaf equal to given target number
	 * Assumption: the root is not null
	 */
	public boolean maxPathSumV(TreeNode root, int target) {
		return maxPathSumV(root, target, 0, new HashSet<Integer>());
	}
	
	private boolean maxPathSumV(TreeNode root, int target, int prefixSum, HashSet<Integer> set) {
		if (root == null) {
			return false;
		}
		int currentSum = prefixSum + root.key;
		if (currentSum == target || set.contains(currentSum - target)) {
			return true;
		}

		set.add(currentSum);
		if (root.left != null) {
			if (maxPathSumV(root.left, target, currentSum, set)) {
				return true;
			}
			set.remove(currentSum + root.left.key);
		}

		if (root.right != null) {
			if (maxPathSumV(root.right, target, currentSum, set)) {
				return true;
			}
			set.remove(currentSum + root.right.key);
		}
		return false;
	}
	
}
