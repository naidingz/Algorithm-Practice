package laicode.class_06;

import java.util.ArrayList;
import java.util.List;

public class CombinationsOfCoins {

    public static void main(String[] args) {
        int[] coins = {25, 10, 5, 1};
        CombinationsOfCoins test = new CombinationsOfCoins();
        for (List<Integer> i : test.combinations(99, coins)) {
            System.out.println(i);
        }
    }

    public List<List<Integer>> combinations(int target, int[] coins) {
        List<List<Integer>> result = new ArrayList<>();
        if (coins == null || coins.length == 0) {
            return result;
        }
        List<Integer> oneWay = new ArrayList<>();
        for (int i = 0; i < coins.length; i++) {
            oneWay.add(0);
        }
        combinations(target, coins, 0, oneWay, result);
        return result;
    }

    private void combinations(int moneyLeft, int coins[], int index, List<Integer> oneWay, List<List<Integer>> result) {
        if (index == coins.length) {
            if (moneyLeft == 0) {
                result.add(new ArrayList<>(oneWay));
            }
            return;
        }

        for (int i = 0; i * coins[index] <= moneyLeft; i++) {
            oneWay.set(index, i);
            combinations(moneyLeft - i * coins[index], coins, index + 1, oneWay, result);
        }
    }
}
