package laicode;

public class PowerOfTwo {
    public boolean isPowerOfTwo(int number) {
        if (number <= 0) {
            return false;
        }

        int count = 0;
        for (int i = 0; i < 31; i++) {
            if (((number >> i) & 1) == 1) {
                count++;
            }
        }
        return count == 1;
    }
}
