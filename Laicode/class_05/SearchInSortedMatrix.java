package laicode.class_05;

import java.util.PriorityQueue;
import java.util.Queue;

public class SearchInSortedMatrix {

    public static void main(String[] args) {
        int[][] matrix = new int[][] {{1,3,5,7},{2,4,8,9},{3,5,11,15},{6,8,13,18}};
        SearchInSortedMatrix test = new SearchInSortedMatrix();
//        System.out.println(Arrays.toString(test.search(matrix, 4)));
        System.out.println(test.kthSmallest(matrix, 8));
        
    }
    
    public int[] search(int[][] matrix, int target) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return null;
        }

        int m = matrix.length, n = matrix[0].length;
        int left = 0, right = m * n - 1;
        while (left <= right) {
            int mid = left + (right - left) / 2;
            int row = mid / n, col = mid % n;
            if (matrix[row][col] == target) {
                return new int[] { row, col };
            } else if (matrix[row][col] < target) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return new int[] { -1, -1 };
    }

    class ArrayElement implements Comparable<ArrayElement> {
        final int row;
        final int col;
        final int value;

        public ArrayElement(int r, int c, int v) {
            row = r;
            col = c;
            value = v;
        }

        public int compareTo(ArrayElement a) {
            if (this.value > a.value) {
                return 1;
            } else if (this.value < a.value) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public int kthSmallest(int[][] matrix, int k) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return -1;
        }

        int m = matrix.length, n = matrix[0].length;
        boolean[][] visited = new boolean[m][n];

        Queue<ArrayElement> pq = new PriorityQueue<>();
        pq.offer(new ArrayElement(0, 0, matrix[0][0]));

        int count = 0;
        while (!pq.isEmpty()) {
            ArrayElement curr = pq.poll();
            if (visited[curr.row][curr.col]) {
                continue;
            }
            count++;
            if (count == k) {
                return curr.value;
            }
            if (curr.row < m - 1) {
                pq.offer(new ArrayElement(curr.row + 1, curr.col, matrix[curr.row + 1][curr.col]));
            }
            if (curr.col < n - 1) {
                pq.offer(new ArrayElement(curr.row, curr.col + 1, matrix[curr.row][curr.col + 1]));
            }
            visited[curr.row][curr.col] = true;

        }
        return -1;
    }
}

