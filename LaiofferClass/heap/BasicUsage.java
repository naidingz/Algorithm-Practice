package heap;
import java.util.*;

public class BasicUsage {

	public static void main(String[] args) {
		
//		PriorityQueue<Integer> pq = new PriorityQueue<>();
//		pq.offer(5);
//		pq.offer(3);
//		pq.offer(6);
//		
//		System.out.println(pq.peek());
//		System.out.println(pq.poll());
//		System.out.println(pq.size());
//		
//		System.out.println(pq.peek());
//		System.out.println(pq.poll());
//		System.out.println(pq.size());
//		
//		System.out.println(pq.peek());
//		System.out.println(pq.poll());
//		System.out.println(pq.size());
		
		PriorityQueue<Node> pq = new PriorityQueue<>(new NodeComparator());
		Node n1 = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		
		pq.offer(n2);
		pq.offer(n1);
		pq.offer(n3);
		
		// won't update
//		n1.value = 4;
		
		pq.remove(n1);
		n1.value = 4;
		pq.offer(n1);
		
		
		System.out.println(pq.poll().value);
		System.out.println(pq.poll().value);
		System.out.println(pq.poll().value);
		
	}
	
}

class Node {
	public int value;
	public Node(int v) {
		value = v;
	}
}

class NodeComparator implements Comparator<Node> {
	@Override
	public int compare(Node o1, Node o2) {
		return o1.value - o2.value;
	}
}